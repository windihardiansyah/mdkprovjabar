$(document).ready(function(){
  // $("#loader").hide();
  // $("#loader_main").hide();
  var kota = document.getElementById("kota");
  var kecamatan = document.getElementById("kecamatan");
  var desa = document.getElementById("desa");
  $("#generate_data").hide();
  $("#loader").hide();
  
  $("#propinsi").change(function(){

        $("#generate_data").fadeOut();

        var propinsi = $("#propinsi").val();
        //alert(propinsi);
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = propinsi;

        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota").html(data);  
                $("#kecamatan").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");                
            }
        });
  });

  $("#kota").change(function(){

        $("#generate_data").fadeOut();
        var kota = $("#kota").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kota;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan").html(data);
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");
            }
        });
  });

  $("#kecamatan").change(function(){
    
        $("#generate_data").fadeOut();
        // $("#data-mahasiswa").fadeOut();
        var kecamatan = $("#kecamatan").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kecamatan;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa").html(data);
            }
        });
  });

  $("#desa").change(function(){
        $("#generate_data").fadeIn();
        // $("#data-mahasiswa").fadeOut();
        var desa = $("#desa").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = desa;

        $.ajax({
            url: "proses_kecamatan.php",
            //data: "wil_id=" + desa + "&kode=RW",
            success: function(data){

            }
        });
  });

});