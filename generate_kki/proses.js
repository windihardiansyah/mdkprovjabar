(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		
		$("#generate_data").bind("click", function(event) {
			$("#loader").fadeIn();
			var url = "generate_data.php";
			var kode_kel = $("#kode_kel").val();

			//alert(kode_kel);
			//exit();
			//var length = kd_fam.length;
			// kode_kel nilai id dari tombol hapus
			if(kode_kel=="" || kode_kel==null){
				alert("No KKI tidak boleh kosong...!");
				var error = document.getElementById("kode_kel");
					error.focus();
			}else{
			
				// tampilkan dialog konfirmasi
				var answer = confirm("Apakah anda ingin melakukan generate Kode KKI dan KAK untuk kode wilayah  "+kode_kel+" ?");
				
				// // ketika ditekan tombol ok
				if (answer) {
					var butt = document.getElementById("generate_data");
						butt.disabled = true;

					$.post(url, {kode_kel: kode_kel} ,function() {
						//reload data
						$("#loader").fadeOut(100);
						alert("Data KAK dan KKI pada wilayah "+kode_kel+" Berhasil Digenerate");
						location.reload();
					});
				}else{
					$("#loader").fadeOut(100);	
				}
				
			}
		});

	});
}) (jQuery);
