<?php 
	
	include "../brt_main_cms_1.php";
	include "../koneksi/koneksi.inc.php"; 
	session_start();
?>

<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="../bootstrap/css/datepicker.css">
<script src="../bootstrap/js/jquery-1.8.3.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wilayah.js"></script>
<style type="text/css">
	select, input[type="file"] {
	    height: 26px;
	    line-height: 19px;
	    font-size: 12px;
	}
	textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input{
			font-size: 12px;
			height: 16px;
	}

</style>
<div id="w_d_k" align="center"></div>
<div id="c_r_sub" align="center"></div>

<center><h5>GENERATE KODE KKI DAN KAK</h5></center>
<div class="container">
	<table>
	    <tr>
	      	<td>
	        	<label class="control-label" for="Propinsi">Propinsi : </label>
	      	</td>
	      	<td>
		        <select name="propinsi" id="propinsi" class="span2">
		          <option value="">--Pilih Propinsi--</option>
		          <?php

		            // tampilkan nama-nama propinsi yang ada di database
		            $sql = mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit=9 ORDER BY no_unit_detail");
		            while($p=mysql_fetch_array($sql)){
		              echo "<option value=$p[id_unik]>$p[no_unit_detail]</option> \n";
		            }
		          ?>
		        </select>
		    </td>
	      	<td>
	        	<label id="lkota" class="control-label" for="Kota">Kota :</label>
	      	</td>
	      	<td>
		        <select name="kota" id="kota" class="span2">
		          <option value="">--Pilih Kota--</option>
		        </select>
	      	</td>
	      	<td>
		        <label id="lkecamatan" class="control-label" for="Kecamatan">Kecamatan :</label>
		    </td>
	      	<td>
		        <select name="kecamatan" id="kecamatan" class="span2">
		          <option value="">--Pilih Kecamatan--</option>
		        </select>
	      	</td>
	      	<td>
	        	<label id="ldesa" class="control-label" for="Kelurahan">Kelurahan :</label>
	      	</td>
	      	<td>  
		        <select name="desa" id="desa" class="span2">
		          <option value="">--Pilih Kelurahan--</option>
		        </select>
	      	</td>
	    </tr>
	    <tr>
	      	<td>
	        	<label class="control-label" for="wilayah">Kode Wilayah : </label>
	      	</td>
	      	<td colspan="2">
	      		<div class="controls">
					<input type="text" id="kode_kel" class="input-large" name="kode_kel" disabled>
				</div>
	      	</td>
	      	<td>
	      		<button id="generate_data" name="generate_data" class="btn btn-primary"><i class="icon-eye-open icon-white"></i> GENERATE DATA</button>
	      	</td>
	    <tr>
	</table>
	<div id="loader" name="loader">
		<center>
			<img src="loader.gif" width="100px" height="auto"><br> <b>Sedang Memproses... Harap Tunggu Sebentar</b>
		</center>
	</div>
</div>
<script type="text/javascript" src="proses.js"></script>