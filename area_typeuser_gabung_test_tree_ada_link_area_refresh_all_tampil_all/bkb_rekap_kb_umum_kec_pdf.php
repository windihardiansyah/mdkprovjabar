<?php
session_start();
if(!isset($_SESSION['un']))
   exit();
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
include "../koneksi/koneksi.inc.php"; 

$pdf= new FPDF('L','cm','A4');
$pdf->Open();
$pdf->SetMargins(0.5, 3, 1);
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(29,0.5,'REKAPITULASI MIX KONTRASEPSI',0,0,'C',1,1);


$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','B',7);

$pdf->SetTextColor(125,50,50);
$qr_prop=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$_GET[prop]' ", $idmysql);
$rc_prop=mysql_fetch_array($qr_prop);
$nm_prop=$rc_prop[no_unit_detail];

$qr_kab_kota=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$_GET[kab_kota]' ", $idmysql);
$rc_kab_kota=mysql_fetch_array($qr_kab_kota);
$nm_kab_kota=$rc_kab_kota[no_unit_detail];


$pdf->Cell(3,0.5,'PROVINSI ',0,0,'',1);$pdf->Cell(5,0.5,': '. $nm_prop,0,0,'',1);
$pdf->Ln(0.4);
$pdf->Cell(3,0.5,'KABUPATEN/KOTA ',0,0,'',1);$pdf->Cell(5,0.5,': '. $nm_kab_kota,0,0,'',1);


$qr=mysql_query("SELECT * FROM dbo_contr_typ ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$jd=mysql_num_rows($qr)*2;
$pdf->SetFont('Arial','B',8);
$pdf->Ln();
$pdf->SetTextColor(255,255,255);
$pdf->SetFillColor(109,136,141);
$pdf->Cell(1,1.2,'NO',0,0,'C',1,1);
$pdf->Cell(3.5,1.2,'KECAMATAN',0,0,'C',1,1);
$pdf->Cell(2.5,1.2,'JUMLAH KK',0,0,'C',1,1);
$pdf->Cell(2,1.2,'PUS','R',0,'C',1,1);
$pdf->Cell($jd+2,1.2,'PESERTA KB PER METODE KONTRASEPSI','LRB',0,'C',1,1);
$pdf->Cell(2,1.2,'PREVALENSI','L',0,'C',1,1);
$pdf->Ln();
$pdf->Cell(1,0.65,'',0,0,'C',1,1);
$pdf->Cell(3.5,0.65,'',0,0,'C',1,1);
$pdf->Cell(2.5,0.65,'',0,0,'C',1,1);
$pdf->Cell(2,0.65,'','R',0,'C',1,1);

		$i=1;
		while($rc=mysql_fetch_array($qr))
		{	
		      $typ[$rc[Kd_contyp]]=0;
			  $tot_typ[$rc[Kd_contyp]]=0;
			  if($i==1)
               $pdf->Cell(2,0.65,$rc[Nm_contyp_ind],'LT',0,'R',1,1);
			  else
			   $pdf->Cell(2,0.65,$rc[Nm_contyp_ind],'T',0,'R',1,1); 
			   $i++;
        }
		$pdf->Cell(2,0.65,'JUMLAH','LT',0,'C',1,1);
$pdf->Cell(2,0.65,'KB','L',0,'C',1,1);
$pdf->Ln();
		
$pdf->SetFillColor(235,235,235);
$pdf->SetFont('Arial','',8);
$pdf->SetTextColor(0,0,0);	
 $qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail_idk='$_GET[kab_kota]' ORDER BY no_unit_detail", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	$i=1; $tot_jd=0; $tot_jd_i=0; 
	$tot_iud=0; $tot_mop=0; $tot_mow=0; $tot_imp=0; $tot_stk=0; $tot_pil=0; $tot_ov=0; $tot_kdm=0; $tot_tkbb=0; $tot_pus=0; 	 			
	while($rc_isi=mysql_fetch_array($qr_isi))
	{
			$qr=mysql_query("SELECT pus, Kd_prosplvl, Kd_fam, Kd_contyp FROM dbo_family WHERE Kd_subdist='$rc_isi[id_unit_detail]' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
		    $jd_i=0; $iud=0; $mop=0; $mow=0; $imp=0; $stk=0; $pil=0; $ov=0; $kdm=0; $jd=0; $pus=0;
			$j=1;
			$jd_i=mysql_num_rows($qr);
			while($rc=mysql_fetch_array($qr))
			{
			  if($rc[pus]=='1')
			  { 
				if($rc[Kd_contyp]!=NULL)
                {
                    $typ[$rc[Kd_contyp]]++;
					$tot_typ[$rc[Kd_contyp]]++;
					
					$jd++;
					$tot_tkbb++;
                }
                
				$pus++;
				$tot_pus++;
			  }   
			   $j++;
		    }
			if($pus!=0)
			{
			    $cu=round((($jd/$pus)*100),2)."%";
			}	
			else
			{
			    $cu="-";	
				$pus="-";	
		   }		
			
		   	$tot_jd_i=$tot_jd_i+$jd_i;
			if($jd_i==0)
			    $jd_i="-";
			
			
			$pdf->Cell(1,0.75,$i,0,0,'C',1);
$pdf->Cell(3.5,0.75,$rc_isi[no_unit_detail],0,0,'L',1);
$pdf->Cell(2.5,0.75,number_format($jd_i,0,',','.'),0,0,'C',1);
$pdf->Cell(2,0.75,number_format($pus,0,',','.'),0,0,'C',1);

           foreach($typ as $idx=>$isi)
	       {
   			   
			   $pdf->Cell(2,0.75,number_format($isi,0,',','.'),0,0,'C',1,1); 
		   }
		   if($jd==0)
			    $jd="-";
$pdf->Cell(2,0.75,number_format($jd,0,',','.'),0,0,'C',1,1);
$pdf->Cell(2,0.75,$cu,0,0,'C',1,1);
$pdf->Ln();
           foreach($typ as $idx=>$isi)
	       {
   			    $typ[$idx]=0;

		   }
$i++;
}			

if($tot_jd==0)
			  $tot_jd="-";
			if($tot_jd_i==0)
			  $tot_jd_i="-";  
			  
			  if($tot_pus==0)
			  $tot_pus="-";  
	if($tot_pus!=0)
	    $tot_cu=round((($tot_tkbb/$tot_pus)*100),2)."%";
	else
        $tot_cu="0";
		$pdf->SetFont('Arial','B',10);		
$pdf->Cell(4.5,0.75,'JUMLAH',1,0,'C',1);
$pdf->Cell(2.5,0.75,number_format($tot_jd_i,0,',','.'),1,0,'C',1);
$pdf->Cell(2,0.75,number_format($tot_pus,0,',','.'),1,0,'C',1);
           
		   foreach($tot_typ as $idx=>$isi)
	       {
              
				  $pdf->Cell(2,0.75,number_format($isi,0,',','.'),1,0,'C',1); 
		   }	
		   if($tot_tkbb==0)
			    $tot_tkbb="0";    
$pdf->Cell(2,0.75,number_format($tot_tkbb,0,',','.'),1,0,'C',1);  
$pdf->Cell(2,0.75,$tot_cu,1,0,'C',1,1);





$pdf->Output();








?>
