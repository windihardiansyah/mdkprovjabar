<?php
session_start();
if(!isset($_SESSION['un']))
   exit();
include "../koneksi/koneksi.inc.php";
include "../brt_main_cms_1.php";
unset($_SESSION['kki']);
unset($_SESSION['id_fam']);
unset($_SESSION['neigh']);
unset($_SESSION['sts_miskin']);
unset($_SESSION['sts_ks']);

?>
<html>
<head>
<title>menu</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="../js/ajaxlibinduk.js" type="text/javascript"></script>
<script src="../js/prototype.js" type="text/javascript"></script>
<script src="../js/instobj.js" type="text/javascript"></script>
<script src="lib.js" type="text/javascript"></script>
<script src="prop.js" type="text/javascript"></script>
<script src="kab_kota.js" type="text/javascript"></script>
<script src="kec.js" type="text/javascript"></script>
<script src="kel.js" type="text/javascript"></script>
<script src="rw.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/modal-message.css" type="text/css">
<script type="text/javascript" src="js/ajax.js"></script>
<script type="text/javascript" src="js/modal-message.js"></script>
<script type="text/javascript" src="js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="js/lib_m_m.js"></script>
<br>
<table>
<tr>
<?php if($_SESSION['id_area']==9) { ?>
<td>
<a href="#" onClick="rekap_prop()">Rekap Propinsi</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kab_kota()">Rekap Kab/Kota</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kec()">Rekap Kecamatan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kel()">Rekap Desa/Kelurahan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rw()">Rekap Dusun/RW</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>
<?php if($_SESSION['id_area']==10) { ?>
<td>
<a href="#" onClick="rekap_kab_kota()">Rekap Kab/Kota</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kec()">Rekap Kecamatan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kel()">Rekap Desa/Kelurahan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rw()">Rekap Dusun/RW</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>
<?php if($_SESSION['id_area']==11) { ?>
<td>
<a href="#" onClick="rekap_kec()">Rekap Kecamatan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_kel()">Rekap Desa/Kelurahan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rw()">Rekap Dusun/RW</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>
<?php if($_SESSION['id_area']==12) { ?>
<td>
<a href="#" onClick="rekap_kel()">Rekap Desa/Kelurahan</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rw()">Rekap Dusun/RW</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>
<?php if($_SESSION['id_area']==13) { ?>
<td>
<a href="#" onClick="rekap_rw()">Rekap Dusun/RW</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>

<?php if($_SESSION['id_area']==14) { ?>
<td>
<a href="#" onClick="rekap_rt()">Rekap RT</a>
</td>
<?php } ?>
</tr>
</table>

<?php if($_SESSION['id_area']==15) { 
?>
<div id="c_rkp"><?php include "rekap_rt_d.php"; ?></div>
<?php
   } else {
?>
<div id="c_rkp"></div>
<?php
}
?>

<div id="w_d_k" align="center"></div>
<br>
<?php if($_SESSION['id_area']==15) { 
?>
<div id="c_r" align="center"><?php include "rekap_rt_detail.php"; ?></div>
<?php
   } else {
?>
<div id="c_r" align="center"></div>
<?php
}
?>

</body>
<html>