/************************************************************************************************************
Ajax dynamic content
Copyright (C) 2006  DTHMLGoodies.com, Alf Magne Kalleland

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

Dhtmlgoodies.com., hereby disclaims all copyright interest in this script
written by Alf Magne Kalleland.

Alf Magne Kalleland, 2006
Owner of DHTMLgoodies.com
	
************************************************************************************************************/	


var tree_cem_mk = null;
var enableCache = true;
var jsCache = new Array();

var dynamicContent_ajaxObjects = new Array();



function ajax_showContent(divId,ajaxIndex,url)
{
	document.getElementById(divId).innerHTML = dynamicContent_ajaxObjects[ajaxIndex].response;
	if(enableCache){
		jsCache[url] = 	dynamicContent_ajaxObjects[ajaxIndex].response;
	}
	dynamicContent_ajaxObjects[ajaxIndex] = false;
    if($('jmlh_d'))
	{
		$('treeboxbox_tree').innerHTML = '';
	    tree_cem_mk=new dhtmlXTreeObject("treeboxbox_tree","100%","100%",0);
        tree_cem_mk.setImagePath("../codebase/imgs/csh_bluebooks/");
		tree_cem_mk.enableCheckBoxes(1);
		tree_cem_mk.setOnCheckHandler(toncheck);
		
		if($('simpan_menu'))
		{
		tree_cem_mk.setXMLAutoLoading("menu_akses_xml.php");
        tree_cem_mk.loadXML("menu_akses_xml.php?id="+$('id_mn').value);
		$('simpan_menu').onclick=function() { 
		                                   var id = encodeURIComponent($('id_mn').value);		
										   var id_cek=encodeURIComponent(tree_cem_mk.getAllChecked());
										   var urlb="simpan_menu_xml.php?id="+id+"&id_cek="+id_cek+"&r="+new Date().getTime();
 										   ajax_hideTooltip();
						                   displayMessage();
										   new Ajax.Request(
                                           urlb,
                                           {
                                                    method:"get",
			                                        onSuccess: function(r) 
													           {
									                		      messageObj.close();
                                                               },
                                                    onFailure: function() { alert('Gagal'); }
                                           }
                                           );  
		                                  
									   };
		}
									
		if($('simpan_menu_admin'))							
		{
		tree_cem_mk.setXMLAutoLoading("menu_akses_admin_xml.php");
        tree_cem_mk.loadXML("menu_akses_admin_xml.php?id="+$('id_mn').value);
		$('simpan_menu_admin').onclick=function() { 
		                                   var id = encodeURIComponent($('id_mn').value);		
										   var id_cek=encodeURIComponent(tree_cem_mk.getAllChecked());
										   var urlb="simpan_menu_admin_xml.php?id="+id+"&id_cek="+id_cek+"&r="+new Date().getTime();
						                   ajax_hideTooltip();
										   $('w_v_u_a').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses !!!<B></font>"; 				 
	                                       new Ajax.Request(
                                           urlb,
                                           {
                                                   method:"get",
			                                       onSuccess: function(request) 
												              {
									                                if($('id_mn_idx').value==1)
																	{
																	   $('div_id_menu').innerHTML=request.responseText;
																	   var dmx = new DropMenuX('menu1');
                                                                       dmx.delay.show = 0;
                                                                       dmx.init();
																	}
									                                $('w_v_u_a').innerHTML="";
                                                               },
                                                    onFailure: function() { alert('Gagal'); }
                                           }
                                           );  
		                                  
									   };							   
		}
									
		
		$('t_c_c').onclick=function() { tree_cem_mk.closeAllItems(0); };
        $('t_cm_c').onclick=function() { tree_cem_mk.openAllItems(0); };
		
	}

	if($('cem_v'))
	{
		var cem_t_c_c = dhtmlXTreeFromHTML('listBox_admin', "150%", "150%", 0);
		cem_t_c_c.openAllItems(1);
		cem_t_c_c.setOnOpenHandler(fbukatutup);
		$('t_c_c_c').onclick=function() { cem_t_c_c.closeAllItems(0); };
        $('t_cm_c_c').onclick=function() { cem_t_c_c.openAllItems(0); };  

	}
}

function toncheck(id,state)
{
			  if(state==1)
              {
				var c=true; id_c=id;
				tree_cem_mk.setSubChecked(id,true);
				while(c)
				{
				   if(tree_cem_mk.getParentId(id_c))
				   {
				      
					  id_c=tree_cem_mk.getParentId(id_c);
					  tree_cem_mk.setCheck(id_c,true);
				   }
				   else	  
				      c=false;
				}  
			  } 
			  else	
			    tree_cem_mk.setSubChecked(id,false);
			
}


function ajax_loadContent(divId,url)
{
	if(enableCache && jsCache[url]){
		document.getElementById(divId).innerHTML = jsCache[url];
		return;
	}

	
	var ajaxIndex = dynamicContent_ajaxObjects.length;
	document.getElementById(divId).innerHTML = '<IMG src=\'../images/spinner.gif\'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses !!!<B></font>';
	dynamicContent_ajaxObjects[ajaxIndex] = new sack();
	
	if(url.indexOf('?')>=0){
		dynamicContent_ajaxObjects[ajaxIndex].method='GET';
		var string = url.substring(url.indexOf('?'));
		url = url.replace(string,'');
		string = string.replace('?','');
		var items = string.split(/&/g);
		for(var no=0;no<items.length;no++){
			var tokens = items[no].split('=');
			if(tokens.length==2){
				dynamicContent_ajaxObjects[ajaxIndex].setVar(tokens[0],tokens[1]);
			}	
		}	
		url = url.replace(string,'');
	}
	
	dynamicContent_ajaxObjects[ajaxIndex].requestFile = url;	// Specifying which file to get
	dynamicContent_ajaxObjects[ajaxIndex].onCompletion = function(){ ajax_showContent(divId,ajaxIndex,url); };	// Specify function that will be executed after file has been found
	dynamicContent_ajaxObjects[ajaxIndex].runAJAX();		// Execute AJAX function	
	
	
}
