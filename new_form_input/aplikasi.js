(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		
		// deklarasikan variabel
		var kd_mhs = 0;
		var main = "mahasiswa.data.php";
		var main_status_keluarga = "status_keluarga.data.php";
		var main_indikator_keluarga = "indikator_keluarga.data.php";
		
		// tampilkan data mahasiswa dari berkas mahasiswa.data.php 
		// ke dalam <div id="data-mahasiswa"></div>
		$("#data-mahasiswa").load(main);
		$("#data-status-keluarga").load(main_status_keluarga);
		$("#data-indikator").load(main_indikator_keluarga);
		
		// ketika tombol ubah/tambah di tekan
		$('.ubah, .tambah').live("click", function(){
			
			var url = "mahasiswa.form.php";
			// ambil nilai id dari tombol ubah

			kd_mhs = this.id;
			
			if(kd_mhs != 0) {
				// ubah judul modal dialog
				$("#myModalLabel").html("Ubah Data Keluarga");
			} else {
				// saran dari mas hangs 
				$("#myModalLabel").html("Tambah Data Keluarga");
			}	

			$.post(url, {id: kd_mhs} ,function(data) {
				// tampilkan mahasiswa.form.php ke dalam <div class="modal-body"></div>
				$(".modal-body").html(data).show();
			});
		});
		
		// ketika tombol hapus ditekan
		$('.hapus').live("click", function(){
			var url = "mahasiswa.input.php";
			// ambil nilai id dari tombol hapus
			kd_mhs = this.id;
			
			// tampilkan dialog konfirmasi
			var answer = confirm("Apakah anda ingin menghapus data ini?");
			
			// ketika ditekan tombol ok
			if (answer) {
				// mengirimkan perintah penghapusan ke berkas transaksi.input.php
				$.post(url, {hapus: kd_mhs} ,function() {
					// tampilkan data mahasiswa yang sudah di perbaharui
					// ke dalam <div id="data-mahasiswa"></div>
					$("#data-mahasiswa").load(main);
				});
			}
		});
		
		// ketika tombol simpan ditekan
		$("#simpan-mahasiswa").bind("click", function(event) {
			
			var v_status 		= $('input:text[name=status_keluarga]').val();
			if(v_status=="status_keluarga"){
				var url = "status_keluarga.input.php";
				//alert("Nama Harus diisi");		
				var v_alamat 		= $('input:text[name=alamat]').val();
				var v_komplek 		= $('input:text[name=komplek]').val();
				var v_kd_fam 		= $('input:text[name=kd_fam]').val();
				var v_uppks 		= $('input:radio[name=uppks]:checked').val();
				var v_btm 			= $('input:radio[name=btm]:checked').val();
				var v_apbn	 		= $('input:radio[name=apbn]:checked').val();
				var v_apbd 			= $('input:radio[name=apbd]:checked').val();
				var v_krista 		= $('input:radio[name=krista]:checked').val();
				var v_kur 			= $('input:radio[name=kur]:checked').val();
				var v_pnpm 			= $('input:radio[name=pnpm]:checked').val();
				var v_lainnya 		= $('input:radio[name=lainnya]:checked').val();
				var v_pus 			= $('input:radio[name=pus]:checked').val();
				var v_peserta_kb	= $('input:radio[name=peserta_kb]:checked').val();
				var v_pemerintah	= $('input:radio[name=pemerintah]:checked').val();
				var v_alat_kb 		= $('select[name=alat_kb]').val();
				var v_tgl_peserta_kb= $('input:text[name=tgl_peserta_kb]').val();
				var v_alasan 		= $('select[name=alasan]').val();
				var v_usia_kehamilan= $('input:text[name=usia_kehamilan]').val();

				if (v_alamat == ""){
					alert("Alamat Harus diisi");				
					var error = document.getElementById("alamat");
						error.focus();
						// kembalikan judul modal dialog
				}else{

					// mengirimkan data ke berkas transaksi.input.php untuk di proses
					$.post(url, {kd_fam: v_kd_fam, uppks: v_uppks, btm: v_btm, apbn: v_apbn, apbd: v_apbd, 
						krista: v_krista, kur: v_kur, alamat: v_alamat, komplek: v_komplek,
						pnpm: v_pnpm, lainnya: v_lainnya, pus: v_pus, peserta_kb: v_peserta_kb,
						pemerintah: v_pemerintah, alat_kb: v_alat_kb, tgl_peserta_kb: v_tgl_peserta_kb, 
						alasan: v_alasan, usia_kehamilan: v_usia_kehamilan} ,function() {
						// tampilkan data mahasiswa yang sudah di perbaharui
						// ke dalam <div id="data-mahasiswa"></div>
						$("#data-status-keluarga").load(main_status_keluarga);

						// sembunyikan modal dialog
						$('#dialog-mahasiswa').modal('hide');
						
						// kembalikan judul modal dialog
						$("#myModalLabel").html("Update Status Keluarga");
					});
				};

			}
			else if(v_status=="indikator_keluarga"){

				var url = "indikator_keluarga.input.php";

				// mengambil nilai dari inputbox, textbox dan select
				var v_kd_fam 		= $('input:text[name=kd_fam]').val();
				var v_btm = "a";
				
				var v_6 = $('input:radio[name=6]:checked').val();
				var v_7 = $('input:radio[name=7]:checked').val();
				var v_8 = $('input:radio[name=8]:checked').val();
				  	var v_62 = $('input:radio[name=62]:checked').val();
				  	var v_63 = $('input:radio[name=63]:checked').val();
				  	var v_64 = $('input:radio[name=64]:checked').val();
				var v_10 = $('input:radio[name=10]:checked').val();
				var v_11 = $('input:radio[name=11]:checked').val();
				var v_12 = $('input:radio[name=12]:checked').val();
				var v_13 = $('input:radio[name=13]:checked').val();
				var v_14 = $('input:radio[name=14]:checked').val();
				var v_21 = $('input:radio[name=21]:checked').val();
				var v_22 = $('input:radio[name=22]:checked').val();
				var v_23 = $('input:radio[name=23]:checked').val();
				var v_24 = $('input:radio[name=24]:checked').val();
				var v_25 = $('input:radio[name=25]:checked').val();
				var v_26 = $('input:radio[name=26]:checked').val();
				var v_27 = $('input:radio[name=27]:checked').val();
				var v_28 = $('input:radio[name=28]:checked').val();
				var v_29 = $('input:radio[name=29]:checked').val();
				var v_30 = $('input:radio[name=30]:checked').val();
				var v_31 = $('input:radio[name=31]:checked').val();
				var v_32 = $('input:radio[name=32]:checked').val();
				var v_33 = $('input:radio[name=33]:checked').val();
				var v_34 = $('input:radio[name=34]:checked').val();
				var v_35 = $('input:radio[name=35]:checked').val();
				var v_36 = $('input:radio[name=36]:checked').val();
			
				if (v_btm == ""){
					alert(v_62);				
						// kembalikan judul modal dialog
				}else{

					// mengirimkan data ke berkas transaksi.input.php untuk di proses
					$.post(url, {kd_fam: v_kd_fam, v_6: v_6, v_7: v_7, v_8: v_8,
						v_10: v_10, v_11: v_11, 
						v_12: v_12, v_13: v_13, v_14: v_14, 
						v_21: v_21,
						v_22: v_22, v_23: v_23, v_24: v_24, 
						v_25: v_25, v_26: v_26, v_27: v_27, 
						v_28: v_28, v_29: v_29, v_30: v_30, 
						v_31: v_31,
						v_32: v_32, v_33: v_33, v_34: v_34, 
						v_35: v_35, v_36: v_36,
						v_62: v_62, v_63: v_63, v_64: v_64
					} ,function() {
						// tampilkan data mahasiswa yang sudah di perbaharui
						// ke dalam <div id="data-mahasiswa"></div>
						$("#data-indikator").load(main_indikator_keluarga);

						// sembunyikan modal dialog
						$('#dialog-mahasiswa').modal('hide');
						$('#simpan-data').disabled = false;
						
						// kembalikan judul modal dialog
						$("#myModalLabel").html("Update Status Keluarga");
					});
				};

			}else{

			var url = "mahasiswa.input.php";

			// mengambil nilai dari inputbox, textbox dan select
			var v_kd_fam 		= $('input:text[name=kd_fam]').val();
			var v_kd_indv 		= $('input:text[name=kd_indv]').val();
			var v_kd_unik_indv 	= $('input:text[name=kd_unik_indv]').val();
			var v_kd_neigh 		= $('input:text[name=kd_neigh]').val();
			var v_nik 			= $('input:text[name=nik]').val();
			var v_nama 			= $('input:text[name=nama]').val();
			var v_kd_fammbrtyp 	= $('select[name=kd_fammbrtyp]').val();
			var v_kd_gen 		= $('select[name=kd_gen]').val();
			var v_kd_martl 		= $('select[name=kd_martl]').val();
			var v_usiakawin 	= $('input:text[name=usiakawin]').val();

			var v_suratnikah 	= $('input:radio[name=suratnikah]:checked').val();
			var v_tpt_lahir 	= $('input:text[name=tpt_lahir]').val();
			var v_tgl_lahir 	= $('input:text[name=Tgl_lahir]').val();
			var v_kd_edu 		= $('select[name=kd_edu]').val();
			var v_kd_emp 		= $('select[name=kd_emp]').val();
			var v_aktalahir 	= $('input:radio[name=aktalahir]:checked').val();
			var v_kk 		 	= $('input:radio[name=kk]:checked').val();
			var v_ktp 		 	= $('input:radio[name=ktp]:checked').val();
			var v_posyandu	 	= $('input:radio[name=posyandu]:checked').val();
			var v_bkb 		 	= $('input:radio[name=bkb]:checked').val();
			var v_bkr 		 	= $('input:radio[name=bkr]:checked').val();
			var v_bkl 		 	= $('input:radio[name=bkl]:checked').val();
			var v_pik 		 	= $('input:radio[name=pik]:checked').val();
			var v_kd_mutasi		= $('select[name=kd_mutasi]').val();
			var v_tgl_mutasi 	= $('input:text[name=tgl_mutasi]').val();

			// var v_alamat = $('textarea[name=alamat]').val();
			// var v_kelas = $('select[name=kelas]').val();
			// var v_status = $('select[name=status]').val();

				if (v_nama == ""){
					alert("Nama Harus diisi");				
						// kembalikan judul modal dialog
						var error = document.getElementById("nama");
						error.focus();
				}else{

					// mengirimkan data ke berkas transaksi.input.php untuk di proses
					$.post(url, {kd_fam: v_kd_fam, kd_indv: v_kd_indv, kd_unik_indv: v_kd_unik_indv, kd_neigh: v_kd_neigh, nik: v_nik, nama: v_nama, id: kd_mhs,
						kd_fammbrtyp: v_kd_fammbrtyp, kd_gen: v_kd_gen, kd_martl: v_kd_martl, usiakawin: v_usiakawin,
						suratnikah: v_suratnikah, tpt_lahir: v_tpt_lahir, tgl_lahir: v_tgl_lahir, kd_edu: v_kd_edu, kd_emp: v_kd_emp,
						aktalahir: v_aktalahir, kk: v_kk, ktp: v_ktp, bkb: v_bkb, bkr: v_bkr, posyandu: v_posyandu,
						bkl: v_bkl, pik: v_pik, kd_mutasi: v_kd_mutasi, tgl_mutasi: v_tgl_mutasi} ,function() {
						// tampilkan data mahasiswa yang sudah di perbaharui
						// ke dalam <div id="data-mahasiswa"></div>
						$("#data-mahasiswa").load(main);

						// sembunyikan modal dialog
						$('#dialog-mahasiswa').modal('hide');
						
						// kembalikan judul modal dialog
						$("#myModalLabel").html("Tambah Data Keluarga");
					});
				};

			}
		});

		$('.ubahb, .tambahb').live("click", function(){
			
			var url = "status_keluarga.form.php";
			// ambil nilai id dari tombol ubah

			kd_mhs = this.id;
			
			if(kd_mhs != 0) {
				// ubah judul modal dialog
				$("#myModalLabel").html("Ubah Status Keluarga");
			} else {
				// saran dari mas hangs 
				$("#myModalLabel").html("Tambah Status Keluarga");
			}	

			$.post(url, {id: kd_mhs} ,function(data) {
				// tampilkan mahasiswa.form.php ke dalam <div class="modal-body"></div>
				$(".modal-body").html(data).show();
			});
		});

		$('.ubahc, .tambahc').live("click", function(){
			
			var url = "indikator_keluarga.form.php";
			// ambil nilai id dari tombol ubah

			kd_mhs = this.id;
			
			if(kd_mhs != 0) {
				// ubah judul modal dialog
				$("#myModalLabel").html("Ubah Indikator Keluarga");
			} else {
				// saran dari mas hangs 
				$("#myModalLabel").html("Tambah Indikator Keluarga");
			}	

			$.post(url, {id: kd_mhs} ,function(data) {
				// tampilkan mahasiswa.form.php ke dalam <div class="modal-body"></div>
				$(".modal-body").html(data).show();
			});
		});

		$("#hapus-data").bind("click", function(event) {
			var url = "hapus_data.php";
			var kd_fam = $("#inputString").val();
			var length = kd_fam.length;
			// ambil nilai id dari tombol hapus
			if(kd_fam=="" || kd_fam==null){
				alert("No KKI tidak boleh kosong...!");
				var error = document.getElementById("inputString");
					error.focus();
			}else{
			
				// tampilkan dialog konfirmasi
				var answer = confirm("Apakah anda ingin menghapus data ini dengan Kode Family "+kd_fam+" ?");
				
				// // ketika ditekan tombol ok
				if (answer) {
					var butt = document.getElementById("hapus-data");
						butt.disabled = true;

					$.post(url, {hapus: kd_fam} ,function() {
						//reload data
						alert("Data "+kd_fam+" Berhasil Dihapus");
						location.reload();
					});
				}
			}
		});

	});
}) (jQuery);
