<style>
	.datepicker{z-index:1151;}
</style>
<script>
	$(function(){
	   
		$("#tgl_peserta_kb").datepicker({
			format:'yyyy-mm-dd'
		});

		// $("#bantuan_modal").hide();
		// $("#pasangan_usia_subur").hide();
		// $("#div_peserta_kb_y").hide();
		// $("#div_peserta_kb_n").hide();
		
		$("#btm_y").change(function(){
			var apbn_y = document.getElementById("apbn_y");
		        apbn_y.disabled = false;
		    var apbn_n = document.getElementById("apbn_n");
		        apbn_n.disabled = false;
		    var apbd_y = document.getElementById("apbd_y");
		        apbd_y.disabled = false;
		    var apbd_n = document.getElementById("apbd_n");
		        apbd_n.disabled = false;
		    var krista_y = document.getElementById("krista_y");
		        krista_y.disabled = false;
		    var krista_n = document.getElementById("krista_n");
		        krista_n.disabled = false;
		    var kur_y = document.getElementById("kur_y");
		        kur_y.disabled = false;
		    var kur_n = document.getElementById("kur_n");
		        kur_n.disabled = false;
		    var pnpm_y = document.getElementById("pnpm_y");
		        pnpm_y.disabled = false;
		    var pnpm_n = document.getElementById("pnpm_n");
		        pnpm_n.disabled = false;
		    var lainnya_y = document.getElementById("lainnya_y");
		        lainnya_y.disabled = false;
		    var lainnya_n = document.getElementById("lainnya_n");
		        lainnya_n.disabled = false;
		});
		$("#btm_n").change(function(){
			var apbn_y = document.getElementById("apbn_y");
		        apbn_y.disabled = true;
		    var apbn_n = document.getElementById("apbn_n");
		        apbn_n.disabled = true; apbn_n.checked = true;
		    var apbd_y = document.getElementById("apbd_y");
		        apbd_y.disabled = true;
		    var apbd_n = document.getElementById("apbd_n");
		        apbd_n.disabled = true; apbd_n.checked = true;
		    var krista_y = document.getElementById("krista_y");
		        krista_y.disabled = true;
		    var krista_n = document.getElementById("krista_n");
		        krista_n.disabled = true; krista_n.checked = true;
		    var kur_y = document.getElementById("kur_y");
		        kur_y.disabled = true;
		    var kur_n = document.getElementById("kur_n");
		        kur_n.disabled = true; kur_n.checked = true;
		    var pnpm_y = document.getElementById("pnpm_y");
		        pnpm_y.disabled = true;
		    var pnpm_n = document.getElementById("pnpm_n");
		        pnpm_n.disabled = true; pnpm_n.checked = true;
		    var lainnya_y = document.getElementById("lainnya_y");
		        lainnya_y.disabled = true;
		    var lainnya_n = document.getElementById("lainnya_n");
		        lainnya_n.disabled = true; lainnya_n.checked = true;
		});

		$("#pus_y").change(function(){
			var peserta_kb_y = document.getElementById("peserta_kb_y");
		        peserta_kb_y.disabled = false;
		    var peserta_kb_n = document.getElementById("peserta_kb_n");
		        peserta_kb_n.disabled = false;
		});
		$("#pus_n").change(function(){
			var peserta_kb_y = document.getElementById("peserta_kb_y");
		        peserta_kb_y.disabled = true; peserta_kb_y.checked = false; 
		    var peserta_kb_n = document.getElementById("peserta_kb_n");
		        peserta_kb_n.disabled = true; peserta_kb_n.checked = false; 
		    var pemerintah = document.getElementById("pemerintah");
		        pemerintah.disabled = true; pemerintah.checked = false; 
		    var swasta = document.getElementById("swasta");
		        swasta.disabled = true; swasta.checked = false;
		    var alat_kb = document.getElementById("alat_kb");
		        alat_kb.disabled = true; alat_kb.value = "";
		    var tgl_peserta_kb = document.getElementById("tgl_peserta_kb");
		        tgl_peserta_kb.disabled = true; tgl_peserta_kb.value = "";
		    var alasan = document.getElementById("alasan");
		        alasan.disabled = true; alasan.value = "";
		    var usia_kehamilan = document.getElementById("usia_kehamilan");
		        usia_kehamilan.disabled = true; usia_kehamilan.value = "";
		});

		$("#peserta_kb_y").change(function(){
		    var pemerintah = document.getElementById("pemerintah");
		        pemerintah.disabled = false;
		    var swasta = document.getElementById("swasta");
		        swasta.disabled = false;
		    var alat_kb = document.getElementById("alat_kb");
		        alat_kb.disabled = false;
		    var tgl_peserta_kb = document.getElementById("tgl_peserta_kb");
		        tgl_peserta_kb.disabled = false;

		    var alasan = document.getElementById("alasan");
		        alasan.disabled = true; alasan.value = "";
		    var usia_kehamilan = document.getElementById("usia_kehamilan");
		        usia_kehamilan.disabled = true; usia_kehamilan.value = "";
		});
		$("#peserta_kb_n").change(function(){
			var pemerintah = document.getElementById("pemerintah");
		        pemerintah.disabled = true; pemerintah.checked = false; 
		    var swasta = document.getElementById("swasta");
		        swasta.disabled = true; swasta.checked = false;
		    var alat_kb = document.getElementById("alat_kb");
		        alat_kb.disabled = true; alat_kb.value = "";
		    var tgl_peserta_kb = document.getElementById("tgl_peserta_kb");
		        tgl_peserta_kb.disabled = true; tgl_peserta_kb.value = "";

			var alasan = document.getElementById("alasan");
		        alasan.disabled = false;  
		    // var usia_kehamilan = document.getElementById("usia_kehamilan");
		    //     usia_kehamilan.disabled = false;  
		        
		});
		$("#alasan").change(function(){
			if ($("#alasan").val() == 1){
				var usia_kehamilan = document.getElementById("usia_kehamilan");
		        	usia_kehamilan.disabled = false;
			}else{
				var usia_kehamilan = document.getElementById("usia_kehamilan");
		        	usia_kehamilan.disabled = true;
			}
		});

	});
</script>
<?php 
	require 'koneksi.php';
	$apbn_n 	= "checked";
	$apbd_n 	= "checked";
	$krista_n 	= "checked";
	$kur_n 		= "checked";
	$pnpm_n 	= "checked";
	$lainnya_n 	= "checked";

	$data = mysql_fetch_array(mysql_query("SELECT * FROM dbo_family_test"));
	$alamat		= $data['alamat'];
	$komplek 	= $data['komplek'];
	$Kd_fam 	= $data['Kd_fam'];
	$usiahamil 	= $data['usiahamil'];
	$tgl_kb 	= $data['tgl_kb'];
	$uppks	 	= $data['uppks'];
	$Kd_contyp 	= $data['Kd_contyp']; 
	$Kd_nonacptr 	= $data['Kd_nonacptr'];
	if ($uppks==1){
		$uppks_y = "checked"; $uppks_n = "null";
	}else{$uppks_y = "null";  $uppks_n = "checked";}
	$btm	 	= $data['Bantuan_modal'];
	if ($btm==1){
		$btm_y = "checked"; $btm_n = "null";
		$dis_apbn_y = NULL; $dis_apbd_y = NULL; $dis_krista_y = NULL; $dis_kur_y = NULL; $dis_pnpm_y = NULL; $dis_lainnya_y = NULL;
		$dis_apbn_n = NULL; $dis_apbd_n = NULL; $dis_krista_n = NULL; $dis_kur_n = NULL; $dis_pnpm_n = NULL; $dis_lainnya_n = NULL;
	}else{$btm_y = "null";  $btm_n = "checked";
		$dis_apbn_y = "disabled"; $dis_apbd_y = "disabled"; $dis_krista_y = "disabled"; $dis_kur_y = "disabled"; $dis_pnpm_y = "disabled"; $dis_lainnya_y = "disabled";
		$dis_apbn_n = "disabled"; $dis_apbd_n = "disabled"; $dis_krista_n = "disabled"; $dis_kur_n = "disabled"; $dis_pnpm_n = "disabled"; $dis_lainnya_n = "disabled";
	}
	$apbn	 	= $data['apbn'];
	if ($apbn==1){
		  $apbn_y = "checked"; $apbn_n = "null";
	}else{$apbn_y = "null";  $apbn_n = "checked";}
	$apbd	 	= $data['apbd'];
	if ($apbd==1){
		  $apbd_y = "checked"; $apbd_n = "null";
	}else{$apbd_y = "null";  $apbd_n = "checked";}
	$krista	 	= $data['krista'];
	if ($krista==1){
		  $krista_y = "checked"; $krista_n = "null";
	}else{$krista_y = "null";  $krista_n = "checked";}
	$kur	 	= $data['kur'];
	if ($kur==1){
		  $kur_y = "checked"; $kur_n = "null";
	}else{$kur_y = "null";  $kur_n = "checked";}
	$pnpm	 	= $data['pnpm'];
	if ($pnpm==2){
		  $pnpm_y = "checked"; $pnpm_n = "null";
	}else{$pnpm_y = "null";  $pnpm_n = "checked";}
	$lainnya 	= $data['lainnya'];
	if ($lainnya==1){
		  $lainnya_y = "checked"; $lainnya_n = "null";
	}else{$lainnya_y = "null";  $lainnya_n = "checked";}
	$pus 	= $data['pus'];
	$Kd_consrc 	= $data['Kd_consrc']; 

	if ($pus==1){
		  $pus_y = "checked"; $pus_n = "null";
		  $dis_peserta_kb_y=NULL; $dis_peserta_kb_n=NULL; $dis_pemerintah = NULL; $dis_swasta=NULL; $dis_alat_kb=NULL; $dis_tgl_peserta_kb=NULL; $dis_alasan=NULL; $dis_usia_kehamilan=NULL;
		  if($Kd_contyp>0 || $Kd_consrc>0){
		  	$peserta_kb_y = "checked"; $dis_alasan="disabled"; $dis_usia_kehamilan="disabled";
		  }else{
		  	$peserta_kb_n = "checked"; $dis_pemerintah = "disabled"; $dis_swasta="disabled"; $dis_alat_kb="disabled"; $dis_tgl_peserta_kb="disabled";
		  }
	}else{$pus_y = "null";  $pus_n = "checked";
		$dis_peserta_kb_y="disabled"; $dis_peserta_kb_n="disabled"; $dis_pemerintah = "disabled"; $dis_swasta="disabled"; $dis_alat_kb="disabled"; $dis_tgl_peserta_kb="disabled"; $dis_alasan="disabled"; $dis_usia_kehamilan="disabled";
	} 
	
	if ($Kd_consrc==1){
		  $pemerintah = "checked"; $swasta = NULL;
	}elseif($Kd_consrc==2){$pemerintah = NULL;  $swasta = "checked";
	}else{$pemerintah = NULL; $swasta = NULL;}


?>
<form class="form-horizontal" id="form-mahasiswa">
	<div class="control-group">
		<label class="control-label" for="alamat">Alamat *</label>
		<div class="controls">
			<input type="text" id="alamat" class="input-large" name="alamat" value="<?php echo $alamat ?>">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="komplek">Komplek</label>
		<div class="controls">
			<input type="text" id="komplek" class="input-medium" name="komplek" value="<?php echo $komplek ?>">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="uppks">UPPKS</label>
		<div class="controls">
			<label class="radio">
			  <input type="radio" name="uppks" id="uppks_y" value="1" <?php echo $uppks_y ?> >Ya
			</label>
			<label class="radio">
			  <input type="radio" name="uppks" id="uppks_n" value="0" <?php echo $uppks_n ?> >Tidak
			</label>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="btm">Bantuan Modal</label>
		<div class="controls">
			<label class="radio">
			  <input type="radio" name="btm" id="btm_y" value="1" <?php echo $btm_y ?> >Ya
			</label>
			<label class="radio">
			  <input type="radio" name="btm" id="btm_n" value="0" <?php echo $btm_n ?> >Tidak
			</label>
		</div>
	</div>
	<!-- bantuan modal -->
	<div id="bantuan_modal" name="bantuan_modal">
	<table border="1">
	  <tr>
		  <td>
			<label class="control-label" for="apbn">APBN</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="apbn" id="apbn_y" value="1"  <?php echo $apbn_y.' '.$dis_apbn_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="apbn" id="apbn_n" value="2"  <?php echo $apbn_n.' '.$dis_apbn_n; ?> >Tidak
				</label>
			</div>
		  </td>
		  <td>
			<label class="control-label" for="apbd">APBD</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="apbd" id="apbd_y" value="1"  <?php echo $apbd_y.' '.$dis_apbd_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="apbd" id="apbd_n" value="2"  <?php echo $apbd_n.' '.$dis_apbd_n; ?> >Tidak
				</label>
			</div>
		  </td>
	  </tr>
	  <tr>
		  <td>
			<label class="control-label" for="krista">KRISTA</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="krista" id="krista_y" value="1"  <?php echo $krista_y.' '.$dis_krista_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="krista" id="krista_n" value="2"  <?php echo $krista_n.' '.$dis_krista_n; ?> >Tidak
				</label>
			</div>
		  </td>
		  <td>
			<label class="control-label" for="kur">KUR</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="kur" id="kur_y" value="1"  <?php echo $kur_y.' '.$dis_kur_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="kur" id="kur_n" value="2"  <?php echo $kur_n.' '.$dis_kur_n; ?> >Tidak
				</label>
			</div>
		  </td>
	  </tr>
	  <tr>
		  <td>
			<label class="control-label" for="pnpm">PNPM</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="pnpm" id="pnpm_y" value="2"  <?php echo $pnpm_y.' '.$dis_pnpm_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="pnpm" id="pnpm_n" value="1"  <?php echo $pnpm_n.' '.$dis_pnpm_n; ?> >Tidak
				</label>
			</div>
		  </td>
		  <td>
			<label class="control-label" for="lainnya">LAINNYA</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="lainnya" id="lainnya_y" value="1"  <?php echo $lainnya_y.' '.$dis_lainnya_y; ?> >Ya
				</label>
				<label class="radio">
				  <input type="radio" name="lainnya" id="lainnya_n" value="2"  <?php echo $lainnya_n.' '.$dis_lainnya_n; ?> >Tidak
				</label>
			</div>
		  </td>
	  </tr>
	</table>
	</div>
	<!-- end bantuan modal -->

	<div class="control-group">
		<label class="control-label" for="pus">PUS</label>
		<div class="controls">
			<label class="radio">
			  <input type="radio" name="pus" id="pus_y" value="1" <?php echo $pus_y ?> >Ya
			</label>
			<label class="radio">
			  <input type="radio" name="pus" id="pus_n" value="0" <?php echo $pus_n ?> >Tidak
			</label>
		</div>
	</div>
	<!-- pasangan_usia_subur -->
	<div id="pasangan_usia_subur" name="pasangan_usia_subur">
	<table border="1">
	  <tr>
		  <td>
			<label class="control-label" for="peserta_kb">Peserta KB</label>
			<div class="controls">
				<label class="radio">
				  <input type="radio" name="peserta_kb" id="peserta_kb_y" value="2" <?php echo $peserta_kb_y.' '.$dis_peserta_kb_y ?> >Ya
				</label>
				<div name="div_peserta_kb_y" id="div_peserta_kb_y">
					<label class="control-label" for="tpt_kb">Tempat Pelayanan KB:</label>
					<div class="controls">
						<label class="radio">
						  <input type="radio" name="pemerintah" id="pemerintah" value="1" <?php echo $pemerintah.' '.$dis_pemerintah ?> >Pemerintah
						</label>
						<label class="radio">
						  <input type="radio" name="pemerintah" id="swasta" value="2" <?php echo $swasta.' '.$dis_swasta ?> >Swasta
						</label>
					</div>
					<label class="control-label" for="alat_kb">Jenis Alat KB:</label>
					<div class="controls">
                      <select class="input-medium" name="alat_kb" id="alat_kb" <?php echo $dis_alat_kb; ?> >
                        <option value="">------ Pilih ------</option>
                        <?php
                          $get= query("select * from dbo_contr_typ");
                          while($r = assoc($get)):
                          	if($r['Kd_contyp']==$Kd_contyp){ echo $se="selected"; } else { $se=""; }
                            echo '<option value="'.$r['Kd_contyp'].'" '.$se.'>'.$r['Nm_contyp_ind'].'</option>';
                          endWhile;
                        ?>
                      </select>    
					</div>
					<br>
					<label class="control-label" for="tgl_peserta_kb">Kapan Menjadi Peserta KB (Metode Kontrasepsi Yang Terakhir Dipakai):</label>
					<div class="controls">
						<input class="input-medium" type="text" id="tgl_peserta_kb" name="tgl_peserta_kb" placeholder="tanggal terakhir" value="<?php echo $tgl_kb ?>" <?php echo $dis_tgl_peserta_kb; ?> >					
					</div>
					<br><br><br>
				</div>
				<label class="radio">
				  <input type="radio" name="peserta_kb" id="peserta_kb_n" value="1"  <?php echo $peserta_kb_n.' '.$dis_peserta_kb_n ?> >Tidak
				</label>
				  <div name="div_peserta_kb_n" id="div_peserta_kb_n">
					<label class="control-label" for="alasan">Alasan:</label>
					<div class="controls">
						<select class="input-medium" name="alasan" id="alasan" <?php echo $alasan.' '.$dis_alasan ?> >
							<option value="">------ Pilih ------</option>
	                      <?php
	                        $get= query("select * from dbo_non_acptr_reas");
	                        while($r = assoc($get)):
	                          if($r['Kd_nonacptr']==$Kd_nonacptr){ echo $se="selected"; } else { $se=""; }
	                          echo '<option value="'.$r['Kd_nonacptr'].'" '.$se.'>'.$r['Nm_nonacptr_ind'].'</option>';
	                        endWhile;
	                      ?>
	                    </select>
					</div><br>
					<label class="control-label" for="usia_kehamilan" id="lbl_usia" name="lbl_usia">Usia Kehamilan Minggu Ke-:</label>
					<div class="controls">
						<input class="input-mini" type="text" id="usia_kehamilan" name="usia_kehamilan" placeholder="Usia Kehamilan" value="<?php echo $usiahamil ?>" <?php echo $dis_usia_kehamilan ?> >					
					</div>
				  </div>	
			</div>
		  </td>
	  </tr>
	</table>
	</div>
	<br>
	<div class="control-group">
		<label class="control-label" for="kd_indv">Kd.Fam</label>
		<div class="controls">
			<input type="text" id="kd_fam" class="input-medium" name="kd_fam" value="<?php echo $Kd_fam ?>" disabled>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="status_keluarga">Status Keluarga</label>
		<div class="controls">
			<input type="text" id="status_keluarga" class="input-medium" name="status_keluarga" value="status_keluarga" disabled>
		</div>
	</div>

</form>