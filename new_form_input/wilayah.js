$(document).ready(function(){
  // $("#loader").hide();
  // $("#loader_main").hide();
  var kota = document.getElementById("kota");
  var kecamatan = document.getElementById("kecamatan");
  var desa = document.getElementById("desa");
  var rw = document.getElementById("rw");
  var rt = document.getElementById("rt");

  // =============================== Wilayah Delete =======================
  var kota_del      = document.getElementById("kota_del");
  var kecamatan_del = document.getElementById("kecamatan_del");
  var desa_del      = document.getElementById("desa_del");
  var rw_del        = document.getElementById("rw_del");
  var rt_del        = document.getElementById("rt_del");

  // =============================== Wilayah Delete =======================
  var kota_update      = document.getElementById("kota_update");
  var kecamatan_update = document.getElementById("kecamatan_update");
  var desa_update      = document.getElementById("desa_update");
  var rw_update        = document.getElementById("rw_update");
  var rt_update        = document.getElementById("rt_update");
  
  $("#data-tabel").hide();
  $("#main-center").hide();
  $("#button-add").hide();
  $("#form-isi-hapus").hide();
  $("#form-isi-update").hide();

  $("#propinsi").change(function(){
        $("#data-tabel").fadeOut();        
        $("#main-center").fadeOut();
        $("#button-add").fadeOut();

        var propinsi = $("#propinsi").val();
        var label_prop = document.getElementById("label-prop");
        label_prop.value = propinsi;

        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota").html(data);  
                $("#kecamatan").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw").html("<option value=''>--Pilih RW--</option>");
                $("#rt").html("<option value=''>--Pilih RT--</option>");
                
            }
        });
  });

  $("#kota").change(function(){
        $("#data-tabel").fadeOut();        
        $("#main-center").fadeOut();
        $("#button-add").fadeOut();
        var kota = $("#kota").val();
        var label_kota = document.getElementById("label-kota");
        label_kota.value = kota.substring(2,4);

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan").html(data);
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw").html("<option value=''>--Pilih RW--</option>");
                $("#rt").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#kecamatan").change(function(){
        $("#data-tabel").fadeOut();        
        $("#main-center").fadeOut();
        $("#button-add").fadeOut();
        // $("#data-mahasiswa").fadeOut();
        var kecamatan = $("#kecamatan").val();
        var label_kec = document.getElementById("label-kec");
        label_kec.value = kecamatan.substring(4,6);

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa").html(data);
                $("#rw").html("<option value=''>--Pilih RW--</option>");
                $("#rt").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#desa").change(function(){
        $("#button-add").fadeOut();
        // $("#data-mahasiswa").fadeOut();
        var desa = $("#desa").val();
        var label_kel = document.getElementById("label-kel");
        label_kel.value = desa.substring(6,9);

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + desa + "&kode=RW",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rw").html(data);
                $("#rt").html("<option value=''>--Pilih RT--</option>");
            }
        });
        $.ajax({
            url: "main.php",
            data: "wil_id=" + desa + "&kode=RW",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#main-center").html(data);
                // $("#loader_main").fadeOut(500);
                $("#data-tabel").fadeIn(1000);
                $("#main-center").fadeIn(1000);
                // $("#form_individu").fadeOut();
            }
        });
  });

  $("#rw").change(function(){
        $("#button-add").fadeOut();
        var rw = $("#rw").val();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + rw + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rt").html(data);
            }
        });
  });

  $("#rt").change(function(){
        $("#button-add").fadeOut();
        var rt = $("#rt").val();
        $.ajax({
            url: "main.php",
            data: "wil_id=" + rt + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#main-center").html(data);
                $("#button-add").fadeIn(500);
                // $("#data-mahasiswa").fadeIn(500);
            }
        });
  });
  
 //======================= Wilayah DELETE =========================================================
 $("#propinsi_del").change(function(){
        var propinsi = $("#propinsi_del").val();
        $("#form-isi-hapus").fadeOut();
        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota_del").html(data);  
                $("#kecamatan_del").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa_del").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw_del").html("<option value=''>--Pilih RW--</option>");
                $("#rt_del").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#kota_del").change(function(){
        var kota = $("#kota_del").val();
        $("#form-isi-hapus").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan_del").html(data);
                $("#desa_del").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw_del").html("<option value=''>--Pilih RW--</option>");
                $("#rt_del").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#kecamatan_del").change(function(){
        var kecamatan = $("#kecamatan_del").val();
        $("#form-isi-hapus").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa_del").html(data);
                $("#rw_del").html("<option value=''>--Pilih RW--</option>");
                $("#rt_del").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#desa_del").change(function(){
        var desa = $("#desa_del").val();
        $("#form-isi-hapus").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + desa + "&kode=RW",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rw_del").html(data);
                $("#rt_del").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#rw_del").change(function(){
        var rw = $("#rw_del").val();
        $("#form-isi-hapus").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + rw + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rt_del").html(data);
            }
        });
  });

  $("#rt_del").change(function(){
        var rt = $("#rt_del").val();
        $("#form-isi-hapus").fadeIn();
        $.ajax({
            url: "main.php",
            data: "wil_id=" + rt + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                //$("#main-center").html(data);
            }
        });
  });
  

  //======================= Wilayah UPDATE =========================================================
 $("#propinsi_update").change(function(){
        var propinsi = $("#propinsi_update").val();
        $("#form-isi-update").fadeOut();
        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota_update").html(data);  
                $("#kecamatan_update").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa_update").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw_update").html("<option value=''>--Pilih RW--</option>");
                $("#rt_update").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#kota_update").change(function(){
        var kota = $("#kota_update").val();
        $("#form-isi-update").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan_update").html(data);
                $("#desa_update").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw_update").html("<option value=''>--Pilih RW--</option>");
                $("#rt_update").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#kecamatan_update").change(function(){
        var kecamatan = $("#kecamatan_update").val();
        $("#form-isi-update").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa_update").html(data);
                $("#rw_update").html("<option value=''>--Pilih RW--</option>");
                $("#rt_update").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#desa_update").change(function(){
        var desa = $("#desa_update").val();
        $("#form-isi-update").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + desa + "&kode=RW",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rw_update").html(data);
                $("#rt_update").html("<option value=''>--Pilih RT--</option>");
            }
        });
  });

  $("#rw_update").change(function(){
        var rw = $("#rw_update").val();
        $("#form-isi-update").fadeOut();
        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + rw + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#rt_update").html(data);
            }
        });
  });

  $("#rt_update").change(function(){
        var rt = $("#rt_update").val();
        $("#form-isi-update").fadeIn();
        $.ajax({
            url: "main.php",
            data: "wil_id=" + rt + "&kode=RT",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                //$("#main-center").html(data);
            }
        });
  });
});