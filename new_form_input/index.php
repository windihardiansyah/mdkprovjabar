<html>
<head>
<title>New Form Input</title>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="css/datepicker.css">
	<script src="js/jquery-1.8.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="wilayah.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	
	<script type="text/javascript">
	 	$(document).ready(function () {
		    $('.nav').tooltip({
		    	selector: "a[data-toggle=tooltip]"
		    });
		});
		function lookup(inputString) {
		    var rt = $("#rt_del").val();
		    if(inputString.length == 0) {
		      // Hide the suggestion box.
		      $('#suggestions').hide();
		    } else {
		      $.post("rpc.php?wilayah="+rt, {queryString: ""+inputString+""}, function(data){
		        if(data.length >0) {
		          $('#suggestions').show();
		          $('#autoSuggestionsList').html(data);
		        }
		      });
		    }
		  } // lookup
		  
		function fill(thisValue) {
		    $('#inputString').val(thisValue);
		    setTimeout("$('#suggestions').hide();", 200);
		}

		function lookup_update(inputString_update) {
		    var rt = $("#rt_update").val();
		    if(inputString_update.length == 0) {
		      // Hide the suggestion box.
		      $('#suggestions_update').hide();
		    } else {
		      $.post("rpc.php?wilayah="+rt, {queryString_update: ""+inputString_update+""}, function(data){
		        if(data.length >0) {
		          $('#suggestions_update').show();
		          $('#autoSuggestionsList_update').html(data);
		        }
		      });
		    }
		} // lookup
		  
		function fill_update(thisValue) {
		    $('#inputString_update').val(thisValue);
		    setTimeout("$('#suggestions_update').hide();", 200);
		}
	</script>
	<style type="text/css">
		.suggestionsBox {
			position: relative;
			left: 250px;
			margin: 10px 0px 0px 0px;
			width: 300px;
			background-color: #212427;
			-moz-border-radius: 7px;
			-webkit-border-radius: 7px;
			border: 2px solid #000;	
			color: #fff;
		}
		
		.suggestionList {
			margin: 0px;
			padding: 0px;
		}
		
		.suggestionList li {
			
			margin: 0px 0px 3px 0px;
			padding: 3px;
			cursor: pointer;
		}
		
		.suggestionList li:hover {
			background-color: #659CD8;
		}
	</style>
</head>
<body data-target=".bs-docs-sidebar" data-spy="scroll" data-twttr-rendered="true">
	<div class="navbar navbar-inverse navbar-fixed-top">
	  <div class="navbar-inner">
	    <div class="container">
	      <a class="brand">BKKBN Provinsi Jawa Barat</a>
	    </div>
	  </div>
	</div>
	<header id="overview" class="jumbotron subhead">
		<div class="container">
			<br><br>
			<h2>Form Data Keluarga (Demo)</h2>
		</div>
	</header>
	<div class="container">
	<?php 
		include "koneksi.php";
		
		$last_kd_fam = mysql_fetch_array(mysql_query("SELECT Kd_fam FROM dbo_family_test"));
		if($last_kd_fam[Kd_fam]!=NULL || $last_kd_fam[Kd_fam]!=""){
			$cek_indikator = mysql_fetch_array(mysql_query("SELECT count(Kd_fam) as jml FROM dbo_fam_ind_detail where Kd_fam='$last_kd_fam[Kd_fam]' "));

			if($cek_indikator[jml]==0){
				mysql_query("DELETE FROM dbo_individu WHERE Kd_fam='$last_kd_fam[Kd_fam]'  "); //Delete Individu
				mysql_query("DELETE FROM dbo_fam_ind_detail WHERE Kd_fam='$last_kd_fam[Kd_fam]'  "); //delete fam indikator
				mysql_query("DELETE FROM dbo_family WHERE Kd_fam='$last_kd_fam[Kd_fam]' "); //delete family				
			}
		}

		mysql_query("DELETE FROM dbo_individu_test  "); //Delete Individu
		mysql_query("DELETE FROM dbo_fam_ind_detail_test  "); //delete fam indikator
		mysql_query("DELETE FROM dbo_family_test "); //delete family	
	?>
	    <div class="tab-pane active" id="tabs-basic">
			<div class="tabbable">
				<ul class="nav nav-tabs" id="tabes">
					<li class="active"><a href="#tabs1-add-data" data-toggle="tab">Input Data Keluarga</a></li>
					<li><a href="#tabs1-update-data" data-toggle="tab">Update Data Keluarga</a></li>
					<li><a href="#tabs1-delete-data" data-toggle="tab">Delete Data Keluarga</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tabs1-add-data">
						<h4>Tambah Data Keluarga</h4>
						<div class="container">
						  <table>
						    <tr>
						      <td>
						        <label class="control-label" for="Propinsi">Propinsi : </label>
						      </td>
						      <td>
						        <select name="propinsi" id="propinsi" class="span2">
						          <option value="">--Pilih Propinsi--</option>
						          <?php

						            // tampilkan nama-nama propinsi yang ada di database
						            $sql = mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit=9 ORDER BY no_unit_detail");
						            while($p=mysql_fetch_array($sql)){
						              echo "<option value=$p[id_unik]>$p[no_unit_detail]</option> \n";
						            }
						          ?>
						        </select>
						      </td>
						      <td>
						        <label id="lkota" class="control-label" for="Kota">Kota :</label>
						      </td>
						      <td>
						        <select name="kota" id="kota" class="span2">
						          <option value="">--Pilih Kota--</option>
						        </select>
						      </td>
						      <td>
						        <label id="lkecamatan" class="control-label" for="Kecamatan">Kecamatan :</label>
						      </td>
						      <td>
						        <select name="kecamatan" id="kecamatan" class="span2">
						          <option value="">--Pilih Kecamatan--</option>
						        </select>
						      </td>
						    </tr>
						    <tr>
						      <td>
						        <label id="ldesa" class="control-label" for="Kelurahan">Kelurahan :</label>
						      </td>
						      <td>  
						        <select name="desa" id="desa" class="span2">
						          <option value="">--Pilih Kelurahan--</option>
						        </select>
						      </td>
						      <td>
						        <label id="lrw" class="control-label" for="rw">RW :</label>
						      </td>
						      <td>
						        <select name="rw" id="rw" class="span2">
						          <option value="">--Pilih RW--</option>
						        </select>
						      </td>
						      <td>
						        <label id="lrt" class="control-label" for="rt">RT :</label>
						      </td>
						      <td>
						        <select name="rt" id="rt" class="span2">
						          <option value="">--Pilih RT--</option>
						        </select>
						      </td>
						    </tr>
						   </table>
						   </div>
						<div class="container" id="alamat">
							<div class="control-group">
								<label>Alamat *</label>
								<div class="controls">
									<input type="text" id="alamat" class="input-large" name="alamat">
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="alamat">Kode KKI</label>
								<div class="controls">
									<input type="text" id="label-prop" name="label-prop" class="input-mini">
									<input type="text" id="label-kota" name="label-kota" class="input-mini">
									<input type="text" id="label-kec" name="label-kec" class="input-mini">
									<input type="text" id="label-kel" name="label-kel" class="input-mini">
									<input type="text" id="label-rw" name="label-rw" class="input-mini">
									<input type="text" id="label-rt" name="label-rt" class="input-mini">
								</div>
							</div>
						</div>   

						<div class="container" id="data-tabel">
						 <div id="main-center">
	   					 </div> 	
					     <div class="container" id="button-add">
					      <div class="container">
					        <h6>A. Identitas Keluarga
					          <a href="#dialog-mahasiswa" id="0" class="btn tambah btn-success btn-small" data-toggle="modal">
					            <i class="icon-plus"></i> Tambah Data
					          </a>
					        </h6>

					        <!-- tempat untuk menampilkan data mahasiswa -->
					        <div class="container" id="data-mahasiswa" ></div>
					      </div>

					      <!-- B. Status Keluarga Dan KB -->
					      <div class="container">
					        <h6>B. Status Keluarga Dan KB
					          <a href="#dialog-mahasiswa" id="0" class="btn tambahb btn-success btn-small" data-toggle="modal">
					            <i class="icon-plus"></i> Tambah Data
					          </a>
					        </h6>
					        <!-- tempat untuk menampilkan data status keluarga -->
					        <div class="container" id="data-status-keluarga" ></div>
					      </div>

					      <!-- C. Status Keluarga Dan KB -->
					      <div class="container">
					        <h6>C. Indikator Dan Status Tahapan Keluarga
					          <a href="#dialog-mahasiswa" id="0" class="btn tambahc btn-success btn-small" data-toggle="modal">
					            <i class="icon-plus"></i> Tambah Data
					          </a>
					        </h6>
					        <!-- tempat untuk menampilkan data indikator -->
					        <div class="container" id="data-indikator" ></div>
					      </div>

					      </div>    
					    </div>     

					    <!-- awal untuk modal dialog -->
						<div id="dialog-mahasiswa" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						    <div class="modal-header">
						      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
						      <h4 id="myModalLabel">Tambah Data Keluarga</h4>
						    </div>
						    <!-- tempat untuk menampilkan form mahasiswa -->
						    <div class="modal-body">
						    </div>
						    <div class="modal-footer">
						      <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Batal</button>
						      <button id="simpan-mahasiswa" class="btn btn-success">Simpan</button>
						    </div>
						</div>
						  <!-- akhir kode modal dialog Identitas Keluarga --> 
					</div>
<!-- =============================== FORM UPDATE ==================================================== --> 
					<div class="tab-pane" id="tabs1-update-data">
						<?php include "tab.update.php" ?>
					</div>
<!-- =============================== FORM HAPUS ==================================================== --> 
					<div class="tab-pane" id="tabs1-delete-data">
						<?php include "tab.hapus.php" ?>
					</div>
				</div><!-- /.tab-content -->
			</div><!-- /.tabbable -->
		</div><!-- .tabs-basic -->
	</div>
	<div class="modal-footer">
      <p align="center">
      	<!-- <b>Badan Kependudukan dan Keluarga Berencana Provinsi Jawa Barat</b> -->
      </p>
    </div>

<script>
  $(document).ready(function () {
    //$('#tabes a[href="#tabs1-pane3"]').tab('show');
    $('#go-tab3').click(function (e) {
	  $('#tabes a[href="#tabs1-pane3"]').tab('show');
	});
  });
</script>

</body>
</html>