<!-- <script type="text/javascript" src="aplikasi_update.js"></script> -->
<h4>Update Data Keluarga</h4>
	<div class="container">
	  <table>
	    <tr>
	      <td>
	        <label class="control-label" for="propinsi_update">Propinsi : </label>
	      </td>
	      <td>
	        <select name="propinsi_update" id="propinsi_update" class="span2">
	          <option value="">--Pilih Propinsi--</option>
	          <?php

	            // tampilkan nama-nama propinsi yang ada di database
	            $sql = mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit=9 ORDER BY no_unit_detail");
	            while($p=mysql_fetch_array($sql)){
	              echo "<option value=$p[id_unik]>$p[no_unit_detail]</option> \n";
	            }
	          ?>
	        </select>
	      </td>
	      <td>
	        <label id="lkota" class="control-label" for="kota_update">Kota :</label>
	      </td>
	      <td>
	        <select name="kota_update" id="kota_update" class="span2">
	          <option value="">--Pilih Kota--</option>
	        </select>
	      </td>
	      <td>
	        <label id="lkecamatan" class="control-label" for="kecamatan_update">Kecamatan :</label>
	      </td>
	      <td>
	        <select name="kecamatan_update" id="kecamatan_update" class="span2">
	          <option value="">--Pilih Kecamatan--</option>
	        </select>
	      </td>
	    </tr>
	    <tr>
	      <td>
	        <label id="ldesa" class="control-label" for="Kelurahan">Kelurahan :</label>
	      </td>
	      <td>  
	        <select name="desa_update" id="desa_update" class="span2">
	          <option value="">--Pilih Kelurahan--</option>
	        </select>
	      </td>
	      <td>
	        <label id="lrw" class="control-label" for="rw">RW :</label>
	      </td>
	      <td>
	        <select name="rw_update" id="rw_update" class="span2">
	          <option value="">--Pilih RW--</option>
	        </select>
	      </td>
	      <td>
	        <label id="lrt" class="control-label" for="rt">RT :</label>
	      </td>
	      <td>
	        <select name="rt_update" id="rt_update" class="span2">
	          <option value="">--Pilih RT--</option>
	        </select>
	      </td>
	    </tr>
	   </table>
	</div>
	<hr>
	<div class="container" name="form-isi-update" id="form-isi-update">
		<p>
		Masukan Nama Kepala Kelurga / No. KKI:
		<input type="text" size="30" value="" name="inputString_update" id="inputString_update" onkeyup="lookup_update(this.value);" onblur="fill_update();" />
		&nbsp;&nbsp;
		<button id="update-data" name="update-data" class="btn btn-primary"><i class="icon-eye-open icon-white"></i> Lihat</button>
		<div class="suggestionsBox" id="suggestions_update" style="display: none;">
			<img src="upArrow.png" style="position: relative; top: -12px; left: 30px;" alt="upArrow" />
			<div class="suggestionList" id="autoSuggestionsList_update">
				&nbsp;
			</div>
		</div>
		</p>
	</div>