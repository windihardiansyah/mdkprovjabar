<?php
// panggil berkas koneksi.php
require 'koneksi.php';

// buat koneksi ke database mysql
//koneksi_buka();

?>

<table class="table table-condensed table-bordered table-hover" cellpadding="0" cellspacing="0">
<thead>
	<tr>
		<th>#</th>
		<th>Kd. Ind</th>
		<th>NIK</th>
		<th>Nama</th>
		<th>Hubungan<br>Dengan KK</th>
		<th>Jenis<br>Kelamin</th>
		<th>Status<br>Kawin</th>
		<th>Usia<br>Pertama Kawin</th>
		<th>Tempat<br>Lahir</th>
		<th>Tanggal<br>Lahir</th>
		<th>Pendidikan</th>
		<th>Pekerjaan</th>
		<th>Memiliki<br>Kartu Keluarga</th>
		<th>Status</th>
	</tr>
</thead>
<tbody>
	<?php 
		$i = 1;
		$query = mysql_query("SELECT * FROM dbo_individu_test WHERE Kd_indv<>'' ORDER BY Kd_indv");
		
		// tampilkan data mahasiswa selama masih ada
		while($data = mysql_fetch_array($query)) {
			
	?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $data['Kd_indv'] ?></td>
		<td><?php echo $data['nik'] ?></td>
		<td><?php echo $data['Nama'] ?></td>
		<td><?php echo get_family_member_type_name($data['Kd_fammbrtyp']) ?></td>
		<td><?php echo get_gender_name($data['Kd_gen']) ?></td>
		<td><?php echo get_marital_name($data['Kd_martl']) ?></td>
		<td><?php echo $data['usiakawin'] ?></td>
		<td><?php echo $data['Tpt_lahir'] ?></td>
		<td><?php echo $data['Tgl_lahir'] ?></td>
		<td><?php echo get_education_name($data['Kode_edu']) ?></td>
		<td><?php echo get_employment_name($data['Kode_enp']) ?></td>
		<td><?php if($data['kk']==2){echo "Ya";}else{echo "Tidak";} ?></td>
		<td>
			<a href="#dialog-mahasiswa" id="<?php echo $data['Kd_indv'] ?>" class="ubah" data-toggle="modal">
				<i class="icon-pencil"></i>
			</a>
			<a href="#" id="<?php echo $data['Kd_indv'] ?>" class="hapus">
				<i class="icon-trash"></i>
			</a>
		</td>
	</tr>
	<?php
		$i++;
		}
	?>
</tbody>
</table>

<?php 
// tutup koneksi ke database mysql
//koneksi_tutup(); 
?>

