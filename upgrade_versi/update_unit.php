<?php
	// tampilkan semua dbo_family yg kd_prop nya 10

	include "../koneksi/koneksi.inc.php";

	mysql_query("ALTER TABLE dbo_family
  			ADD (pnpm tinyint(4),
       			 pkh tinyint(4),
       			 blt tinyint(4),
       			 raskin tinyint(4),
       			 bantuan_pendidikan tinyint(4),
       			 puap tinyint(4),
       			 pemp tinyint(4),
       			 uppks tinyint(4),
       			 apbn tinyint(4),
       			 apbd tinyint(4),
       			 krista tinyint(4),
       			 kur tinyint(4),
       			 lainnya tinyint(4));
	");	

	mysql_query("ALTER TABLE dbo_individu
  			ADD (aktalahir tinyint(4),
       			 usiakawin tinyint(4),
       			 suratnikah tinyint(4),
       			 kk tinyint(4),
       			 ktp tinyint(4),
       			 jamkesmas tinyint(4),
       			 jamkesda tinyint(4),
       			 jampersal tinyint(4),
       			 bkb tinyint(4),
       			 bkr tinyint(4),
       			 bkl tinyint(4),
       			 pik tinyint(4),
       			 tgl_mutasi date,
       			 nik varchar(50));
	");	

	//=========================== update versi untuk tabel dbo_individu dan dbo_family ===========================
	mysql_query("Update dbo_family set Kd_dist=concat('32',SUBSTR(Kd_dist,3,2)), Kd_fam=concat('32',SUBSTR(Kd_fam,3,14)), Kd_prop='32', Kd_subdist=concat('32',SUBSTR(Kd_subdist,3,4)), Kd_vill=concat('32',SUBSTR(Kd_vill,3,7)), Kd_subvill=concat('32',SUBSTR(Kd_subvill,3,10)), Kd_neigh=concat('32',SUBSTR(Kd_neigh,3,13)) where Kd_prop='10'");
	
	mysql_query("update dbo_individu set Kd_fam=concat('32',SUBSTR(Kd_fam,3,14)), Kd_neigh=concat('32',SUBSTR(Kd_neigh,3,13)), Kd_indv=concat('32',SUBSTR(Kd_indv,3,14)) where SUBSTR(Kd_fam,1,2)='10';");

	mysql_query("update dbo_fam_ind_detail set Kd_fam=concat('32',SUBSTR(Kd_fam,3,14)) where SUBSTR(Kd_fam,1,2)='10'");

	mysql_query("update dbo_unit_detail set id_unit_detail=99 where no_unit_detail='PAPUA BARAT' ");

	mysql_query("delete from dbo_unit_detail where no_unit_detail='KOTA a'");

	mysql_query("update dbo_unit_detail set id_unit_detail=concat('32',SUBSTR(id_unit_detail,3,13)), id_unit_detail_idk=concat('32',SUBSTR(id_unit_detail_idk,3,13)) where SUBSTR(id_unit_detail,1,2)='10'");

	mysql_query("update dbo_unit_detail set id_unit_detail_idk=0 where id_unit_detail='32'");

	mysql_query("delete from dbo_unit_detail where no_unit_detail='KOTA a'");

	mysql_query("
		DROP TABLE IF EXISTS dbo_yesno;		
	");

	mysql_query("
		CREATE TABLE dbo_yesno (
		  Kd_yesno tinyint(4) NOT NULL,
		  Nm_yesno_eng varchar(20) DEFAULT NULL,
		  Nm_yesno_ind varchar(20) DEFAULT NULL,
		  PRIMARY KEY (Kd_yesno)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1;
	");

	mysql_query("
		INSERT INTO dbo_yesno VALUES ('1', 'No', 'Tidak'),
		INSERT INTO dbo_yesno VALUES ('2', 'Yes', 'Ya')
	");

	mysql_query("DELETE FROM dbo_menu");

	mysql_query("
		INSERT INTO dbo_menu VALUES ('76', '65', '3', 'menu/menu_pilih.php', 'Menu', 'Menu', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('112', '106', '0', 'rekap_1/rekap_rt.php', 'Report General', 'Report General', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('50', null, '3', 'logout.php', 'Logout', 'Logout', '7', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('65', null, '3', '', 'Setup', 'Setup', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('91', '65', '0', 'indikator_ks/indikator.php', 'Indikator KS', 'Indikator KS', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('106', null, '0', '', 'Report ', 'Report', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('95', null, '0', null, 'Data', 'Data', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('102', '95', '0', 'form_keluarga/hapus_data_keluarga.php', 'Penghapusan Data Keluarga', 'Penghapusan Data Keluarga', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('101', '95', '0', 'form_keluarga/form_edit.php', 'Perubahan Data Keluarga', 'Perubahan Data Keluarga', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('100', '95', '0', 'form_keluarga/menu_ajax.php', 'Penambahan Data  Keluarga Baru', 'Tambah Keluarga Baru', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('162', '106', '0', 'rekap_data_keluarga/rekap_rt.php', 'Report Keluaran - Output MDK', 'Report Data Keluarga Pusat', '11', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('161', '65', '0', 'area_induk_test_tree_ada_link_area_refresh_all_tampil_all/area.php', 'Area-wilayah', '', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('163', null, '0', '', 'Rekapitulasi ', 'Rekapitulasi', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('164', '163', '0', '', 'KS', 'KS', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('165', '164', '0', 'report_ks/rekap_rt.php', 'PRA S Dan KS I', 'PRA S & KS I', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('166', '164', '0', 'report_ks_kb/rekap_rt.php', 'PRA KS Dan KS I KB', 'PRA KS & KS I KB', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('167', '164', '0', 'report_kb/rekap_rt.php', 'KB Detail', 'KB Detail', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('168', '163', '0', '', 'KB', 'KB', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('169', '168', '0', 'report_kb_kontrasepsi/rekap_rt.php', 'Mix Kontrasepsi', 'Mix Kontrasepsi', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('170', '106', '0', 'rekap_register_data_keluarga/rekap_rt.php', 'Register Data Keluarga R1 KS - R I PUS - R II PUS', 'Register Data Keluarga', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('171', '95', '0', 'cari_individu/rekap_rt.php', 'Cari individu', 'Cari individu', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('172', '163', '0', '', 'Demografi', 'Demografi', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('173', '172', '0', 'report_demografi/rekap_rt.php', 'Umur', 'Umur', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('174', '172', '0', 'report_demografi_krj/rekap_rt.php', 'Status Kerja Dan Umur', '', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('175', '172', '0', 'report_demografi_skl/rekap_rt.php', 'Status Usia Sekolah', '', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('176', '163', '0', 'report_non_kb_alsn/rekap_rt.php', 'Non KB per Alasan', '', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('177', '163', '0', 'report_tahapan/rekap_rt.php', 'Tahapan KS', '', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('178', '65', '0', 'area_typeuser_gabung_test_tree_ada_link_area_refresh_all_tampil_all/area.php', 'Typeuser-Menuakses-User', '', '8', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('179', '172', '0', 'report_jumlah_kk_jiwa/rekap_rt.php', 'Jumlah KK dan Jiwa', '', '0', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('221', '220', '0', 'pelaporan/tabel_1/rekap_rt.php', '1. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN ', 'JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('222', '220', '0', 'pelaporan/tabel_2/rekap_rt.php', '2. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR JENIS KELAMIN DAN BERSTATUS BEKERJA ', '', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('223', '220', '0', 'pelaporan/tabel_3/rekap_rt.php', '3. JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA ', 'JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('224', '220', '0', 'pelaporan/tabel_4/rekap_rt.php', '4. JUMLAH PESERTA KB MENURUT KELOMPOK UMUR METODE KONTRSEPSI DAN JUMLAH ANAK ', 'JUMLAH PESERTA KB MENURUT KELOMPOK UMUR METODE KONTRSEPSI DAN JUMLAH ANAK', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('225', '220', '0', 'pelaporan/tabel_5/rekap_rt.php', '5. JUMLAH PUS DAN PUS PESERTA KB, PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR DAN ALASAN TIDAK BER-KB ', 'JUMLAH PUS DAN PUS PESERTA KB, PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR DAN ALASAN TIDAK BER-KB', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('226', '220', '0', 'pelaporan/tabel_6/rekap_rt.php', '6. JUMLAH JIWA MENURUT KELOMPOK UMUR DAN JENIS KELAMIN ', 'JUMLAH JIWA MENURUT KELOMPOK UMUR DAN JENIS KELAMIN', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('227', '220', '0', 'pelaporan/tabel_7/rekap_rt.php', '7. JUMLAH PUS DAN PUS PESERTA KB USIA 45-49 ', 'JUMLAH PUS DAN PUS PESERTA KB USIA 45-49', '7', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('228', '220', '0', 'pelaporan/tabel_8/rekap_rt.php', '8. JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, DAN STATUS BERSEKOLAH ', 'JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH', '8', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('229', '220', '0', 'pelaporan/tabel_9/rekap_rt.php', '9. JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH', 'JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH', '9', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('230', '220', '0', 'pelaporan/tabel_10/rekap_rt.php', '10. JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PENDIDIKAN', 'JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PENDIDIKAN', '10', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('231', '220', '0', 'pelaporan/tabel_11/rekap_rt.php', '11. JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PEKERJAAN', 'JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PEKERJAAN', '11', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('232', '220', '0', 'pelaporan/tabel_12/rekap_rt.php', '12. JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR, DAN STATUS TAHAPAN KELUARGA SEJAHTERA', 'JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR, DAN STATUS TAHAPAN KELUARGA SEJAHTERA', '12', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('233', '220', '0', 'pelaporan/tabel_13/rekap_rt.php', '13. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR,  KELAMIN, STATUS BEKERJA DAN TAHAPAN KELUARGA SEJAHTERA', 'JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR,  KELAMIN, STATUS BEKERJA DAN TAHAPAN KELUARGA SEJAHTERA', '13', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('234', '220', '0', 'pelaporan/tabel_14/rekap_rt.php', '14. JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR,   STATUS TAHAPAN KELUARGA SEJAHTERA DAN PENDIDIKAN', 'JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR,   STATUS TAHAPAN KELUARGA SEJAHTERA DAN PENDIDIKAN', '14', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('235', '220', '0', 'pelaporan/tabel_15/rekap_rt.php', '15. JUMLAH KELUARGA DENGAN ANAK BALITA MENURUT MENURUT KELOMPOK UMUR,  JUMLAH BALITA, KEGIATAN POSYANDU DAN STATUS THP KS', 'JUMLAH KELUARGA DENGAN ANAK BALITA MENURUT MENURUT KELOMPOK UMUR,  JUMLAH BALITA, KEGIATAN POSYANDU DAN STATUS THP KS', '15', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('236', '220', '0', 'pelaporan/tabel_16/rekap_rt.php', '16. JUMLAH PUS  MENURUT KELOMPOK UMUR, KESERTAAN BER-KB, TEMPAT PELAYANAN DAN TAHAPAN KELUARGA SEJAHTERA', 'JUMLAH PUS  MENURUT KELOMPOK UMUR, KESERTAAN BER-KB, TEMPAT PELAYANAN DAN TAHAPAN KELUARGA SEJAHTERA', '16', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('254', '219', '0', '', 'Tabel Status Kerja Pertahapan KS', 'Tabel Status Kerja Kepala Keluarga', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('250', '240', '0', 'pelaporan/report_temanggung_ks_tabel_3/rekap_rt.php', '3. JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - KECIL 5 TH DAN PROXY CWR', 'JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - < 5 TH DAN PROXY CWR', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('248', '240', '0', 'pelaporan/report_temanggung_ks_tabel_1/rekap_rt.php', '1. JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN', 'JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('247', '239', '0', 'pelaporan/report_temanggung_tabel_6_1/rekap_rt.php', '7. JUMLAH KEPALA KELUARGA PRASEJAHTERA MENURUT INDIKATOR YANG JATUH', 'JUMLAH KEPALA KELUARGA PRASEJAHTERA MENURUT INDIKATOR YANG JATUH', '7', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('246', '239', '0', 'pelaporan/report_temanggung_tabel_6/rekap_rt.php', '6. JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI', 'JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('245', '239', '0', 'pelaporan/report_temanggung_tabel_5/rekap_rt.php', '5. JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN', 'JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('243', '239', '0', 'pelaporan/report_temanggung_tabel_3/rekap_rt.php', '3. JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - KECIL 5 TH DAN PROXY CWR', 'JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - < 5 TH DAN PROXY CWR', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('257', '254', '0', 'pelaporan/report_ks_tabel_1/rekap_rt.php', 'Berdasarkan Kepala Keluarga', '', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('255', '219', '0', 'pelaporan/report_ks_tabel_2/rekap_rt.php', 'Tabel Non KB Peralasan Per Thp KS', 'sdas', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('258', '254', '0', 'pelaporan/report_ks_tabel_3/rekap_rt.php', 'Berdasarkan Individu, Umur dan Pendidikan', '', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('259', null, '0', null, 'Backup-Restore Data', 'Backup & Restore Data', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('260', '259', '0', 'backup-restore/index.php', 'Eksport', 'Eksport', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('249', '240', '0', 'pelaporan/report_temanggung_ks_tabel_2/rekap_rt.php', '2. JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 - KECIL 1 TH DAN PROXY CBR', 'JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 -< 1 TH DAN PROXY CBR', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('244', '239', '0', 'pelaporan/report_temanggung_tabel_4/rekap_rt.php', '4. JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR', 'JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('242', '239', '0', 'pelaporan/report_temanggung_tabel_2/rekap_rt.php', '2. JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 - KECIL 1 TH DAN PROXY CBR', 'JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 -< 1 TH DAN PROXY CBR', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('238', '219', '0', '', 'Tabel Umum', 'Tabel Umum', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('239', '238', '0', '', 'Berdasarkan Wilayah', '', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('240', '238', '0', '', 'Berdasarkan Tahapan', '', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('241', '239', '0', 'pelaporan/report_temanggung_tabel_1/rekap_rt.php', '1. JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN', 'JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('219', null, '0', '', 'Tabel Kependudukan  ', 'TABEL KEPENDUDUKAN', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('220', '219', '0', '', 'Kelompok Umur', '', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('237', '220', '0', 'pelaporan/tabel_17/rekap_rt.php', '17. JUMLAH PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR,  ALASAN TIDAK BER-KB DAN TAHAPAN KELUARGA SEJAHTERA', 'JUMLAH PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR,  ALASAN TIDAK BER-KB DAN TAHAPAN KELUARGA SEJAHTERA', '17', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('251', '240', '0', 'pelaporan/report_temanggung_ks_tabel_4/rekap_rt.php', '4. JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR', 'JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('252', '240', '0', 'pelaporan/report_temanggung_ks_tabel_5/rekap_rt.php', '5. JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN', 'JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('253', '240', '0', 'pelaporan/report_temanggung_ks_tabel_6/rekap_rt.php', '6. JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI', 'JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('261', '259', '0', 'backup-restore/recovery_data.php', 'Import-Restore', 'Import', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('262', '259', '0', '', 'Backup', '', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('263', '262', '0', 'backup_data_desa/index.php', 'Backup Data Keluarga', '', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('264', '262', '0', 'backup_tabel/backup.php', 'Backup Tabel', '', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('265', '262', '0', 'backup_data_desa/backup', 'Download File Data Keluarga', '', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('266', '259', '0', 'backup_repair/backup.php', 'Repair Tabel', '', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('267', '259', '0', 'backup-restore/backup/', 'Download File Backup', '', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('268', '95', '0', 'kiki/rekap_rt.php', 'Update Data Keluarga', '', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('269', '65', '0', 'conver_status/rekap_rt.php', 'Konversi', 'conver_status', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('270', '220', '0', 'pelaporan/tabel_3_+/rekap_rt.php', '3+. JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA', 'JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('271', '219', '0', 'pelaporan/report_kat_usia/rekap_rt.php', 'Tabel Kategori Usia (Balita, Remaja, Lansia)', 'Tabel Kategori Usia (Balita, Remaja, Lansia)', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('272', '219', '0', 'pelaporan/report_temanggung_pus/rekap_rt.php', 'Jumlah PUS, Peserta KB dan bukan peserta KB ', 'Jumlah PUS, Peserta KB dan bukan peserta KB ', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('273', '164', '0', 'report_ks_alek/rekap_rt.php', 'Pra S - KS I Alek Dan Non Alek', 'Pra S - KS I Alek Dan Non Alek ', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('312', '313', '0', 'report_kelahiran_menurut_asfr/rekap_rt.php', '12. PROXY ANGKA KELAHIRAN MENURUT KELOMPOK UMUR (ASFR)', 'Report Kelahiran Menurut ASFR Per Daerah', '11', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('310', '313', '0', 'report_punya_lansia/rekap_rt.php', '7. JUMLAH SASARAN KELUARGA MEMILIKI LANSIA', 'Report Punya Lansia', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('311', '313', '0', 'report_punya_remaja/rekap_rt.php', '6. JUMLAH SASARAN KELUARGA MEMILIKI REMAJA', 'Report Keluarga Punya Remaja', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('309', '313', '0', 'report_kel_uppks/rekap_rt.php', '8. JUMLAH KELUARGA IKUT UPPKS PER DAERAH', 'Report Keluarga Ikut UPPKS', '7', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('308', '313', '0', 'report_kel_per_daerah/rekap_rt.php', '3. JUMLAH KELUARGA PER DAERAH DAN STATUS TAHAPAN KELUARGA SEJAHTERA', 'Report Jml Keluarga per Daerah dan Status Tahapan Keluarga Sejahtera', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('307', '313', '0', 'report_jum_kel_menurut_anak/rekap_rt.php', '4. JUMLAH KELUARGA MENURUT JUMLAH ANAK YANG DIMILIKI DAN STATUS TAHAPAN KELUARGA SEJAHTERA ', 'Report Jml Keluarga Menurut Jml Anak & Status Tahapan Keluarga', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('306', '313', '0', 'report_kematian_bayi/rekap_rt.php', '1. PROXI ANGKA KEMATIAN BAYI (IMR) PER DAERAH', 'Report Kematian Bayi', '0', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('305', '313', '0', 'report_KP_KSI_tanpa_jamban/rekap_rt.php', '9.  JUMLAH KPS DAN KS I YANG TIDAK MEMILIKI JAMBAN / SUMBER AIR MINUM BERSIH / SUMBER PENERANGAN LISTRIK / BAHAN BAKAR GAS/LISTRIK UNTUK MEMASAK', 'Report KPS dan KS 1 Tidak Punya Punya Jamban', '8', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('304', '313', '0', 'report_peserta_kb/rekap_rt.php', '2. PESERTA KB MENURUT METODE KONTRASEPSI, KELOMPOK UMUR DAN JUMLAH ANAK', 'Peserta KB Menurut Metoda, Kelompok Umur dan Jumlah Anak', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('300', '313', '0', 'report_jumlah_sasaran_11/rekap_rt.php', '5. JUMLAH KELUARGA MEMILIKI ANAK 0-11 TAHUN', '', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('301', '313', '0', 'report_sasaran_kel_balita/rekap_rt.php', '13.  JUMLAH SASARAN KELUARGA MEMILIKI BALITA', '', '12', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('302', '313', '0', 'report_ratio_beban_per_daerah/rekap_rt.php', '11. PROXY RATIO BEBAN KETERGANTUNGAN PER DAERAH', 'Report Proxi Ratio Beban Ketergantungan Daerah', '10', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('303', '313', '0', 'report_angka_kematian_kasar/rekap_rt.php', '10. PROXY ANGKA KEMATIAN KASAR PER DAERAH', 'Report Proxi Angka Kematian Kasar', '9', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('313', '219', '0', '', 'Output Tambahan', '', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('314', '219', '0', 'report_data_ind_ks/rekap_rt.php', 'Data Individu Rekapitulasi Pra S & KS I Alek, Non Alek', '', '7', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('315', '219', '0', 'chart/index.php', 'Piramida Kependudukan', 'Piramida Kependudukan', '9', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('316', '259', '0', 'generate_kki/index.php', 'Generate KKI & KAK', 'Generate KKi dan KAK', '6', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('317', '313', '0', 'rekap_jumlah_mutasi/rekap_rt.php', '14. REKAP JUMLAH MUTASI DATA KELUARGA', '14. REKAP JUMLAH MUTASI DATA KELUARGA', '13', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('318', '313', '0', 'rekap_kepemilikan_akta_lahir/rekap_rt.php', '15. REKAP KEPEMILIKAN AKTA LAHIR', '15. REKAP KEPEMILIKAN AKTA LAHIR', '14', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('319', '313', '0', 'rekap_pus_belum_berkb/rekap_rt.php', '16. Rekap PUS Belum Ber-KB Dengan Jumlah Anak', '16. Rekap PUS Belum Ber-KB Dengan Jumlah Anak', '15', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('320', '170', '0', 'rekap_register_data_keluarga/rekap_rt.php', 'A. DATA DEMOGRAFI DAN KB', 'A. DATA DEMOGRAFI DAN KB', '1', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('321', '170', '0', 'rekap_register_data_keluarga_b/rekap_rt.php', 'B. TAHAPAN KELUARGA SEJAHTERA', 'B. TAHAPAN KELUARGA SEJAHTERA', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('322', '170', '0', 'rekap_register_data_keluarga_c/rekap_rt.php', 'C. DATA ANGGOTA KELUARGA', 'C. DATA ANGGOTA KELUARGA', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('323', '170', '0', 'rekap_register_data_keluarga_d/rekap_rt.php', 'RI PUS Bagi Seluruh Keluarga', 'RI PUS Bagi Seluruh Keluarga', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('324', '170', '0', 'rekap_register_data_keluarga_e/rekap_rt.php', 'Register Kelompok KB Bagi Keluarga PRA S & KS I', 'Register Kelompok KB Bagi Keluarga PRA S & KS I', '5', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('325', '168', '0', 'report_kb_kontrasepsi_pra_s_alek/rekap_rt.php', 'Mix Kontrasepsi Pra S Alek', 'Mix Kontrasepsi Pra S Alek', '2', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('326', '168', '0', 'report_kb_kontrasepsi_pras_non_alek/rekap_rt.php', 'Mix Kontrasepsi Pra S Non Alek', 'Mix Kontrasepsi Pra S Non Alek', '3', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('327', '168', '0', 'report_kb_kontrasepsi_ks_alek/rekap_rt.php', 'Mix Kontrasepsi KS I Alek', 'Mix Kontrasepsi KS I Alek', '4', null)
	");
	mysql_query("
		INSERT INTO dbo_menu VALUES ('328', '168', '0', 'report_kb_kontrasepsi_ks_non_alek/rekap_rt.php', 'Mix Kontrasepsi KS I Non Alek', 'Mix Kontrasepsi KS I Non Alek', '5', null)							
	");

	mysql_query("DELETE FROM dbo_typeuser_menu_akses");
	mysql_query("DELETE FROM dbo_typeuser_menu");

	$menu = mysql_query("SELECT * FROM dbo_menu ORDER BY id_menu");
	while($idmenu = mysql_fetch_array($menu)){
		$id = $idmenu['id_menu'];
		mysql_query("insert into dbo_typeuser_menu_akses VALUES('".$id."','0000') ");
		mysql_query("insert into dbo_typeuser_menu VALUES('".$id."','0000') ");
	}


	
	//=========================== AKHIR update versi untuk tabel dbo_individu dan dbo_family ===========================

	// //============== update dbo_user ====================
	// $data_user = mysql_query("select * from dbo_user where id_typeuser like '10%' ");
	// while($duser=mysql_fetch_array($data_user)){

	// 	$id_typeuser_lama 		= $duser['id_typeuser'];
	// 	$panjang_id_typeuser 	= strlen($duser['id_typeuser']);
	// 	$id_typeuser_skrg		= "32".substr($id_typeuser_lama,2,$panjang_id_typeuser);


	// 	mysql_query("UPDATE dbo_user SET id_typeuser='$id_typeuser_skrg' WHERE id_typeuser='$id_typeuser_lama' ");

	// }

	// //============== update dbo_typeuser_unit_detail ====================
	// $data_user = mysql_query("select * from dbo_typeuser_unit_detail where id_typeuser like '10%' ");
	// while($duser=mysql_fetch_array($data_user)){

	// 	$id_typeuser_lama 		= $duser['id_typeuser'];
	// 	$panjang_id_typeuser 	= strlen($duser['id_typeuser']);
	// 	$id_typeuser_skrg		= "32".substr($id_typeuser_lama,2,$panjang_id_typeuser);


	// 	mysql_query("UPDATE dbo_typeuser_unit_detail SET id_typeuser='$id_typeuser_skrg' WHERE id_typeuser='$id_typeuser_lama' ");

	// }

	// //============== update dbo_typeuser_menu_akses ====================
	// $data_user = mysql_query("select * from dbo_typeuser_menu_akses where id_typeuser like '10%' ");
	// while($duser=mysql_fetch_array($data_user)){

	// 	$id_typeuser_lama 		= $duser['id_typeuser'];
	// 	$panjang_id_typeuser 	= strlen($duser['id_typeuser']);
	// 	$id_typeuser_skrg		= "32".substr($id_typeuser_lama,2,$panjang_id_typeuser);


	// 	mysql_query("UPDATE dbo_typeuser_menu_akses SET id_typeuser='$id_typeuser_skrg' WHERE id_typeuser='$id_typeuser_lama' ");

	// }

	// //============== update dbo_typeuser_menu ====================
	// $data_user = mysql_query("select * from dbo_typeuser_menu where id_typeuser like '10%' ");
	// while($duser=mysql_fetch_array($data_user)){

	// 	$id_typeuser_lama 		= $duser['id_typeuser'];
	// 	$panjang_id_typeuser 	= strlen($duser['id_typeuser']);
	// 	$id_typeuser_skrg		= "32".substr($id_typeuser_lama,2,$panjang_id_typeuser);


	// 	mysql_query("UPDATE dbo_typeuser_menu SET id_typeuser='$id_typeuser_skrg' WHERE id_typeuser='$id_typeuser_lama' ");

	// }

	// //============== update dbo_typeuser ====================
	// $data_user = mysql_query("select * from dbo_typeuser where id_typeuser like '10%' ");
	// while($duser=mysql_fetch_array($data_user)){

	// 	$id_typeuser_lama 		= $duser['id_typeuser'];
	// 	$panjang_id_typeuser 	= strlen($duser['id_typeuser']);
	// 	$id_typeuser_skrg		= "32".substr($id_typeuser_lama,2,$panjang_id_typeuser);


	// 	mysql_query("UPDATE dbo_typeuser SET id_typeuser='$id_typeuser_skrg' WHERE id_typeuser='$id_typeuser_lama' ");

	// }

	//echo "DATA SUDAH DI UPDATE"

?>
<br><br><br><br><br><br><br><br><br><br>
<div align="center"><font size="+4" color="#0066CC" face="Verdana, Arial, Helvetica, sans-serif">PROSES BERHASIL</font><br><br><br>
<div align="center"><font size="+4" color="#0066CC" face="Verdana, Arial, Helvetica, sans-serif">MDK V.3</font><br><br><br>
<a href="../"><font size="3" color="#0066CC" face="Verdana, Arial, Helvetica, sans-serif">LOGIN MDK</font></a>
</div>


