$(document).ready(function(){
  // $("#loader").hide();
  $("#ulangi").hide();
  var kota = document.getElementById("kota");
  var kecamatan = document.getElementById("kecamatan");
  var desa = document.getElementById("desa");
  $("#generate_data").hide();
  $("#loader").hide();
  
  $("#propinsi").change(function(){

        $("#generate_data").fadeOut();

        var propinsi = $("#propinsi").val();
        //alert(propinsi);
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = propinsi;

        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota").html(data);  
                $("#kecamatan").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");                
                $("#rw").html("<option value=''>--Pilih RW--</option>");                
                $("#rt").html("<option value=''>--Pilih RT--</option>");                
            }
        });
  });

  $("#kota").change(function(){

        $("#generate_data").fadeOut();
        var kota = $("#kota").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kota;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan").html(data);
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");
                $("#rw").html("<option value=''>--Pilih RW--</option>");                
                $("#rt").html("<option value=''>--Pilih RT--</option>");                
            }
        });
  });

  $("#kecamatan").change(function(){
    
        $("#generate_data").fadeOut();
        // $("#data-mahasiswa").fadeOut();
        var kecamatan = $("#kecamatan").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kecamatan;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa").html(data);
                $("#rw").html("<option value=''>--Pilih RW--</option>");                
                $("#rt").html("<option value=''>--Pilih RT--</option>");                
            }
        });
  });

  $("#desa").change(function(){
        $("#generate_data").fadeIn();
        // $("#data-mahasiswa").fadeOut();
        var desa = $("#desa").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = desa;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + desa + "&kode=RW",
            success: function(data){
              $("#rw").html(data);        
              $("#rt").html("<option value=''>--Pilih RT--</option>");                
            }
        });
  });

  $("#rw").change(function(){
        $("#generate_data").fadeIn();
        // $("#data-mahasiswa").fadeOut();
        var rw = $("#rw").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = rw;

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + rw + "&kode=RT",
            success: function(data){
              $("#rt").html(data);
            }
        });
  });

  $("#rt").change(function(){
        $("#generate_data").fadeIn();
        // $("#data-mahasiswa").fadeOut();
        var rt = $("#rt").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = rt;

        $.ajax({
            url: "proses_kecamatan.php",
            // data: "wil_id=" + rt + "&kode=RT",
            success: function(data){
              // $("#rt").html(data);
            }
        });
  });



});