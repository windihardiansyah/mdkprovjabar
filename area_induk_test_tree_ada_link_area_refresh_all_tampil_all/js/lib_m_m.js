messageObj = new DHTML_modalMessage();	// We only create one object of this class
messageObj.setShadowOffset(5);	// Large shadow


function displayMessage()
{
	
	
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(100,100);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.setHtmlContent("<IMG src='../images/spinner.gif'>&nbsp;&nbsp;Silahkan Tunggu, Sedang Proses !!!");		 
	messageObj.display();
	return false;
}

function displayStaticMessage(messageContent,cssClass)
{
	messageObj.setHtmlContent(messageContent);
	messageObj.setSize(300,150);
	messageObj.setCssClassMessageBox(cssClass);
	messageObj.setSource(false);	// no html source since we want to use a static message here.
	messageObj.setShadowDivVisible(false);	// Disable shadow for these boxes	
	messageObj.display();
	
	
}

function closeMessage()
{
	messageObj.close();	
}
