<?php
session_start();
if(!isset($_SESSION['un']))
   exit();
include "../koneksi/koneksi.inc.php";
include "../brt_main_cms_1.php";
unset($_SESSION['kki']);
unset($_SESSION['id_fam']);
unset($_SESSION['neigh']);
unset($_SESSION['sts_miskin']);
unset($_SESSION['sts_ks']);

?>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script type="text/javascript" src="../../tooltipsajax_1/js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="../../tooltipsajax_1/js/ajax.js"></script>
<script type="text/javascript" src="../../tooltipsajax_1/js/ajax-tooltip.js"></script>	
<link rel="stylesheet" href="../../tooltipsajax_1/css/ajax-tooltip.css" media="screen" type="text/css">

<script src="../js/ajaxlibinduk.js" type="text/javascript"></script>
<script src="../js/prototype.js" type="text/javascript"></script>
<script src="../js/instobj.js" type="text/javascript"></script>
<script src="js/lib.js" type="text/javascript"></script>
<script src="../pelaporan/js/prop.js" type="text/javascript"></script>
<script src="../pelaporan/js/kab_kota.js" type="text/javascript"></script>
<script src="../pelaporan/js/kec.js" type="text/javascript"></script>
<script src="../pelaporan/js/kel.js" type="text/javascript"></script>
<script src="../pelaporan/js/rw.js" type="text/javascript"></script>
<link rel="stylesheet" href="../js/modal-message.css" type="text/css">
<script type="text/javascript" src="../js/ajax.js"></script>
<script type="text/javascript" src="../js/modal-message.js"></script>
<script type="text/javascript" src="../js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="../pelaporan/js/lib_m_m.js"></script>
<script type='text/javascript' src='../js/common.js'></script>
<script type='text/javascript' src='../js/css.js'></script>
<script type='text/javascript' src='js/standardista-table-sorting.js'></script>
</head>
<body>
<br>
<br>
<?php 
    if($_SESSION['id_area']==15)  
	{
?>
     <div align="center"><font size="+2" color="#663333">Akses Halaman Ini Hanya Sampai Tingkat DUSUN/RW</font></div>
<?php 
     return false;
    } 
?>

<div id="c_r">

<form name="f_d_kel" id="f_d_kel">

<table bgcolor="" cellspacing="0" style="border-collapse: collapse" bordercolor="#E0DFE3" width="" leftmargin="100" border="1">
<?php 
   if($_SESSION['id_area']==9)
   {
?>
        <td>
        &nbsp;&nbsp;PROVINSI: <td><?php include "s_prop_5.php"; ?></td><td><div id="prop_kd"></div></td>
        </td>
<?php
   } 
   else
   {
      
	  
?>
      <td>
      PROVINSI :<td><B><?php echo $nm_prop;?></B> </td><td><B><?php echo $unik_prop;?></B></td>
	  <input type="hidden" id="props" value="">
      </td>
  
<?php
   }
?>
<?php 
   if($_SESSION['id_area']==9)
   {
?>
      <td id="tdkabkota" style="display:none ">
      &nbsp;&nbsp;KAB/KOTA : <td id="kabkotad"></td><td><div id="kab_kota_kd"></div></td>
      </td>
<?php
   } 
   else if($_SESSION['id_area']==10)
   {
?>   	 
      <td>
      &nbsp;&nbsp;KAB/KOTA : <td id="kabkotad"><?php include "s_kab_kota_4.php"; ?></td><td><div id="kab_kota_kd"></div></td>
      </td>
<?php
   }
   else
   {
      
	  
?>
       <td>
      &nbsp;&nbsp;KAB/KOTA :<td><B><?php echo $nm_kab_kota;?></B> </td><td><B><?php echo $unik_kab_kota;?></B></td>
	  <input type="hidden" id="kabkotas" value="">
      </td>
  
<?php
   }
?>
<?php 
   if($_SESSION['id_area']==9 OR $_SESSION['id_area']==10)
   {
?>
      <td id="tdkec" style="display:none ">
      &nbsp;&nbsp;KECAMATAN : <td id="kecd"></td><td><div id="kec_kd"></div></td>
      </td>
<?php
   } 
   else if($_SESSION['id_area']==11)
   {
?>   	 
      <td>
      &nbsp;&nbsp;KECAMATAN : <td id="kecd"><?php include "s_kec_3.php"; ?></td><td><div id="kec_kd"></div></td>
      </td>
<?php
   }
   else
   {
?>
      <td>
      &nbsp;&nbsp;KECAMATAN :<td><B><?php echo $nm_kec;?></B> </td><td><B><?php echo $unik_kec;?></B></td>
	  <input type="hidden" id="kecs" value="">
      </td>
  
<?php
   }
?>

<?php 
   if($_SESSION['id_area']==9 OR $_SESSION['id_area']==10 OR $_SESSION['id_area']==11)
   {
?>
     <td id="tdkel" style="display:none ">
     &nbsp;&nbsp;DESA/KELURAHAN : <td id="keld"></td><td><div id="kel_kd"></div></td>
     </td>
<?php 
   }
   else if($_SESSION['id_area']==12)
   {
?>
     <td>
     &nbsp;&nbsp;DESA/KELURAHAN : <td id="keld"><?php include "s_kel_2.php"; ?></td><td><div id="kel_kd"></div></td>
     </td>
<?php 
   } 
 else
   {
?>
      <td>
      &nbsp;&nbsp;DESA/KELURAHAN:<td><B><?php echo $nm_kel;?></B> </td><td><B><?php echo $unik_kel;?></B></td>
	  <input type="hidden" id="kels" value="">
      </td>
  
<?php
   }
?>
<?php 
   if($_SESSION['id_area']==9 OR $_SESSION['id_area']==10 OR $_SESSION['id_area']==11 OR $_SESSION['id_area']==12)
   {
?>
     <td id="tdrw" style="display:none ">
     &nbsp;&nbsp;DUSUN/RW : <td id="rwd"></td><td><div id="rw_kd"></div></td>
     </td>

<td>
<?php 
    }
	else if($_SESSION['id_area']==13) 
	{
?>
      <td>
      &nbsp;&nbsp;DUSUN/RW : <td id="rwd"><?php include "s_rw_1.php"; ?></td><td><div id="rw_kd"></div></td>
      </td>
<?php 
    } 
    else 
	{
?>
      <td>
      &nbsp;&nbsp;DUSUN/RW:<td><B><?php echo $nm_rw;?></B> </td><td><B><?php echo $unik_rw;?></B></td>
	  <input type="hidden" id="rws" value="">
      </td>
<?php
    }
?>
<?php 
    if($_SESSION['id_area']==14) 
    { 
?>
      <td>
      &nbsp;&nbsp;RT : <td id="rtd"><?php include "s_rt.php"; ?></td><td><div id="rt_kd"></div></td>
      </td>
<?php 
    } 
	else if($_SESSION['id_area']==15)  
	{
?>
      <td>
      &nbsp;&nbsp;RT : <td><B><?php echo $nm_rt;?></B> </td><td><B><?php echo $unik_rt;?></B></td>
	  <input type="hidden" id="rts" value="">
      </td>
<?php
    }
	
	
	else 
	{
?>
      <td id="tdrt" style="display:none ">
      &nbsp;&nbsp;RT : <td id="rtd"></td><td><div id="rt_kd"></div></td>
      </td>
<?php 
    } 
?>
<td>
<input id="btnok" type="button" onClick="ok_ks()" value="OK" onMouseOver="this.style.cursor='hand'; this.style.cursor='pointer'">
</td>

</tr>
</table>
<br>
<input type="hidden" id="id_jml_ks" value="<?php echo $i;?>">
<input type="hidden" id="f_d_k_prop" value="<?php echo $prop_f;?>">
<input type="hidden" id="f_d_k_kab_kota" value="<?php echo $kab_kota_f;?>">
<input type="hidden" id="f_d_k_kec" value="<?php echo $kec_f;?>">
<input type="hidden" id="f_d_k_kel" value="<?php echo $kel_f;?>">
<input type="hidden" id="f_d_k_rw" value="<?php echo $rw_f;?>">
<input type="hidden" id="f_d_k_rt" value="<?php echo $rt_f;?>">
</form>
<div id="w_d_k" align="center"></div>
<div id="c_r_sub"></div>
</div>
</body>
</html>