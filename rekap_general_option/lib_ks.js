function strp_slashes(str) 
{
    str=str.replace(new RegExp(/^\s+/),"");
	str=str.replace(new RegExp(/\s+$/),"");
	return str;
}
function proc_alt_kb(t) 
{
   var sli=t.selectedIndex;
  
   if(t.options[sli].text.toLowerCase()=='implant')
   {
     
	 $('div_sub_sub').style.display='';
	 
   }
   else
   {
     
	 $('div_sub_sub').style.display='none';
   }
      
}

function cek_imp_wrn()
{
	for(i=0;i<$('h_impl').value;i++)
	{
	   $('ipl'+i).style.backgroundColor='';
	}
}

function proc_pus(t) 
{
   if(t.value==1)
   {
     $('kb').disabled=false;
	 $('kb_1').disabled=false;
	 $('id_div_kb').style.display='';
 	 
   }
   else if(t.value==0)
   {
     
	 for(i=0;i<$('h_i').value;i++)
	 {
	   $('id_src'+i).style.backgroundColor='';
	   $('id_src'+i).checked=false;
	   $('id_src'+i).disabled=true;
	   
	 }
	for(i=0;i<$('h_impl').value;i++)
	{
	   $('ipl'+i).style.backgroundColor='';
	   $('ipl'+i).checked=false;
	}
	$('alt_kb').selectedIndex=0;
	$('alt_kb').disabled=true;
	$('s_bkb').selectedIndex=0;
	$('s_bkb').disabled=true;
	$('kb_1').checked=false;
	$('kb').checked=false;
	$('kb_1').disabled=true;
	$('kb').disabled=true;
	$('id_div_kb').style.display='none';
	$('div_sub').style.display='none';
	$('div_sub_sub').style.display='none';
	$('div_sub_sub_1').style.display='none';
	$('kb').style.backgroundColor=''; 
	$('kb_1').style.backgroundColor='';
   }
      
}

function up_id_ks() 
{
   ajax_hideTooltip();
   var k_kss=false; var idx_ks="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if((strp_isi.toLowerCase()!="v" && strp_isi.toLowerCase()!="x*" && strp_isi.toLowerCase()!="x" && strp_isi.toLowerCase()!="-") && strp_isi!="")    
		{
              k_kss=true;
			  var idx_n=idx_ks;
			  break;
			  
		}
	}   
	var k_ks=false; var idx_ks="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if(strp_isi=='')       
		{
              k_ks=true;
			  var idx_k=idx_ks;
			  break;
		}
	}   
	if(k_kss)
	{
		alert("Data Yang Dimasukkan Tidak Boleh Berisi Selain X* X V -") ;
		$('f_ks'+idx_n).select();
    }
	else if(k_ks)
	{
		alert("Data Yang Dimasukkan Tidak Lengkap") 
		$('f_ks'+idx_k).focus();
    }
	else
	   up_s_id_ks();
}
function up_s_id_ks() 
{
	var idx_ks=""; var v_ks=$('jml_ks').value; var up=""; 
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if(strp_isi.toLowerCase()=="v")
		   var idvn=1;
		else if(strp_isi.toLowerCase()=="x")
		   var idvn=2;
		else if(strp_isi.toLowerCase()=="x*")
		   var idvn=3;   
		else if(strp_isi.toLowerCase()=="-")
		   var idvn=4;   
	    var idvv='id'+idx_ks;
		if(i==1)
	           up+=idvv+"="+idvn;  
		else
 	           up+="&"+idvv+"="+idvn;     
   
	}
    var uprd="edit_ind_ks.php?"+up+"&jmlh_ks="+v_ks+"&r="+new Date().getTime();	

          new Ajax.Request(
          uprd,
          {
               method:"get",
			   onSuccess: rsltsikupdtt,
               onFailure: rsltsikupdtt

          }
          );
		  $('u_d_i_t_k').disabled=true;
		  $('w_d_ks').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
}
function rsltsikupdtt(request) {
    $('u_d_i_t_k').disabled=false;
	$('w_d_ks').innerHTML="<font size='3' color='blue'>Update Data Indikator Tahapan KS Berhasil</font>";
	$('td_sts_m_k').innerHTML=request.responseText;
	
}
function u_id_ks() 
{
   var k_ks=false; var idx_ks="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		if($('n_ks'+idx_ks).value=='')    
		{
              k_ks=true;
			  break;
		}
	}   
	if(k_ks)
	{
		if(confirm("Data Indikator Yang Anda Masukkan Tidak Lengkap, Lanjutkan Menyimpan Data ?"))
		{
		   u_s_id_ks();
		}
    }
	else
	   u_s_id_ks();
}
function u_s_id_ks() 
{
	var idx_ks=""; var v_ks=$('jml_ks').value; var up="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		var idvn=$('n_ks'+idx_ks).value;
	    var idvv='id'+idx_ks;
	    if(i==1)
	       up+=idvv+"="+idvn;  
  	    else
 	       up+="&"+idvv+"="+idvn;  
	}
	        
    var uprd="simpan_ind_ks.php?"+up+"&jmlh_ks="+v_ks+"&r="+new Date().getTime();	

          new Ajax.Request(
          uprd,
          {
               method:"get",
			   onSuccess: rsltsik1,
               onFailure: rsltsik1

          }
          );
		  $('w_d_ks').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
}
function rsltsik1(request) {
    $('w_d_ks').innerHTML="<font size='3' color='blue'>Update Data Indikator Tahapan KS Berhasil</font>";
	
}
function s_id_ks() 
{
    ajax_hideTooltip();
	var k_kss=false; var idx_ks=""; var strp_isi="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if((strp_isi.toLowerCase()!="v" && strp_isi.toLowerCase()!="x*" && strp_isi.toLowerCase()!="x" && strp_isi.toLowerCase()!="-") && strp_isi!="")    
		{
              k_kss=true;
			  var idx_n=idx_ks;
			  break;
			  
		}
	}   
	var k_ks=false; var idx_ks="";
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if(strp_isi=='')    
		{
              k_ks=true;
			  var idx_k=idx_ks;
			  break;
		}
	}   
	if(k_kss)
	{
		alert("Data Yang Dimasukkan Tidak Boleh Berisi Selain X* X V -") ;
		$('f_ks'+idx_n).select();
    }
	else if(k_ks)
	{
		alert("Data Yang Dimasukkan Tidak Lengkap") 
		$('f_ks'+idx_k).focus();
    }
	
	else
	   p_s_id_ks();
}
function p_s_id_ks() 
{
	var idx_ks=""; var v_ks=$('jml_ks').value; var up=""; var j=1;
	for(var i=1;i<$('jml_ks').value;i++)
	{
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if(strp_isi.toLowerCase()=="v")
		   var idvn=1;
		else if(strp_isi.toLowerCase()=="x")
		   var idvn=2;
		else if(strp_isi.toLowerCase()=="x*")
		   var idvn=3;   
		else if(strp_isi.toLowerCase()=="-")
		   var idvn=4;   
	    var idvv='id'+idx_ks;
		if(idvn!='')
		{
			if(j==1)
	           up+=idvv+"="+idvn;  
			else
 	           up+="&"+idvv+"="+idvn;     
			j++;
		}
	    
  	    
	}
	        
    var uprd="simpan_ind_ks.php?"+up+"&jmlh_ks="+j+"&r="+new Date().getTime();	

          new Ajax.Request(
          uprd,
          {
               method:"get",
			   onSuccess: rsltsik,
               onFailure: rsltsik

          }
          );
		  $('s_d_i_t_k').disabled=true;
		  $('w_d_ks').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
		  
}
function rsltsik(request) {
    $('w_d_ks').innerHTML="<font size='3' color='blue'>Penyimpanan Data Indikator Tahapan KS Berhasil</font>";
	$('divhcemkki').innerHTML="Data Indikator Tahapan KS Nomor KKI : "+$('hcemkki').value;
	$('s_d_i_t_k').disabled=false;
	$('s_d_i_t_k').style.display='none';
	$('u_d_i_t_k').style.display='';
	$('td_sts_m_k').innerHTML=request.responseText;


}
function cek_ks(t,id)
{
		     var s=t.value.split("|");
			 $('g_ks'+id).innerHTML=s[1];
			 $('n_ks'+id).value=s[0];
			 ajax_hideTooltip();
}
function td_ks(t,id)
{
	  ajax_showTooltip('d_ks_1.php?id='+id+'&r='+new Date().getTime(),t);  return false;
}
function t_ks(evt,t,id,cks)
{
	  ajax_hideTooltip();
	  $('w_d_ks').innerHTML='&nbsp;';
	  var jmlhd=$('jml_ks').value-1;
	  t.value=t.value.toUpperCase();
	  
	  
	  if(evt.keyCode==34)
	  {
	   t.value='-'; 
	   if(!$('n_val'+id+4))
		{
			alert("Posisi - Tidak Valid");   
			t.value='';
		}
		else
		{
		
		
		if(cks<jmlhd)
		{  
		  var cks=cks+1;
		  var spc=$('t_ks'+cks).value;
		  $('f_ks'+spc).focus();
		}
		else if(cks==jmlhd)
		{
		  var spc=$('t_ks'+1).value;
		  $('f_ks'+spc).focus();  
		}
	  }
	    
	  }
	  if(evt.keyCode==35)
	  {
	    t.value='X'; 
		if(cks<jmlhd)
		{  
		  var cks=cks+1;
		  var spc=$('t_ks'+cks).value;
		  $('f_ks'+spc).focus();
		}
		else if(cks==jmlhd)
		{
		  var spc=$('t_ks'+1).value;
		  $('f_ks'+spc).focus();  
		}
	    
	  }
	  if(evt.keyCode==37)
	  {
	    
		if(cks>1)
		{  
		  var cks=cks-1;
		  var spc=$('t_ks'+cks).value;
		  $('f_ks'+spc).focus();
		}
		else if(cks==1)
		{
		  var spc=$('t_ks'+jmlhd).value;
		  $('f_ks'+spc).focus();  
		}
	    
	  }
		
	  if(evt.keyCode==38 || evt.keyCode==13)
	  {
	    t.value='V'; 
		if(cks<jmlhd)
		{  
		  var cks=cks+1;
		  var spc=$('t_ks'+cks).value;
		  $('f_ks'+spc).focus();
		}
		else if(cks==jmlhd)
		{
		  var spc=$('t_ks'+1).value;
		  $('f_ks'+spc).focus();  
		}
	    
	  }
	  if(evt.keyCode==39)
	  {
	    if(cks<jmlhd)
		{  
		  var cks=cks+1;
		  var spc=$('t_ks'+cks).value;
		  $('f_ks'+spc).focus();
		}
		else if(cks==jmlhd)
		{
		  var spc=$('t_ks'+1).value;
		  $('f_ks'+spc).focus();  
		}
	    
	  }
	 
	 
	  
	  
	  if(t.value=='-')
	  {
		  if(!$('n_val'+id+4))
		  {
			alert("Posisi - Tidak Valid");   
			t.value='';
		  }  
	  }
}
function tambah_ang()
{
	for(var i=1;i<16;i++)
	{
	    if($('div_ks'+i).style.display=='none')    
		{
			  $('div_ks'+i).style.display='';
			  break;
		}
	}
}
function hapus_ang(i)
{
	  			  $('div_ks'+i).style.display='none';
			
	
}
function ind_ks()
{
   		  ajax_hideTooltip();
		  var urlb="ind_ks_no.php?r="+new Date().getTime();
          new Ajax.Request(
          urlb,
          {
               method:"get",
			   onSuccess: rslt1,
               onFailure: rslt1

          }
          );
		  
		  
            $('panel1').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<B>Silahkan Tunggu, Sedang Proses ...<B>";
		
	      

	   
}

function rslt1(request) {
$('panel1').innerHTML = request.responseText;
$('isi_h_tab').value=2;	
}

function d_k_e()
{
   		 $('panel0').innerHTML='';
		 $('btn_mn_2').style.backgroundColor='#9999CC';
		 $('btn_mn_1').style.backgroundColor='';
         $('btn_mn_3').style.backgroundColor='';
		 $('isi_h_kel_kak').value=2;
		 ajax_hideTooltip();
		 var urlb="form_data_keluarga_ajax_edit.php?r="+new Date().getTime();
          new Ajax.Request(
          urlb,
          {
               method:"get",
			   onSuccess: rsltdke,
               onFailure: rsltdke

          }
          );
		  
		  
            $('panel1').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<B>Silahkan Tunggu, Sedang Proses ...<B>";
		
	      

	   
}

function rsltdke(request) {
$('panel1').innerHTML = request.responseText;
new Ajax.Autocompleter('kki','hint','auto_individu.php',{minChars:6, callback: eventCallBack });
	
}

function d_k_i()
{
   		 $('panel1').innerHTML='';
		 $('btn_mn_1').style.backgroundColor='#9999CC';
		 $('btn_mn_2').style.backgroundColor='';
         $('btn_mn_3').style.backgroundColor='';
		 $('isi_h_kel_kak').value=1;
		 ajax_hideTooltip();
		 var urlb="form_data_keluarga_ajax_edit_w.php?r="+new Date().getTime();
          new Ajax.Request(
          urlb,
          {
               method:"get",
			   onSuccess: rsltdki,
               onFailure: rsltdki

          }
          );
		  
		  
            $('panel0').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<B>Silahkan Tunggu, Sedang Proses ...<B>";
		
	      

	   
}

function rsltdki(request) {
$('panel0').innerHTML = request.responseText;
new Ajax.Autocompleter('kki','hint','auto_individu.php',{minChars:6, callback: eventCallBack });
	
}

function d_k()
{
		 ajax_hideTooltip();
		 $('btn_mn_2').style.backgroundColor='#9999CC';
		 $('btn_mn_1').style.backgroundColor='';
         $('btn_mn_3').style.backgroundColor='';
		 var urlb="form_data_keluarga_menu_ajax.php?r="+new Date().getTime();
          new Ajax.Request(
          urlb,
          {
               method:"get",
			   onSuccess: rslt2,
               onFailure: rslt2

          }
          );
		  
		  
            $('panel0').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<B>Silahkan Tunggu, Sedang Proses ...<B>";
		
	      

	   
}

function rslt2(request) {
$('panel0').innerHTML = request.responseText;
new Ajax.Autocompleter('kki','hint','auto_individu.php',{minChars:6, callback: eventCallBack });
	
}

function d_a_k()
{
   		  ajax_hideTooltip();
		  var urlb="form_input_anggota_keluarga.php?r="+new Date().getTime();
          new Ajax.Request(
          urlb,
          {
               method:"get",
			   onSuccess: rslt3,
               onFailure: rslt3

          }
          );
		  
		  
            $('panel2').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<B>Silahkan Tunggu, Sedang Proses ...<B>";
		
	      

	   
}

function rslt3(request) {
$('panel2').innerHTML = request.responseText;
$('isi_h_tab').value=3;
}
function proc_kb() {
	
     for(i=0;i<$('h_i').value;i++)
	 {
	   $('id_src'+i).disabled=false;
   
	 }
	
	$('alt_kb').disabled=false;
	$('s_bkb').selectedIndex=0;
	$('s_bkb').disabled=true;
	$('div_sub').style.display='';
	$('div_sub_sub_1').style.display='none';
    
}
function proc_bkb() {
	
     for(i=0;i<$('h_i').value;i++)
	 {
	   $('id_src'+i).style.backgroundColor='';
	   $('id_src'+i).checked=false;
	   $('id_src'+i).disabled=true;
	   
	 }
	 for(i=0;i<$('h_impl').value;i++)
	 {
	   $('ipl'+i).style.backgroundColor='';
	   $('ipl'+i).checked=false;
	 }
	$('div_sub_sub_1').style.display='';
	$('alt_kb').selectedIndex=0;
	$('alt_kb').disabled=true;
	$('s_bkb').disabled=false;
	$('div_sub_sub').style.display='none';
	$('div_sub').style.display='none';
  
	
}

function s_d_k_1() {
	
	    for(var j=1;j<=20;j++)
	    {
			 if($('tr_f_agt'+j).style.display=='')
		     {	 
				 
				 $('kak'+j).style.backgroundColor="";   
			 }
	    }
		
		
		
		
		var ada_kbr=false;
		var arr_kbr=new Array();
		
		if($('kak1').value=='')
		{
			    alert("Kode Anggota Keluarga Harus Diisi");
				$('kak1').focus();
		}
		
		for(var i=1;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('nm'+i).value=='')
				    {   
					   
					   alert("Nama Anggota Keluarga Harus Diisi");
					   $('nm'+i).style.backgroundColor="yellow";
				       $('nm'+i).focus();
					   break;
					}
				}
				
			 }  
	    }
		
		for(var i=1;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('tpt_lhr'+i).value=='')
				    {   
					   
					   alert("Tempat Lahir Anggota Keluarga Harus Diisi");
					   $('tpt_lhr'+i).style.backgroundColor="yellow";
				       $('tpt_lhr'+i).focus();
					   break;
					}
				}
				
			 }  
	    }
		
		
		
		for(var i=1;i<20;i++)
	    {
		    arr_kbr[1]=i;
			var k=2;
			if($('tr_f_agt'+i).style.display=='')
		    {
				for(var j=i+1;j<=20;j++)
	            {
		           if($('tr_f_agt'+j).style.display=='')
		           {
					 if($('kak'+i).value!='')
					 {
					  if($('kak'+i).value==$('kak'+j).value)
					  {
					       ada_kbr=true;  
						   arr_kbr[k]=j;
						   k++;
						   
					  }
					 }
				   }
	            }   
			}
			if(ada_kbr)
			   break;
        }
		
		if(ada_kbr)
		{
			alert("Terdapat Nomor KAK Yang Sama");
			for(var j=1;j<arr_kbr.length;j++)
	        {
				 $('kak'+arr_kbr[j]).style.backgroundColor="yellow";   
			}
		}

}

function s_d_k_idk() {
	   s_d_k();
}

function bg_wrn_slct()
{
	    for(var j=1;j<=20;j++)
	    {
			 if($('tr_f_agt'+j).style.display=='')
		     {	 
				 $('hub'+j).style.backgroundColor="";   
				 
			 }
	    } 
}

function s_d_k() {
	
        $('btn_mn_2').disabled=true;
	    $('btn_mn_3').disabled=true;
		
		for(var j=1;j<=20;j++)
	    {
			 if($('tr_f_agt'+j).style.display=='')
		     {	 
				 $('hub'+j).style.backgroundColor="";   
				 $('kak'+j).style.backgroundColor="";   
			 }
	    }
  var fm_atas=true;		
  if($('f_d_k_prop').value=='')
  {
      alert("Pilih Provinsi Terlebih Dahulu");
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('f_d_k_kab_kota').value=='')
  {
      alert("Pilih Kabupaten/Kota Terlebih Dahulu");	 
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('f_d_k_kec').value=='')
  {
      alert("Pilih Kecamatan Terlebih Dahulu");
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('f_d_k_kel').value=='')
  {
      alert("Pilih Desa/Kelurahan Terlebih Dahulu");	  
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('f_d_k_rw').value=='')
  {
      alert("Pilih Dusun/RW Terlebih Dahulu");	  
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('f_d_k_rt').value=='')
  {
      alert("Pilih RT Terlebih Dahulu");
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('kdpos').value=='')
  {
      alert("Isi Kode Pos Terlebih Dahulu");
	  $('kdpos').focus();
	  $('kdpos').style.backgroundColor="yellow";
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('nourt').value=='')
  {
      alert("Isi Nomor Urut Terlebih Dahulu");
	  $('nourt').focus();
	  $('nourt').style.backgroundColor="yellow";
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  return false;
  }
  else if($('kki').value=='')
  {
      alert("Isi Nomor Kode Keluarga Indonesia (KKI) Terlebih Dahulu");
	  $('kki').style.backgroundColor="yellow";   
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  $('kki').select();
	  return false;
  }
  else if($('kki').value.length!=7)
  {
      alert("Jumlah Digit/Karakter Nomor Kode Keluarga Indonesia (KKI) Harus Sama Dengan Tujuh (7)");
	  $('kki').style.backgroundColor="yellow";   
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  $('kki').select();
	  return false;
  } 
   
   
   
		var ada_kbr=false;
		var arr_kbr=new Array();
		var nm_ksg=false;
		var tpt_lhr_ksg=false;
		var fm_atas=true;
		if($('kak1').value=='')
		{
			    alert("Kode Anggota Keluarga Harus Diisi");
				$('kak1').style.backgroundColor="yellow";   
				$('btn_mn_2').disabled=false;
                $('btn_mn_3').disabled=false;
				$('kak1').focus();
				return false;
		}
		
		for(var i=1;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('nm'+i).value=='')
				    {   
					   
					   alert("Nama Anggota Keluarga Harus Diisi");
					   nm_ksg=true;
					   $('btn_mn_2').disabled=false;
                       $('btn_mn_3').disabled=false;
					   $('nm'+i).style.backgroundColor="yellow";
				       $('nm'+i).focus();
					   return false;
					   
					}
				}
				
			 }  
	    }   
		
		var arr_kk_kpl=new Array();
		var idx_kk_kpl=0;
		for(var i=2;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('hub'+i).selectedIndex==0)
					{
				       arr_kk_kpl[idx_kk_kpl]=i; 
					   idx_kk_kpl++;
					}
				}
				
			 }  
	    }   
		 
		if(arr_kk_kpl.length>0)
		{
			alert("Hubungan Dengan KK 'KEPALA KELURAGA' Tidak Boleh Lebih Dari Satu ");
			$('btn_mn_2').disabled=false;
            $('btn_mn_3').disabled=false;
		    $('hub1').style.backgroundColor="yellow";
			for(var j=0;j<arr_kk_kpl.length;j++)
	        {
				 $('hub'+arr_kk_kpl[j]).style.backgroundColor="yellow";   
			}
			     return false;    
		}
		 
		 
		  
	    for(var i=1;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('tlhr'+i).value=='')
				    {   
					   
					   alert("Tanggal Lahir Anggota Keluarga Harus Diisi FF");
					   $('btn_mn_2').disabled=false;
                       $('btn_mn_3').disabled=false;
					   nm_ksg=true;
					   
					   $('tlhr'+i).style.backgroundColor="yellow";
				   
					   return false;
					   
					}else{
						var tgl_sekarang = new Date();
						var dtStr =  $('tlhr'+i).value;
	            		var pos2 = dtStr.indexOf('-');
	            		var pos1 = dtStr.indexOf('-', pos2 + 1);
	            		var strDay = dtStr.substring(0, pos1);
	            		var strMonth = dtStr.substring(pos2 + 1, pos1);
	            		var strYear = dtStr.substring(pos1 + 1);
						
						var month = parseInt(strMonth);
	            		var day = parseInt(strDay);
	            		var TahunLahir = parseInt(strYear);
						
						var tgl_lahir  = new Date(TahunLahir, month, day);
						var selisih  = (Date.parse(tgl_sekarang.toGMTString())-Date.parse(tgl_lahir.toGMTString()))/(1000*60*60*24*365);
						var usia = Math.floor(selisih);
						if(usia>=150){
							alert("Usia terlalu tua " + usia +",/n Silahkan ubah kembali tanggal lahirnya");
	                    	if ($('mts' + i).selectedIndex == 0) $('tlhr' + i).focus();
	
	                    	nm_ksg = true;
	
	                    	$('tlhr' + i).style.backgroundColor = "yellow";
	
	                    	return false;
						}else{
							
						}
					}
				}
				
			 }  
	    }  
		
		for(var i=1;i<=20;i++)
	    {
		     if($('tr_f_agt'+i).style.display=='')
		     {
                if($('kak'+i).value!='')
				{
					if($('yd1'+i).checked==false && $('yd2'+i).checked==false && $('div_pos_yd'+i).style.display=='')
					{
                       alert("Ikut Program Posyandu Harus Dipilih");
					   $('btn_mn_2').disabled=false;
                       $('btn_mn_3').disabled=false;
					   $('yd1'+i).style.backgroundColor="yellow";
					   $('yd2'+i).style.backgroundColor="yellow";
				       return false;
					   
					}
				}
				
			 }  
	    }   
		
		
		  for(var i=1;i<20;i++)
	      {
		    arr_kbr[1]=i;
			var k=2;
			if($('tr_f_agt'+i).style.display=='')
		    {
				for(var j=i+1;j<=20;j++)
	            {
		           if($('tr_f_agt'+j).style.display=='')
		           {
					 if($('kak'+i).value!='')
					 {
					  if($('kak'+i).value==$('kak'+j).value)
					  {
					       ada_kbr=true;  
						   arr_kbr[k]=j;
						   k++;
						   
					  }
					 }
				   }
	            }   
			}
			if(ada_kbr)
			   break;
        }
		
		if(ada_kbr)
		{
			alert("Terdapat Nomor Kode Anggota Keluarga KAK Yang Sama");
			$('btn_mn_2').disabled=false;
            $('btn_mn_3').disabled=false;
			for(var j=1;j<arr_kbr.length;j++)
	        {
				 $('kak'+arr_kbr[j]).style.backgroundColor="yellow";   
			}
			return false;
		}	    
	 
   
   
  
  var fm_tengah=true;
  if($('btm1').checked==false && $('btm2').checked==false)
  {  
      alert("Bantuan Modal Harus Dipilih");
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  $('btm1').style.backgroundColor="yellow";  
	  $('btm2').style.backgroundColor="yellow";  
	  return false;
  }
  else if($('pus1').checked==false && $('pus2').checked==false)
  {
      alert("PUS Harus Dipilih");
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  $('pus1').style.backgroundColor="yellow";  
	  $('pus2').style.backgroundColor="yellow";  
	  return false;
  }
  else if($('pus1').checked==true)
  {
    
		
	if($('kb').checked==false && $('kb_1').checked==false)
	{
      alert("Pilih Option Peserta KB/Bukan Peserta KB Terlebih Dahulu");	
	  $('btn_mn_2').disabled=false;
      $('btn_mn_3').disabled=false;
	  $('kb').style.backgroundColor="yellow";
	  $('kb_1').style.backgroundColor="yellow";
	  return false;
	}
    else if($('kb').checked==true)
    {
         var sli=document.getElementById('alt_kb').selectedIndex;
		
		 if(document.getElementById('alt_kb').options[sli].text.toLowerCase()=='implant')
         {
		      var cek_impl=false;  
			  for(i=0;i<$('h_impl').value;i++)
	          { 
	               if($('ipl'+i).checked==true)
	               {
					   var cek_impl=true;
					   var ipl=$('ipl'+i).value;
					   break;
				   }
	          }
		 }
		 else
		    var cek_impl=true;
		
		var cek=false
	    for(i=0;i<$('h_i').value;i++)
	    {
	       if($('id_src'+i).checked==true)
	       {
		      cek=true;
			  var src=$('id_src'+i).value;
			  break;
	       }
	    }
	
	if(!cek)
	{
	    alert("Pilih Tempat Pelayanan KB (Pemerintah/Swasta) Terlebih Dahulu"); 
		$('btn_mn_2').disabled=false;
        $('btn_mn_3').disabled=false;
	    for(i=0;i<$('h_i').value;i++)
	    {
	       $('id_src'+i).style.backgroundColor="yellow";
		   
	    }
	   
	   return false;
	}
	else if(!cek_impl)
	{
	   alert("Pilih Option 'Implan Akan Dicabut' Terlebih Dahulu"); 
	   $('btn_mn_2').disabled=false;
       $('btn_mn_3').disabled=false;
	   for(i=0;i<$('h_impl').value;i++)
	    {
	       $('ipl'+i).style.backgroundColor="yellow";
		   
	    }
	   return false;
	}
	
   }
  }
		
		
		
		
	
	
	
	  ajax_hideTooltip();
	  var k_kss=false; var idx_ks=""; var strp_isi="";
	  
	  for(var i=1;i<$('jml_ks').value;i++)
	  {
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if((strp_isi.toLowerCase()!="v" && strp_isi.toLowerCase()!="x" && strp_isi.toLowerCase()!="x*" && strp_isi.toLowerCase()!="-") && strp_isi!="")    
		{
              k_kss=true;
			  var idx_n=idx_ks;
			  break;
			  
		}
	  }   
	  var k_ks=false; var idx_ks="";
	  for(var i=1;i<$('jml_ks').value;i++)
	  {
	    idx_ks=$('t_ks'+i).value;
		strp_isi=strp_slashes($('f_ks'+idx_ks).value);
		if(strp_isi=='')    
		{
              k_ks=true;
			  var idx_k=idx_ks;
			  break;
		}
	  }   
	  if(k_kss)
	  {
		alert("Data Indikator Dan Status Tahapan Keluarga Tidak Boleh Berisi Selain X* X V -") ;
		$('btn_mn_2').disabled=false;
        $('btn_mn_3').disabled=false;
		$('f_ks'+idx_n).select();
		return false;
      }
	  else if(k_ks)
	  {
		alert("Data Indikator Dan Status Tahapan Keluarga Tidak Lengkap"); 
		$('btn_mn_2').disabled=false;
        $('btn_mn_3').disabled=false;
		$('f_ks'+idx_k).focus();
		return false;
      }	   
	
	    var XMLHttpRequestObject=ajaxpustaka();
		var ada_kki=false;
		if(XMLHttpRequestObject) 
		{
			 var kel = encodeURIComponent(document.getElementById('f_d_k_kel').value);
		     var kki = kel+document.getElementById('kki').value;
			 var urlp="kki="+kki+"&r="+new Date().getTime();  
		   
		     XMLHttpRequestObject.open("POST", "cek_kki.php");
             $('w_d_k_1').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
             
          XMLHttpRequestObject.onreadystatechange = function()
          {
      
			  
			  if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			  {
				 if(XMLHttpRequestObject.responseText==1)
				 {
				     
					 alert("NOMOR KKI " +kki+ " SUDAH ADA DIDATABASE SILAHKAN DIULANGI !!!");
					 $('btn_mn_2').disabled=false;
                     $('btn_mn_3').disabled=false;
					 $('kki').select();
					 $('w_d_k_1').innerHTML='';
					 ada_kki=true;
				 }
				 else
				 {
					   $('w_d_k_1').innerHTML='';
					   
					   kak_cek(1);   
				 }
				 
              }
          }
          XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          XMLHttpRequestObject.send(urlp);
		}
	     
		 
		
		
		
   
}
function src_bg()
{
	    for(i=0;i<$('h_i').value;i++)
	    {
	       $('id_src'+i).style.backgroundColor="";
		   
	    }   
}

function ipl_bg()
{
	    for(i=0;i<$('h_impl').value;i++)
	    {
	       $('ipl'+i).style.backgroundColor="";
		   
	    }   
}

function u_d_k() {
  
  if($('f_d_k_prop').value=='')
      alert("Pilih Propinsi Terlebih Dahulu");
  else if($('f_d_k_kab_kota').value=='')
      alert("Pilih Kab/Kota Terlebih Dahulu");
  else if($('f_d_k_kec').value=='')
      alert("Pilih Kecamatan Terlebih Dahulu");
  else if($('f_d_k_kel').value=='')
      alert("Pilih Kelurahan Terlebih Dahulu");	  
  else if($('f_d_k_rw').value=='')
      alert("Pilih RW Terlebih Dahulu");	  
  else if($('f_d_k_rt').value=='')
      alert("Pilih RT Terlebih Dahulu");
  else if($('kdpos').value=='')
  {
      alert("Isi Kode Pos Terlebih Dahulu");
	  $('kdpos').focus();
	  
  }
  else if($('nourt').value=='')
  {
      alert("Isi Nomor Urut Rumah Tangga Terlebih Dahulu");
	  $('nourt').focus();
  }
  else if($('kki').value=='')
  {
      alert("Isi Nomor Kode Keluarga Indonesia Terlebih Dahulu");
	  $('kki').focus();
  }
  else if($('btm1').checked==false && $('btm2').checked==false)
      alert("Bantuan Modal Harus Dipilih");
  
  else if($('pus1').checked==false && $('pus2').checked==false)
      alert("PUS Harus Dipilih");
  else if($('pus1').checked==true)
  {
    if($('kb').checked==false && $('kb_1').checked==false)
      alert("Pilih Option Peserta KB/Bukan Peserta KB Terlebih Dahulu");	
    else if($('kb').checked==true)
    {
        
		var sli=document.getElementById('alt_kb').selectedIndex;
		
		 if(document.getElementById('alt_kb').options[sli].text.toLowerCase()=='implant')
         {
		      var cek_impl=false;  
			  for(i=0;i<$('h_impl').value;i++)
	          { 
	               if($('ipl'+i).checked==true)
	               {
					   var cek_impl=true;
  					   var ipl=$('ipl'+i).value;
					   break;
				   }
	          }
		 }
		 else
		    var cek_impl=true;
		
		
		var cek=false
	    for(i=0;i<$('h_i').value;i++)
	    {
	       if($('id_src'+i).checked==true)
	       {
		      cek=true;
			  var src=$('id_src'+i).value;
			  break;
	       }
	    }
	 if(!cek)
	   alert("Pilih Sumber KB (Pemerintah,swasta,..) Terlebih Dahulu");  
	 else if(!cek_impl)
	   alert("Pilih Implan Terlebih Dahulu");      
	 else
	 {
		var XMLHttpRequestObject=ajaxpustaka();
		if(XMLHttpRequestObject) {

		  var prop = encodeURIComponent(document.getElementById('f_d_k_prop').value);
 		  var kab_kota = encodeURIComponent(document.getElementById('f_d_k_kab_kota').value);
		  var kec = encodeURIComponent(document.getElementById('f_d_k_kec').value);
 		  var kel = encodeURIComponent(document.getElementById('f_d_k_kel').value);
		  var rw = encodeURIComponent(document.getElementById('f_d_k_rw').value);
		  var rt = encodeURIComponent(document.getElementById('f_d_k_rt').value);
		  var kdpos = encodeURIComponent(document.getElementById('kdpos').value);
  		  var nourt = encodeURIComponent(document.getElementById('nourt').value);
  		  var kki = kel+document.getElementById('kki').value;
		  var kki_unik = encodeURIComponent(document.getElementById('kki').value);
		  var s_rt = rt;
		  var pus = encodeURIComponent(document.getElementById('pus1').value);
		  var alt_kb = encodeURIComponent(document.getElementById('alt_kb').value);
	      
		  if($('btm1').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm1').value);
		  else if($('btm2').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm2').value);	
  		   
		  var sli=document.getElementById('alt_kb').selectedIndex;
          if(document.getElementById('alt_kb').options[sli].text.toLowerCase()=='implant')
          {
      		  var urlp="prop="+prop+"&kab_kota="+kab_kota+"&kec="+kec+"&kel="+kel+"&rw="+rw+"&rt="+rt+"&kdpos="+kdpos+"&nourt="+nourt+"&kki="+kki+"&pus="+pus+"&src="+src+"&ipl="+ipl+"&alt="+alt_kb+"&btm="+btm+"&s_rt="+s_rt+"&kki_unik="+kki_unik+"&r="+new Date().getTime();  
          }
		  else
		      var urlp="prop="+prop+"&kab_kota="+kab_kota+"&kec="+kec+"&kel="+kel+"&rw="+rw+"&rt="+rt+"&kdpos="+kdpos+"&nourt="+nourt+"&kki="+kki+"&pus="+pus+"&src="+src+"&alt="+alt_kb+"&btm="+btm+"&s_rt="+s_rt+"&kki_unik="+kki_unik+"&r="+new Date().getTime();  
		   
		  XMLHttpRequestObject.open("POST", "update_data_keluarga.php");
          $('w_d_k').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
          $('u_d_kk').disabled=true;
		  $('kki').disabled=true;
		  
		 XMLHttpRequestObject.onreadystatechange = function()
          {
      
			  if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			  {
				 if(XMLHttpRequestObject.responseText==1)
				 {
				     
					 $('w_d_k').innerHTML="<font size='3' color='red'>Nomor Kode Keluarga Indonesia (KKI) "+$('div_kki_rt').innerHTML+document.getElementById('kki').value+" Sudah Ada Didatabase, Silahkan Diulangi Lagi</font>";
                     $('u_d_kk').disabled=false;
					 $('kki').disabled=false;
					 $('kki').select();
				 }	 
				 else
				 {
				 	 $('u_d_kk').disabled=false;
		             $('kki').disabled=false;
					 $('w_d_k').innerHTML="<font size='3' color='blue'>Update Data Keluarga Berhasil</font>";
					 $('fdk_cem').innerHTML="Form Data Keluarga NO. KKI : "+XMLHttpRequestObject.responseText;				     
				 }
              }
          }
          XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          XMLHttpRequestObject.send(urlp);
		  
        }
		 
	 }
	 
	 
   }
   else if($('kb_1').checked==true)
   {
	     var XMLHttpRequestObject=ajaxpustaka();
		if(XMLHttpRequestObject) {

		  var prop = encodeURIComponent(document.getElementById('f_d_k_prop').value);
 		  var kab_kota = encodeURIComponent(document.getElementById('f_d_k_kab_kota').value);
		  var kec = encodeURIComponent(document.getElementById('f_d_k_kec').value);
 		  var kel = encodeURIComponent(document.getElementById('f_d_k_kel').value);
		  var rw = encodeURIComponent(document.getElementById('f_d_k_rw').value);
		  var rt = encodeURIComponent(document.getElementById('f_d_k_rt').value);
		  var kdpos = encodeURIComponent(document.getElementById('kdpos').value);
  		  var nourt = encodeURIComponent(document.getElementById('nourt').value);
	      var pus = encodeURIComponent(document.getElementById('pus1').value);
		 
  		  var s_bkb = encodeURIComponent(document.getElementById('s_bkb').value);
  		  if($('btm1').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm1').value);
		  else if($('btm2').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm2').value);	
			
  		  var kki = kel+document.getElementById('kki').value;	
		  var kki_unik = encodeURIComponent(document.getElementById('kki').value);
  		  var s_rt = rt;
		  var urlp="prop="+prop+"&kab_kota="+kab_kota+"&kec="+kec+"&kel="+kel+"&rw="+rw+"&rt="+rt+"&kdpos="+kdpos+"&nourt="+nourt+"&kki="+kki+"&pus="+pus+"&s_bkb="+s_bkb+"&btm="+btm+"&s_rt="+s_rt+"&kki_unik="+kki_unik+"&r="+new Date().getTime();  
		  
		  XMLHttpRequestObject.open("POST", "update_data_keluarga.php");
          $('w_d_k').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
          $('u_d_kk').disabled=true;
		  $('kki').disabled=true;
		  XMLHttpRequestObject.onreadystatechange = function()
          {
             		   
		      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			  {
				 if(XMLHttpRequestObject.responseText==1)
				 {
					 $('w_d_k').innerHTML="<font size='3' color='red'>Nomor Kode Keluarga Indonesia (KKI) "+$('div_kki_rt').innerHTML+document.getElementById('kki').value+" Sudah Ada Didatabase, Silahkan Diulangi Lagi</font>";
                     $('u_d_kk').disabled=false;
  		             $('kki').disabled=false;
					 $('kki').select();
				 }	 
				 else
				 {
				 	 $('u_d_kk').disabled=false;
  		             $('kki').disabled=false;
					 $('w_d_k').innerHTML="<font size='3' color='blue'>Update Data Keluarga Berhasil</font>";
					 $('fdk_cem').innerHTML="Form Data Keluarga NO. KKI : "+XMLHttpRequestObject.responseText;					 
				     
				 }
              }
          }
          XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          XMLHttpRequestObject.send(urlp);
        }
    
   }
  }
  else if($('pus2').checked==true)
  {
        	var XMLHttpRequestObject=ajaxpustaka();
		if(XMLHttpRequestObject) {

		  var prop = encodeURIComponent(document.getElementById('f_d_k_prop').value);
 		  var kab_kota = encodeURIComponent(document.getElementById('f_d_k_kab_kota').value);
		  var kec = encodeURIComponent(document.getElementById('f_d_k_kec').value);
 		  var kel = encodeURIComponent(document.getElementById('f_d_k_kel').value);
		  var rw = encodeURIComponent(document.getElementById('f_d_k_rw').value);
		  var rt = encodeURIComponent(document.getElementById('f_d_k_rt').value);
		  var kdpos = encodeURIComponent(document.getElementById('kdpos').value);
  		  var nourt = encodeURIComponent(document.getElementById('nourt').value);
	      var pus = encodeURIComponent(document.getElementById('pus2').value);
		 
  		  if($('btm1').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm1').value);
		  else if($('btm2').checked==true)
  		    var btm = encodeURIComponent(document.getElementById('btm2').value);	
			
  		  var kki = kel+document.getElementById('kki').value;
		  var kki_unik = encodeURIComponent(document.getElementById('kki').value);
  		  var s_rt = rt;
		  var urlp="prop="+prop+"&kab_kota="+kab_kota+"&kec="+kec+"&kel="+kel+"&rw="+rw+"&rt="+rt+"&kdpos="+kdpos+"&nourt="+nourt+"&kki="+kki+"&pus="+pus+"&btm="+btm+"&s_rt="+s_rt+"&kki_unik="+kki_unik+"&r="+new Date().getTime();  
		  
		  XMLHttpRequestObject.open("POST", "update_data_keluarga.php");
          $('w_d_k').innerHTML="<IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses ...<B></font>"; 				 
          $('u_d_kk').disabled=true;
		  $('kki').disabled=true;
		  XMLHttpRequestObject.onreadystatechange = function()
          {
             		   
		      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			  {
				 if(XMLHttpRequestObject.responseText==1)
				 {
					 $('w_d_k').innerHTML="<font size='3' color='red'>Nomor Kode Keluarga Indonesia (KKI) "+$('div_kki_rt').innerHTML+document.getElementById('kki').value+" Sudah Ada Didatabase, Silahkan Diulangi Lagi</font>";
                     $('u_d_kk').disabled=false;
  		             $('kki').disabled=false;
					 $('kki').select();
				 }	 
				 else
				 {
				 	 $('u_d_kk').disabled=false;
  		             $('kki').disabled=false;
					 $('w_d_k').innerHTML="<font size='3' color='blue'>Update Data Keluarga Berhasil</font>";
					 $('fdk_cem').innerHTML="Form Data Keluarga NO. KKI : "+XMLHttpRequestObject.responseText;				     
				 }
              }
          }
          XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
          XMLHttpRequestObject.send(urlp);
        }     
  }
}