<?php 
session_start();
if(!isset($_SESSION['un']))
   exit();
include "../koneksi/koneksi.inc.php";
include "../brt_main_cms_1.php";
unset($_SESSION['kki']);
unset($_SESSION['s_rt']);
unset($_SESSION['id_fam']);
unset($_SESSION['neigh']);
unset($_SESSION['sts_miskin']);
unset($_SESSION['sts_ks']);
?>
<html>
<head>
<title>form edit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="tooltipsajax/js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="tooltipsajax/js/ajax.js"></script>
<script type="text/javascript" src="tooltipsajax/js/ajax-tooltip.js"></script>	
<link rel="stylesheet" href="tooltipsajax/css/ajax-tooltip.css" media="screen" type="text/css">
<script src="../js/ajaxlibinduk.js" type="text/javascript"></script>
<script src="../js/prototype.js" type="text/javascript"></script>
<script src="../js/scriptaculous.js" type="text/javascript"></script>
<script src="../js/effects.js" type="text/javascript"></script>
<script src="../js/controls.js" type="text/javascript"></script>
<script src="../js/instobj.js" type="text/javascript"></script>
<script src="prop.js" type="text/javascript"></script>
<script src="kab_kota.js" type="text/javascript"></script>
<script src="kec.js" type="text/javascript"></script>
<script src="kel.js" type="text/javascript"></script>
<script src="rw.js" type="text/javascript"></script>
<script src="lib.js" type="text/javascript"></script>
<script src="lib_a_k.js" type="text/javascript"></script>
<script src="lib_ks.js" type="text/javascript"></script>
<script src="lib_kak.js" type="text/javascript"></script>
<script src="lib_kak_u.js" type="text/javascript"></script>
<script src="lib_u_d_k.js" type="text/javascript"></script>
<link rel="stylesheet" href="../dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link href="../js/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script src="../js/SpryTabbedPanels.js" type="text/javascript"></script>
<style type="text/css">
    div.auto_complete {
      position:absolute;
      width:250px;
      background-color:white;
      border:1px solid #888;
      margin:0px;
      padding:0px;
    }
    ul.contacts  {
      list-style-type: none;
      margin:0px;
      padding:0px;
    }
    ul.contacts li.selected { background-color: #ffb; }
    li.contact {
      list-style-type: none;
      display:block;
      margin:0;
      padding:2px;
      height:32px;
    }
    li.contact div.image {
      float:left;
      width:32px;
      height:32px;
      margin-right:8px;
    }
    li.contact div.name {
      font-weight:bold;
      font-size:12px;
      line-height:1.2em;
    }
    li.contact div.email {
      font-size:10px;
      color:#888;
    }
    #list {
      margin:0;
      margin-top:10px;
      padding:0;
      list-style-type: none;
      width:250px;
    }
    #list li {
      margin:0;
      margin-bottom:4px;
      padding:5px;
      border:1px solid #888;
      cursor:move;
    }
  </style>

</head>

<body onLoad="$('kki').focus();">
<div id="form_edit">
<br><br>
<div align="center">
<font size="5">Perubahan Data Keluarga</font>
</div>
<br><br>
<form name="f">
<table bgcolor="" cellspacing="0" style="border-collapse: collapse" bordercolor="#E0DFE3" width="" leftmargin="100" align="center">
<tr>
<td>
<font size="4">No. Kode Keluarga Indonesia (KKI) :</font>
</td>
<td>
<input type="text" id="kki" name="kki">
</td>
<td>
<input type="button" id="btn_edt" name="btn_edt" value="OK" onClick="btn_e_d_a()" onMouseOver="this.style.cursor='hand'; this.style.cursor='pointer';">
</td>
</tr>
</table>
</form>
</div>
</body>
</html>