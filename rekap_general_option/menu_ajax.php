<?php
session_start();
if(!isset($_SESSION['un']))
   exit();
include "../koneksi/koneksi.inc.php";
include "../brt_main_cms_1.php";
unset($_SESSION['kki']);
unset($_SESSION['id_fam']);
unset($_SESSION['neigh']);
unset($_SESSION['sts_miskin']);
unset($_SESSION['sts_ks']);

?>

<html>
<head>
<title>Menu Tab</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="default1_data/news.css" type="text/css">
<link href="default1_data/forum.css" rel="stylesheet" type="text/css">
<link href="default1_data/styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="tooltipsajax/js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="tooltipsajax/js/ajax.js"></script>
<script type="text/javascript" src="tooltipsajax/js/ajax-tooltip.js"></script>	
<link rel="stylesheet" href="tooltipsajax/css/ajax-tooltip.css" media="screen" type="text/css">
<script src="../js/ajaxlibinduk.js" type="text/javascript"></script>
<script src="../js/prototype.js" type="text/javascript"></script>
<script src="../js/scriptaculous.js" type="text/javascript"></script>
<script src="../js/effects.js" type="text/javascript"></script>
<script src="../js/controls.js" type="text/javascript"></script>
<script src="../js/instobj.js" type="text/javascript"></script>
<script src="prop.js" type="text/javascript"></script>
<script src="kab_kota.js" type="text/javascript"></script>
<script src="kec.js" type="text/javascript"></script>
<script src="kel.js" type="text/javascript"></script>
<script src="rw.js" type="text/javascript"></script>
<script src="lib.js" type="text/javascript"></script>
<script src="lib_idw.js" type="text/javascript"></script>
<script src="lib_a_k.js" type="text/javascript"></script>
<script src="lib_ks.js" type="text/javascript"></script>
<script src="lib_kak.js" type="text/javascript"></script>
<script src="lib_kak_u.js" type="text/javascript"></script>
<script src="lib_u_d_k.js" type="text/javascript"></script>
<link rel="stylesheet" href="../dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="../dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<link href="../js/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
<script src="../js/SpryTabbedPanels.js" type="text/javascript"></script>




<style type="text/css">
    div.auto_complete {
      position:absolute;
      width:250px;
      background-color:white;
      border:1px solid #888;
      margin:0px;
      padding:0px;
    }
    ul.contacts  {
      list-style-type: none;
      margin:0px;
      padding:0px;
    }
    ul.contacts li.selected { background-color: #ffb; }
    li.contact {
      list-style-type: none;
      display:block;
      margin:0;
      padding:2px;
      height:32px;
    }
    li.contact div.image {
      float:left;
      width:32px;
      height:32px;
      margin-right:8px;
    }
    li.contact div.name {
      font-weight:bold;
      font-size:12px;
      line-height:1.2em;
    }
    li.contact div.email {
      font-size:10px;
      color:#888;
    }
    #list {
      margin:0;
      margin-top:10px;
      padding:0;
      list-style-type: none;
      width:250px;
    }
    #list li {
      margin:0;
      margin-bottom:4px;
      padding:5px;
      border:1px solid #888;
      cursor:move;
    }
  </style>



</head>

<body>
       <br>
        <div id="TabbedPanels1" class="TabbedPanels">
		<ul class="TabbedPanelsTabGroup">
   		    <button onClick="d_k_i()" style="background-color:#9999CC " id="btn_mn_1" onMouseOver="this.style.cursor='hand'; this.style.cursor='pointer'"><li class="TabbedPanelsTab" tabindex="0" ><font size="2">Input Data Keluarga</font></li></button>
			<button onClick="d_k_e()" id="btn_mn_2" onMouseOver="this.style.cursor='hand'; this.style.cursor='pointer'"><li class="TabbedPanelsTab" tabindex="0" ><font size="2">Edit Data Keluarga</font></li></button>
		    <button onClick="v_data()" id="btn_mn_3" onMouseOver="this.style.cursor='hand'; this.style.cursor='pointer'"><li class="TabbedPanelsTab" tabindex="0" ><font size="2">View Data Keluarga</font></li></button>
		</ul>
		<div class="TabbedPanelsContentGroup">
	    	<div class="TabbedPanelsContent" id="panel0">
             <?php include "form_data_keluarga_ajax.php"; ?>
            </div>
		    <div class="TabbedPanelsContent" id="panel1">

            </div>
			<div class="TabbedPanelsContent" id="panel2">

            </div>
	    </div>
		</div>
		<form>
		<input type="hidden" id="isi_h_tab" value="1">
		<input type="hidden" id="isi_h_kel_kak" value="1">
		</form>
</body>
</html>		
       
	<script type="text/javascript">
	    
		 var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
     
	</script>
	
	<?php if($_SESSION['id_area']==15)  { ?>
	<script type="text/javascript"> new Ajax.Autocompleter('kki','hint','auto_individu.php',{minChars:6, callback: eventCallBack }); </script>  
	<?php } ?>
