function u_d_k_baru() {
    for(var j=1;j<=25;j++)
    {
        if($('tr_f_agt'+j).style.display=='')
        {
				 
            $('kak'+j).style.backgroundColor="";
        }
    }
    var fm_atas=true;
    if($('almt').value=='')
    {
        alert("Field Alamat Tidak boleh Kosong");
        $('almt').style.backgroundColor="yellow";
	  
	  
        $('almt').focus();
        return false;
    }
    else if($('f_d_k_prop').value=='')
    {
        alert("Pilih Propinsi Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('f_d_k_kab_kota').value=='')
    {
        alert("Pilih Kabupaten/Kota Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('f_d_k_kec').value=='')
    {
        alert("Pilih Kecamatan Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('f_d_k_kel').value=='')
    {
        alert("Pilih Desa/Kelurahan Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('f_d_k_rw').value=='')
    {
        alert("Pilih Dusun/RW Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('f_d_k_rt').value=='')
    {
        alert("Pilih RT Terlebih Dahulu");
	  
	  
        return false;
    }
    else if($('kki').value=='')
    {
        alert("Isi Nomor Kode Keluarga Indonesia (KKI) Terlebih Dahulu");
        $('kki').style.backgroundColor="yellow";
        $('kki').focus();
	  
	  
        return false;
    }
    else if($('kki').value.length!=7)
    {
        alert("Jumlah Digit/Karakter Nomor Kode Keluarga Indonesia (KKI) Harus Sama Dengan Tujuh (7)");
        $('kki').style.backgroundColor="yellow";
        $('kki').focus();
	  
	  
        return false;
    }

    var ada_kbr=false;
    var arr_kbr=new Array();
    var nm_ksg=false;
    var tpt_lhr_ksg=false;
    var fm_atas=true;
		
		
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('nm'+i).value=='')
                {
					   
                    alert("Nama Anggota Keluarga Harus Diisi");
                    nm_ksg=true;
					   
					   
                    $('nm'+i).style.backgroundColor="yellow";
                    if($('mts'+i).selectedIndex==0)
                        $('nm'+i).focus();
                    return false;
					   
                }
            }
				
        }
        else
        {
            break;

        }
    }
    var arr_kk_kpl=new Array();
    var idx_kk_kpl=0;
		
    var kk_kosong=true;
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('hub'+i).selectedIndex==0 && $('mts'+i).selectedIndex==0 && $('kak'+i).value!='')
            {
                kk_kosong=false;
                break;
            }
        }
        else
            break;
    }
    if(kk_kosong)
    {
        alert("Hubungan Dengan KK 'KEPALA KELURAGA' Harus Ada Di Data Anggota Keluarga");
			  
			
        return false;
    }
		
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('hub'+i).selectedIndex==0 && $('mts'+i).selectedIndex==0)
                {
                    arr_kk_kpl[idx_kk_kpl]=i;
                    idx_kk_kpl++;
                }
            }
				
        }
        else
        {
            break;
					
        }
    }
		 
    if(arr_kk_kpl.length>1)
    {
        alert("Hubungan Dengan KK 'KEPALA KELURAGA' Tidak Boleh Lebih Dari Satu ");
        $('hub1').style.backgroundColor="yellow";
			
			
        for(var j=0;j<arr_kk_kpl.length;j++)
        {
            $('hub'+arr_kk_kpl[j]).style.backgroundColor="yellow";
        }
        return false;
    }

    for (var i = 1; i <= 25; i++) {
        if ($('tr_f_agt' + i).style.display == '') {
            if ($('kak' + i).value != '') {
                if ($('sts' + i).value != 1 ) {
                    if ($('usiakawin' + i).value == '' && ($('hub' + i).value == 2)) {
                        alert("Usia Pertama Kawin Anggota Keluarga Harus Diisi");
                        usiakawin_ksg = true;
                        $('usiakawin' + i).style.backgroundColor = "yellow";
                        return false;
                    }
                }
            }

        } else {
            break;
        }
    }
		
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('tpt_lhr'+i).value=='')
                {
                    alert("Tempat Lahir Anggota Keluarga Harus Diisi");
                    tpt_lhr_ksg=true;
					   
					   
                    $('tpt_lhr'+i).style.backgroundColor="yellow";
                    if($('mts'+i).selectedIndex==0)
                        $('tpt_lhr'+i).focus();
                    return false;
                }
            }
				
        }
        else
        {
            break;

        }
    }
		  
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('tlhr'+i).value=='')
                {
					   
                    alert("Tanggal Lahir Anggota Keluarga Harus Diisi AA");
                    nm_ksg=true;
					   
					   
                    if($('mts'+i).selectedIndex==0)
                        $('tlhr'+i).focus();
                    $('tlhr'+i).style.backgroundColor="yellow";
				   
                    return false;
					   
                }else{
					var tgl_sekarang = new Date();
					var dtStr =  $('tlhr'+i).value;
            		var pos2 = dtStr.indexOf('-');
            		var pos1 = dtStr.indexOf('-', pos2 + 1);
            		var strDay = dtStr.substring(0, pos1);
            		var strMonth = dtStr.substring(pos2 + 1, pos1);
            		var strYear = dtStr.substring(pos1 + 1);
					
					var month = parseInt(strMonth);
            		var day = parseInt(strDay);
            		var TahunLahir = parseInt(strYear);
					
					var tgl_lahir  = new Date(TahunLahir, month, day);
					var selisih  = (Date.parse(tgl_sekarang.toGMTString())-Date.parse(tgl_lahir.toGMTString()))/(1000*60*60*24*365);
					var usia = Math.floor(selisih);
					if(usia>=150){
						alert("Usia terlalu tua " + usia +",/n Silahkan ubah kembali tanggal lahirnya");
                    	if ($('mts' + i).selectedIndex == 0) $('tlhr' + i).focus();

                    	nm_ksg = true;

                    	$('tlhr' + i).style.backgroundColor = "yellow";

                    	return false;
					}else{
						
					}
					
				}
            }
				
        }
        else
        {
            break;

        }
    }
		
		
		
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('tlhr'+i).value!='')
                {
					   
                    if(prosesformattgl($('tlhr'+i).value)==false)
                    {
                        $('tlhr'+i).focus();
						    
							
                        $('tlhr'+i).style.backgroundColor="yellow";
                        return false;
                    }
					   
					   
					   
                }
            }
				
        }
        else
        {
            break;

        }
    }

				
    for(var i=1;i<=25;i++)
    {
        if($('tr_f_agt'+i).style.display=='')
        {
            if($('kak'+i).value!='')
            {
                if($('yd1'+i).checked==false && $('yd2'+i).checked==false && $('div_pos_yd'+i).style.display=='')
                {
                    alert("Ikut Program Posyandu Harus Dipilih");
					   
					   
                    $('yd1'+i).style.backgroundColor="yellow";
                    $('yd2'+i).style.backgroundColor="yellow";
                    return false;
					   
                }
            }
				
        }
        else
        {
            break;

        }
    }
		
		
    for(var i=1;i<25;i++)
    {
        arr_kbr[1]=i;
        var k=2;
        if($('tr_f_agt'+i).style.display=='')
        {
            for(var j=i+1;j<=25;j++)
            {
                if($('tr_f_agt'+j).style.display=='')
                {
                    if($('kak'+i).value!='')
                    {
                        if($('kak'+i).value==$('kak'+j).value)
                        {
                            ada_kbr=true;
                            arr_kbr[k]=j;
                            k++;
						   
                        }
                    }
                }
            }
        }
        if(ada_kbr)
            break;
    }
		
    if(ada_kbr)
    {
        alert("Terdapat Nomor Kode Anggota Keluarga KAK Yang Sama");

        for(var j=1;j<arr_kbr.length;j++)
        {
            $('kak'+arr_kbr[j]).style.backgroundColor="yellow";
        }
        return false;
    }
  
    var fm_tengah=true;
    if($('btm1').checked==false && $('btm2').checked==false)
    {
        alert("Bantuan Modal Harus Dipilih");
	  
	  
        $('btm1').style.backgroundColor="yellow";
        $('btm2').style.backgroundColor="yellow";
        return false;
    }
    else if($('pus1').checked==false && $('pus2').checked==false)
    {
        alert("PUS Harus Dipilih");
	  
	  
        $('pus1').style.backgroundColor="yellow";
        $('pus2').style.backgroundColor="yellow";
        return false;
    }
    else if($('pus1').checked==true)
    {
        var Istri=0;
        var Usia = '';
        for(var i=1;i<=25;i++)
        {
            if($('tr_f_agt'+i).style.display=='')
            {
                if($('hub'+i).selectedIndex==1 && $('mts'+i).selectedIndex==0 && $('kak'+i).value!='')
                {
                    Istri=i;
                    break;
                }
            }
            else
                break;
        }

        if(Istri !== 0)
        {
            var tgl_skrg = new Date();
            var bln = tgl_skrg.getMonth() + 1;
            var tgl = tgl_skrg.getDate();
            var thn = tgl_skrg.getFullYear();

            var dtStr =  $('tlhr'+Istri).value;
            var pos2 = dtStr.indexOf('-')
            var pos1 = dtStr.indexOf('-', pos2 + 1)
            var strDay = dtStr.substring(0, pos1)
            var strMonth = dtStr.substring(pos2 + 1, pos1)
            var strYear = dtStr.substring(pos1 + 1)

            var month = parseInt(strMonth)
            var day = parseInt(strDay)
            var TahunLahir = parseInt(strYear)

            var umurIstri = thn - TahunLahir;
             if (bln == month) {
                umurIstri = umurIstri - 1
            }
            if (bln == month && tgl < day) {
                umurIstri = umurIstri - 1
            }

            if (umurIstri > 49){
                alert('Usia istri = '+umurIstri+' tahun, bukan PUS');
                return false;
            }
            else if($('kb').checked==false && $('kb_1').checked==false)
            {
                alert("Pilih Option Peserta KB/Bukan Peserta KB Terlebih Dahulu");
	  
	  
                $('kb').style.backgroundColor="yellow";
                $('kb_1').style.backgroundColor="yellow";
                return false;
            }
            else if($('kb').checked==true)
            {
                var sli=document.getElementById('alt_kb').selectedIndex;
		
                if(document.getElementById('alt_kb').options[sli].text.toLowerCase()=='implant')
                {
                    var cek_impl=false;
                    for(i=0;i<$('h_impl').value;i++)
                    {
                        if($('ipl'+i).checked==true)
                        {
                            var cek_impl=true;
                            var ipl=$('ipl'+i).value;
                            break;
                        }
                    }
                }
                else
                    var cek_impl=true;
		
                var cek=false
                for(i=0;i<$('h_i').value;i++)
                {
                    if($('id_src'+i).checked==true)
                    {
                        cek=true;
                        var src=$('id_src'+i).value;
                        break;
                    }
                }
	
                if(!cek)
                {
                    alert("Pilih Tempat Pelayanan KB (Pemerintah/Swasta) Terlebih Dahulu");
		
		
                    for(i=0;i<$('h_i').value;i++)
                    {
                        $('id_src'+i).style.backgroundColor="yellow";
		   
                    }
	   
                    return false;
                }
                else if(!cek_impl)
                {
                    alert("Pilih Implan Terlebih Dahulu");
	   
	   
                    for(i=0;i<$('h_impl').value;i++)
                    {
                        $('ipl'+i).style.backgroundColor="yellow";
		   
                    }
                    return false;
                }
                else if($('tkont').value=='')
                {
                    alert("Isi Tanggal Kapan Menjadi Peserta KB Terlebih Dahulu (Metode Kontrasepsi Yang Terakhir Dipakai)");
                    $('tkont').focus();
	   
	   
                    $('tkont').style.backgroundColor="yellow";
                    return false;
                }
                else if($('tkont').value!='')
                {
					 
                    if(prosesformattgl($('tkont').value)==false)
                    {
                        $('tkont').focus();
                        $('tkont').style.backgroundColor="yellow";
                        return false;
                    }
                }
            } else if ($('kb_1').checked == true) {
                var sli_bkb = document.getElementById('s_bkb').selectedIndex;
                if (sli_bkb == 0 && $('usiahamil').value == '') {
                    alert("Usia kehamilan (minggu ke) harus diisi ");
                    $('usiahamil').focus();
                    $('usiahamil').style.backgroundColor = "yellow";
                    return false;
                }
            }



        }else {
            alert('Pilihan PUS harus Suami istri');
            return false;
        }

    }

    else if($('pus2').checked==true)
    {
        var Istri=0;
        var Suami=0;
        var Usia = '';
        for(var i=1;i<=25;i++)
        {
            if($('tr_f_agt'+i).style.display=='')
            {
                if($('hub'+i).selectedIndex==0 && $('mts'+i).selectedIndex==0 && $('kak'+i).value!='')
                {
                    Suami=i;
                }
                if($('hub'+i).selectedIndex==1 && $('mts'+i).selectedIndex==0 && $('kak'+i).value!='')
                {
                    Istri=i;
                }
            }
            else
                break;
        }

        if(Istri !== 0 && Suami!==0)
        {
            var tgl_skrg = new Date();
            var bln = tgl_skrg.getMonth() + 1;
            var tgl = tgl_skrg.getDate();
            var thn = tgl_skrg.getFullYear();

            var dtStr =  $('tlhr'+Istri).value;
            var pos2 = dtStr.indexOf('-')
            var pos1 = dtStr.indexOf('-', pos2 + 1)
            var strDay = dtStr.substring(0, pos1)
            var strMonth = dtStr.substring(pos2 + 1, pos1)
            var strYear = dtStr.substring(pos1 + 1)

            var month = parseInt(strMonth)
            var day = parseInt(strDay)
            var TahunLahir = parseInt(strYear)

            var umurIstri = thn - TahunLahir;
             if (bln == month) {
                umurIstri = umurIstri - 1
            }
            if (bln == month && tgl < day) {
                umurIstri = umurIstri - 1
            }


            if (umurIstri > 14 && umurIstri < 50){
                alert('Pasangan suami istri  dengan usia istri = '+umurIstri+' tahun, Pilih PUS');
                return false;
            }
        }
    }
	
    ajax_hideTooltip();
    var k_kss=false;
    var idx_ks="";
    var strp_isi="";
	  
    for(var i=1;i<$('jml_ks').value;i++)
    {
        idx_ks=$('t_ks'+i).value;
        strp_isi=strp_slashes($('f_ks'+idx_ks).value);
        if((strp_isi.toLowerCase()!="v" && strp_isi.toLowerCase()!="x" && strp_isi.toLowerCase()!="x*" && strp_isi.toLowerCase()!="-") && strp_isi!="")
        {
            k_kss=true;
            var idx_n=idx_ks;
            break;
			  
        }
    }
    var k_ks=false;
    var idx_ks="";
    for(var i=1;i<$('jml_ks').value;i++)
    {
        idx_ks=$('t_ks'+i).value;
        strp_isi=strp_slashes($('f_ks'+idx_ks).value);
        if(strp_isi=='')
        {
            k_ks=true;
            var idx_k=idx_ks;
            break;
        }
    }
    if(k_kss)
    {
        alert("Data Indikator Dan Status Tahapan Keluarga Tidak Boleh Berisi Selain X* X V -") ;
		
		
        $('f_ks'+idx_n).select();
        return false;
    }
    else if(k_ks)
    {
        alert("Data Indikator Dan Status Tahapan Keluarga Tidak Lengkap");
		
		
        $('f_ks'+idx_k).focus();
        return false;
    }



    var kel = encodeURIComponent(document.getElementById('f_d_k_kel').value);
    var kki = kel+document.getElementById('kki').value;
	    
    if($('hdn_kki').value!=kki)
    {
        var XMLHttpRequestObject=ajaxpustaka();
        var ada_kki=false;
        if(XMLHttpRequestObject)
        {
			 
            var kki_skrg=$('hdn_kki').value;
            var urlp="kki="+kki+"&kki_skrg="+kki_skrg+"&r="+new Date().getTime();
            cemangtampilmodal("<br><IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Silahkan Tunggu, Sedang Proses Validasi Nomor KKI "+kki+" ...");
            XMLHttpRequestObject.open("POST", "cek_kki_u.php");
             
             
            XMLHttpRequestObject.onreadystatechange = function()
            {
      
			  
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200)
                {
                    if(XMLHttpRequestObject.responseText==1)
                    {
                        messageObj.close();
                        alert("NOMOR KKI " +kki+ " SUDAH ADA DIDATABASE SILAHKAN DIULANGI !!!");
                        $('kki').select();
                        $('w_d_k_1').innerHTML='';
                        ada_kki=true;
					 
                    }
                    else
                    {
                        $('w_d_k_1').innerHTML='';
                        kak_cek_u(1);
                    }
				 
                }
            }
            XMLHttpRequestObject.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
            XMLHttpRequestObject.send(urlp);
        }
    }
    else
    {
        cemangtampilmodal("<br><IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">Silahkan Tunggu, Sedang Proses Validasi Nomor KAK ...");
        kak_cek_u(1);
    }
		 
		
		
		
   
}