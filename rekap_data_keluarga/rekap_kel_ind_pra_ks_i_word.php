<?php
header("Content-Type: application/vnd.ms-word"); 
header("Content-Disposition: attachment; filename=nama_file.doc");
header("Pragma: no-cache");
header("Expires: 0");
include "../koneksi/koneksi.inc.php"; 

include "../config/params.php";

$tamat_kul = TAMATKULIAH();
$tamat_sma = TAMATSMA();
$tamat_smp = TAMATSLTP();
$tamat_sd = TAMATSD();
$tdk_tamat_sd = TIDAKTAMATSD();
$tidakbekerja = TIDAKBEKERJA();

if($_POST['ort']=='portrait')
	{
	    $sz=explode('-',$_POST['type']);
		$sz=$sz[0];
	}
	else
	{
	    $sz=explode('-',$_POST['type']);
		$sz=$sz[1];
	}
	
	$ort=$_POST['ort'];
	$tm=$_POST['tm']."cm";
	$lm=$_POST['lm']."cm";
	$rm=$_POST['rm']."cm";
	$bm=$_POST['bm']."cm";

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="ProgId" content="Word.Document">
<style>
@page format
{size: <?php echo $sz ?>;
mso-page-orientation: <?php echo $ort ?>;
margin: <?php echo $tm ?> <?php echo $rm ?> <?php echo $bm ?> <?php echo $lm ?>;
mso-paper-source: 0;}
div.format
{page: format;}
</style>
</head>
<body>
<div class="format">
<div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" size="3"><b>REKAPITULASI HASIL PEMUTAKHIRAN DATA KELUARGA PRA SEJAHTERA TINGKAT DESA/KELURAHAN</b></font></div>
<br><br><br><br>
<table width="100%">
<?php
$qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail_idk='$_POST[kel]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$jd=mysql_num_rows($qr_isi);
?>
<tr>
<td width="14%">
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>JUMLAH DUSUN/RW YANG ADA</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $jd;?></b></font></div>
</td>
</tr>
<?php
$qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$_POST[kel]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc_isi=mysql_fetch_array($qr_isi);
?>
<tr>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>DESA/KELURAHAN</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[no_unit_detail];?></b></font></div>
</td>
<td width="60%">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>NO. KODE DESA/KELURAHAN</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[id_unit_detail];?></b></font></div>
</td>
</tr>
<?php
$qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$rc_isi[id_unit_detail_idk]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc_isi=mysql_fetch_array($qr_isi);
?>
<tr>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KECAMATAN</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[no_unit_detail];?></b></font></div>
</td>
<td width="60%">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>NO. KODE KECAMATAN</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[id_unit_detail];?></b></font></div>
</td>
</tr>
<?php
$qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$rc_isi[id_unit_detail_idk]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc_isi=mysql_fetch_array($qr_isi);
?>
<tr>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>KABUPATEN/KOTA</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[no_unit_detail];?></b></font></div>
</td>
<td width="60%">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>NO. KODE KABUPATEN/KOTA</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[id_unit_detail];?></b></font></div>
</td>
</tr>
<?php
$qr_isi=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail='$rc_isi[id_unit_detail_idk]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc_isi=mysql_fetch_array($qr_isi);
?>
<tr>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>PROVINSI</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[no_unit_detail];?></b></font></div>
</td>
<td width="50%">
<div align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>NO. KODE PROVINSI</b></font></div>
</td>
<td>
<div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><b>: <?php echo $rc_isi[id_unit_detail];?></b></font></div>
</td>
</tr>
</table>
<br>
<table  border="1" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="2%" rowspan="3"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>NO URUT</B></font></td> 
<td width="2%" rowspan="3" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>RT</B></font></td>
<td width="2%" rowspan="3" colspan="2" align="center" bgcolor="#66CCCC" width="30%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>STATUS KELUARGA PRA SEJAHTERA</B></font></td>
<td width="2%" rowspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KEPALA KELUARGA</B></font></td>
<td width="2%" colspan="24" align="center" bgcolor="#66CCCC"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B>KELUARGA</B></font></td>

</tr>
<tr>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KEPALA KELUARGA MENURUT JENIS KELAMIN</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KEPALA KELUARGA MENURUT STATUS PEKERJAAN</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KEPALA KELUARGA MENURUT STATUS PERKAWINAN</B></font></td>
<td width="2%" colspan="4" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KEPALA KELUARGA MENURUT STATUS PENDIDIKAN</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KELUARGA MENDAPATKAN KREDIT MIKRO/ BANTUAN MODAL</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KELUARGA IKUT UPPKS</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KELUARGA PUNYA BALITA IKUT BKB</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KELUARGA PUNYA REMAJA IKUT BKR</B></font></td>
<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH KELUARGA PUNYA LANSIA IKUT BKL</B></font></td>
<td width="2%" colspan="3" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH JIWA DALAM KELUARGA</B></font></td>
<td width="2%" rowspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH WANITA USIA SUBUR(WUS) (15-49 TAHUN)</B></font></td>
</tr>
<tr>

<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YANG DI DATA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>LAKI-LAKI</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>PEREMPUAN</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>BEKERJA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK BEKERJA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>KAWIN</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>DUDA/ JANDA/ BELUM KAWIN</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK TAMAT SD</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TAMAT SD - SLTP</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TAMAT SLTA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TAMAT AK/PT</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>YA</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>TIDAK</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>LAKI-LAKI</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>PEREMPUAN</B></font></td>
<td width="2%"  align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH</B></font></td>
</tr>
<tr>
<?php
     for($i=1;$i<=28;$i++)
     {
     	if($i==3){
?>
       		<td width="2%" colspan="2" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo $i;?></B></td>
<?php 
     	}else{
?>
       		<td width="2%" align="center" bgcolor="#66CCCC"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo $i;?></B></td>
<?php 
		}
     }
?>
</tr>

<?php 
 $no=1;  $d=date('j'); $m=date('n'); $y=date('Y'); 
 if($d<10)
   $d="0".$d;
 if($m<10)
   $m="0".$m;  

 $u_skrg=$y.$m.$d;
   
 $y_lb=$y-15; $y_es=$y-50;
 $u_lb=$y_lb.$m.$d;	  
 $u_es=$y_es.$m.$d;
 
 $tgl_lb=$y_lb."-".$m."-".$d;
 $tgl_es=$y_es."-".$m."-".$d;
 
 $y_s=$y-1;
 $u_s=$y_s.$m.$d;	
 
 $y_l=$y-5;
 $u_l=$y_l.$m.$d;	  
 
 $qr_rw=mysql_query("SELECT * FROM dbo_unit_detail WHERE id_unit_detail_idk='$_POST[kel]' ORDER BY no_unit_detail", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
 while($rc_rw=mysql_fetch_array($qr_rw))
 {
    $qr_kk=mysql_query("SELECT SUM(idv.Kd_fammbrtyp=1) AS j_rt,".
					                "SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_gen=1) AS j_lk_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_gen=2) AS j_pr_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.kd_emp!=$tidakbekerja) AS j_krj_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.kd_emp=$tidakbekerja) AS j_tkrj_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_martl=2) AS j_kwn_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_martl!=2) AS j_tkwn_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tdk_tamat_sd') AS j_ttmt_sd_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND (idv.Kd_edu='$tamat_sd' OR idv.Kd_edu='$tamat_smp')) AS j_tmt_sd_sltp_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tamat_sma') AS j_tmt_slta_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tamat_kul') AS j_tmt_ak_pt_kk,".
									"SUM(fam.Bantuan_modal='1' AND idv.Kd_fammbrtyp=1) AS j_btm_kk,".
									"SUM(fam.Bantuan_modal='0' AND idv.Kd_fammbrtyp=1) AS j_tbtm_kk,".
									"SUM(fam.uppks='1' AND idv.Kd_fammbrtyp=1) AS j_uppks_kk,".
									"SUM((fam.uppks='0' OR fam.uppks IS NULL OR fam.uppks='') AND idv.Kd_fammbrtyp=1) AS j_tuppks_kk,".
									"SUM(idv.bkb='1' AND idv.Kd_fammbrtyp=1) AS j_bkb_kk,".
									"SUM((idv.bkb='2' OR idv.bkb IS NULL OR idv.bkb='') AND idv.Kd_fammbrtyp=1) AS j_tbkb_kk,".
									"SUM(idv.bkr='1' AND idv.Kd_fammbrtyp=1) AS j_bkr_kk,".
									"SUM((idv.bkr='2' OR idv.bkr IS NULL OR idv.bkr='') AND idv.Kd_fammbrtyp=1) AS j_tbkr_kk,".
									"SUM(idv.bkl='1' AND idv.Kd_fammbrtyp=1) AS j_bkl_kk,".
									"SUM((idv.bkl='2' OR idv.bkl IS NULL OR idv.bkl='') AND idv.Kd_fammbrtyp=1) AS j_tbkl_kk,".
									"SUM(idv.Kd_gen=1) AS j_lk_jiwa,".
									"SUM(idv.Kd_gen=2) AS j_pr_jiwa,".
									"SUM(idv.Kd_gen=2 AND (idv.Tgl_lahir <= '$tgl_lb' AND idv.Tgl_lahir >= '$tgl_es')) AS j_wus ".
							 "FROM dbo_family fam, dbo_individu idv  ".
							 "WHERE fam.Kd_fam=idv.Kd_fam AND fam.Kd_subvill='$rc_rw[id_unit_detail]' AND (fam.Kd_prosplvl=1) AND idv.Kd_mutasi IS NULL", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
                             $rc_kk=mysql_fetch_array($qr_kk);
	
    $qr_kk_alek=mysql_query("SELECT SUM(idv.Kd_fammbrtyp=1) AS j_rt,".
					                "SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_gen=1) AS j_lk_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_gen=2) AS j_pr_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.kd_emp!=$tidakbekerja) AS j_krj_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.kd_emp=$tidakbekerja) AS j_tkrj_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_martl=2) AS j_kwn_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_martl!=2) AS j_tkwn_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tdk_tamat_sd') AS j_ttmt_sd_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND (idv.Kd_edu='$tamat_sd' OR idv.Kd_edu='$tamat_smp')) AS j_tmt_sd_sltp_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tamat_sma') AS j_tmt_slta_kk,".
									"SUM(idv.Kd_fammbrtyp=1 AND idv.Kd_edu='$tamat_kul') AS j_tmt_ak_pt_kk,".
									"SUM(fam.Bantuan_modal='1' AND idv.Kd_fammbrtyp=1) AS j_btm_kk,".
									"SUM(fam.Bantuan_modal='0' AND idv.Kd_fammbrtyp=1) AS j_tbtm_kk,".
									"SUM(fam.uppks='1' AND idv.Kd_fammbrtyp=1) AS j_uppks_kk,".
									"SUM((fam.uppks='0' OR fam.uppks IS NULL OR fam.uppks='') AND idv.Kd_fammbrtyp=1) AS j_tuppks_kk,".
									"SUM(idv.bkb='1' AND idv.Kd_fammbrtyp=1) AS j_bkb_kk,".
									"SUM((idv.bkb='2' OR idv.bkb IS NULL OR idv.bkb='') AND idv.Kd_fammbrtyp=1) AS j_tbkb_kk,".
									"SUM(idv.bkr='1' AND idv.Kd_fammbrtyp=1) AS j_bkr_kk,".
									"SUM((idv.bkr='2' OR idv.bkr IS NULL OR idv.bkr='') AND idv.Kd_fammbrtyp=1) AS j_tbkr_kk,".
									"SUM(idv.bkl='1' AND idv.Kd_fammbrtyp=1) AS j_bkl_kk,".
									"SUM((idv.bkl='2' OR idv.bkl IS NULL OR idv.bkl='') AND idv.Kd_fammbrtyp=1) AS j_tbkl_kk,".
									"SUM(idv.Kd_gen=1) AS j_lk_jiwa,".
									"SUM(idv.Kd_gen=2) AS j_pr_jiwa,".
									"SUM(idv.Kd_gen=2 AND (idv.Tgl_lahir <= '$tgl_lb' AND idv.Tgl_lahir >= '$tgl_es')) AS j_wus ".
							 "FROM dbo_family fam, dbo_individu idv  ".
							 "WHERE fam.Kd_fam=idv.Kd_fam AND fam.Kd_subvill='$rc_rw[id_unit_detail]' AND (fam.Kd_prosplvl=6) AND idv.Kd_mutasi IS NULL", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
                             $rc_kk_alek=mysql_fetch_array($qr_kk_alek);                          
	?>
	<tr>
 	 <td width="2%" rowspan="2" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo $no; ?></B></td>
 	 <td width="2%" rowspan="2" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo $rc_rw[no_unit_detail]; ?></B></td>	 
 	 <td width="2%" rowspan="2" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>PRA SEJAHTERA</B></td>	 
	 <td width="2%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>Non Alek</B></td>
     <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_rt'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_lk_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_pr_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_krj_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tkrj_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_kwn_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tkwn_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_ttmt_sd_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tmt_sd_sltp_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tmt_slta_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tmt_ak_pt_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_btm_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tbtm_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_uppks_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tuppks_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_bkb_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tbkb_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_bkr_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tbkr_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_bkl_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_tbkl_kk'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_lk_jiwa'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_pr_jiwa'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_lk_jiwa']+$rc_kk['j_pr_jiwa'],0,',','.');?></B></font></td>
	 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk['j_wus'],0,',','.');?></B></font></td>
    </tr>
    <tr>
    	<td width="2%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B>Alek</B></td>
    	<td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_rt'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_lk_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_pr_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_krj_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tkrj_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_kwn_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tkwn_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_ttmt_sd_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tmt_sd_sltp_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tmt_slta_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tmt_ak_pt_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_btm_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tbtm_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_uppks_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tuppks_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_bkb_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tbkb_kk'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_bkr_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tbkr_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_bkl_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_tbkl_kk'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_lk_jiwa'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_pr_jiwa'],0,',','.');?></B></font></td>
		 <td width="1%"  align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_lk_jiwa']+$rc_kk_alek['j_pr_jiwa'],0,',','.');?></B></font></td>
		 <td width="1%" align="center" ><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($rc_kk_alek['j_wus'],0,',','.');?></B></font></td>
    </tr>

	<?php  
	$no++;
	$tot_rt=$tot_rt+$rc_kk['j_rt']+$rc_kk_alek['j_rt'] ; $tot_lk_kk=$tot_lk_kk+$rc_kk['j_lk_kk']+$rc_kk_alek['j_lk_kk'] ; $tot_pr_kk=$tot_pr_kk+$rc_kk['j_pr_kk']+$rc_kk_alek['j_pr_kk'] ; $tot_krj_kk=$tot_krj_kk+$rc_kk['j_krj_kk']+$rc_kk_alek['j_krj_kk'] ; $tot_tkrj_kk=$tot_tkrj_kk+$rc_kk['j_tkrj_kk']+$rc_kk_alek['j_tkrj_kk'];
 $tot_kwn_kk=$tot_kwn_kk+$rc_kk['j_kwn_kk']+$rc_kk_alek['j_kwn_kk']; $tot_tkwn_kk=$tot_tkwn_kk+$rc_kk['j_tkwn_kk']+$rc_kk_alek['j_tkwn_kk']; $tot_ttmt_sd_kk=$tot_ttmt_sd_kk+$rc_kk['j_ttmt_sd_kk']+$rc_kk_alek['j_ttmt_sd_kk']; $tot_tmt_sd_sltp_kk=$tot_tmt_sd_sltp_kk+$rc_kk['j_tmt_sd_sltp_kk']+$rc_kk_alek['j_tmt_sd_sltp_kk'];
 $tot_tmt_slta_kk=$tot_tmt_slta_kk+$rc_kk['j_tmt_slta_kk']+$rc_kk_alek['j_tmt_slta_kk']; $tot_tmt_ak_pt_kk=$tot_tmt_ak_pt_kk+$rc_kk['j_tmt_ak_pt_kk']+$rc_kk_alek['j_tmt_ak_pt_kk']; $tot_btm_kk=$tot_btm_kk+$rc_kk['j_btm_kk']+$rc_kk_alek['j_btm_kk']; $tot_tbtm_kk=$tot_tbtm_kk+$rc_kk['j_tbtm_kk']+$rc_kk_alek['j_tbtm_kk'];
 $tot_lk_jiwa=$tot_lk_jiwa+$rc_kk['j_lk_jiwa']+$rc_kk_alek['j_lk_jiwa']; $tot_pr_jiwa=$tot_pr_jiwa+$rc_kk['j_pr_jiwa']+$rc_kk_alek['j_pr_jiwa']; $tot_lk_pr_jiwa=$tot_lk_pr_jiwa+$rc_kk['j_lk_jiwa']+$rc_kk_alek['j_lk_jiwa']+$rc_kk['j_pr_jiwa']+$rc_kk_alek['j_pr_jiwa']; $tot_wus=$tot_wus+$rc_kk['j_wus']+$rc_kk_alek['j_wus'];
 $tot_uppks_kk=$tot_uppks_kk+$rc_kk['j_uppks_kk']+$rc_kk_alek['j_uppks_kk']; $tot_tuppks_kk=$tot_tuppks_kk+$rc_kk['j_tuppks_kk']+$rc_kk_alek['j_tuppks_kk'];
 $tot_bkb_kk=$tot_bkb_kk+$rc_kk['j_bkb_kk']+$rc_kk_alek['j_bkb_kk']; $tot_tbkb_kk=$tot_tbkb_kk+$rc_kk['j_tbkb_kk']+$rc_kk_alek['j_tbkb_kk'];
 $tot_bkr_kk=$tot_bkr_kk+$rc_kk['j_bkr_kk']+$rc_kk_alek['j_bkr_kk']; $tot_tbkr_kk=$tot_tbkr_kk+$rc_kk['j_tbkr_kk']+$rc_kk_alek['j_tbkr_kk'];
 $tot_bkl_kk=$tot_bkl_kk+$rc_kk['j_bkl_kk']+$rc_kk_alek['j_bkl_kk']; $tot_tbkl_kk=$tot_tbkl_kk+$rc_kk['j_tbkl_kk']+$rc_kk_alek['j_tbkl_kk'];
 }	
 
 ?>
 <tr>
 	 <td width="2%" align="center" bgcolor="#999999" colspan="2" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B>JUMLAH</B></td>
     <td width="2%" colspan="2" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B>PRA SEJAHTERA</B></td>	 	 	 
     <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_rt,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_lk_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_pr_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_krj_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tkrj_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_kwn_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tkwn_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_ttmt_sd_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tmt_sd_sltp_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tmt_slta_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tmt_ak_pt_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_btm_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tbtm_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_uppks_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tuppks_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_bkb_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tbkb_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_bkr_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tbkr_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_bkl_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_tbkl_kk,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_lk_jiwa,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_pr_jiwa,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_lk_pr_jiwa,0,',','.');?></B></font></td>
	 <td width="1%" align="center" bgcolor="#999999" ><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><B><?php echo number_format($tot_wus,0,',','.');?></B></font></td>
 </tr>
 </table>
</div>
</body>
</html>
 
 