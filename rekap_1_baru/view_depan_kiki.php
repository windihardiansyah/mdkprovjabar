<?php
$pdf->Ln(80); 
$pdf->SetFont('Arial','',5);
$pdf->Cell(120,2,'',0,0,'L',0);
$pdf->Cell(20,2,'TANGGAL DICETAK ',0,0,'L',0); $pdf->Cell(15,2,': '.date("d-m-Y"),0,0,'L',0);
$pdf->Ln();
$pdf->Cell(120,2,'',0,0,'L',0);
$pdf->Cell(20,2,'WAKTU DICETAK ',0,0,'L',0); $pdf->Cell(15,2,': '.date("H:i:s"),0,0,'L',0);
$pdf->Ln(3.5);
$pdf->SetFont('Arial','',7);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(102,5,'DAFTAR KELUARGA DAN ANGGOTA KELUARGA','LTR',0,'C',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$thn=date('Y');
$pdf->Cell(102,5,'TAHUN : '.$thn,'LBR',0,'C',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'RT',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[rt],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'RW',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[rw],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'Desa/Kelurahan',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[kel],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'Kecamatan',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[kec],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'Kabupaten/Kota',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[kota],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(26,5,'Propinsi',0,0,'L',0);$pdf->Cell(71,5,': '.$rc_isi[prop],'R',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(51,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','LB',0,'L',0); $pdf->Cell(97,5,'Kode Pos','RB',0,'L',0);
$pdf->Cell(51,5,'',0,0,'L',0);


?>