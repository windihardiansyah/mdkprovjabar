
function hapusspasichars(s, bag){
	var i;
    var returnString = "";
    
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function rst_s_t()
{
	if($('id_0').disabled==true)
	{
	    $('id_td_0').innerHTML="";
        $('id_0').disabled=false;
	}
	for(var i=1; i<$('id_hdn_c').value; i++)
	{
	  if($('id_'+i).disabled==true)
	  {
		$('id_td_'+i).innerHTML="";
	    $('id_'+i).disabled=false;
	  }
    }
}

function rst_t_khs(i)
{
	$('id_td_khs'+i).innerHTML="";
	$('id_khs_t'+i).value=0
}
function rst_t_r(i)
{
	$('id_td_r'+i).innerHTML="";
	$('id_r_t'+i).value=0
}


function rst_t(i)
{
	$('id_td_'+i).innerHTML="";
	$('id_'+i).disabled=false;
}

function cek_semua()
{
	for(var i=1; i<$('id_hdn_c').value; i++)
	{
		if($('id_'+i).checked==false && $('id_'+i).disabled!=true)
	       $('id_'+i).checked=true;
    }
}

function uncek_semua()
{
	var ada=false; var ksg=false;
	for(var i=1; i<$('id_hdn_c').value; i++)
	{
		if($('id_'+i).checked==true && $('id_'+i).disabled!=true)
		{
	       $('id_'+i).checked=false;
		   ada=true;
		}
		if($('id_'+i).checked==true)
		{
		   ksg=true;
		}
    }
	if(!ada && ksg)
	   alert('TABEL SUDAH DISABLED, PROSES UNCEK TIDAK DAPAT DIPROSES !!!');
}

function back_up_r(k)
{
				       var ada=false;

					   for(var i=k; i<parseInt($('id_hdn_c_r').value); i++)
				       {
      							     
							  if($('id_r_t'+i).value==0)
							  {
								 ada=true;
								 break;
							  }
					   }
					   if(!ada)
					   {
						       alert("SEMUA TABEL SUDAH DIREPAIR !!!");
							   $('bp_r').disabled=false; 
							   return false;
					   }
				   
					   $('idx_c_r').value=i;
					   $('id_td_r'+i).innerHTML="&nbsp;<IMG src='spinner.gif'>&nbsp;&nbsp;Sedang Proses ...";
					   var id_d=$('id_d').value; 
					   var ket_d=$('ket_d').value;
					   var u=$('id_r'+i).value;

					   
					   var n=encodeURIComponent($('nm_db').value);
					   var urlb="ex_sql_d_r.php?id_d="+id_d+"&ket_d="+ket_d+"&u="+u+"&n="+n+"&r="+new Date().getTime();
                       new Ajax.Request(
                       urlb,
                       {
                               method:"get",
			                   onSuccess:rslt_u_r,
							   onFailure:rslt_u_r 

                       }
                     );
}

function rslt_u_r(r)
{

	  var k=parseInt($('idx_c_r').value);
	  $('id_r_t'+k).value=1;
	  if(r.responseText==1)
	  {
		  document.getElementById("id_td_r"+k).innerHTML='<font color="red">GAGAL</font> Merepair Tabel: '+document.getElementById("id_r"+k).value+' | <a href=javascript:void(0) onclick="rst_t_r('+k+')">reset</a>';
	  }
      else if(r.responseText!="")
	  {
		 document.getElementById("id_td_r"+k).innerHTML='GAGAL, <font color="red">'+r.responseText+'</font> | <a href=javascript:void(0) onclick="rst_t_r('+k+')">reset</a>';

      }
	  else
	  {
	  document.getElementById("id_td_r"+k).innerHTML='<font color="blue">OK</font>, Nama File : '+document.getElementById("id_r"+k).value+'.sql | <a href=javascript:void(0) onclick="rst_t_r('+k+')">reset</a>';

	  }
      
	  k=parseInt($('idx_c_r').value)+1;
      var ada=false;
	  if(k < parseInt($('id_hdn_c_r').value))
	  {
	           for(var i=k; i<parseInt($('id_hdn_c_r').value); i++)
			   {
				  if($('id_r_t'+i).value==0)
				  {
 					   ada=true;
					   $('idx_c_r').value=i;
					   break;
			      }
			   }
			   if(ada)
		          back_up_r(i);
			   else	 
			     $('bp_r').disabled=false; 
      }    
	  else	 
	     $('bp_r').disabled=false; 
}


function back_up_s()
{
	x=/[a-z]/i; x1=/[0-9]/i;
	if($('ins_s').value=='')
	{
		   alert('ISI NAMA FILE TERLEBIH DAHULU !!!');
		   $('ins_s').focus();
		   $('bp_s').disabled=false; 
		   return false;
	}
	
	for(var i=0; i<strp_slashes($('ins_s').value).length; i++)
	 {
	    if(strp_slashes($('ins_s').value).charAt(i)==" ")
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI SPASI !!!");
			$('bp_s').disabled=false; 
			$('ins_s').focus();
			return false;
		}
	 }
	
	for(var i=0; i<strp_slashes($('ins_s').value).length; i++)
	{
		if(strp_slashes($('ins_s').value).charAt(i).search(x)==-1 && strp_slashes($('ins_s').value).charAt(i).search(x1)==-1 && strp_slashes($('ins_s').value).charAt(i)!='_')
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI KARAKTER SELAIN ANGKA (0-9), ABJAD (a-z,A-Z) DAN _ ");
			$('bp_s').disabled=false; 
			$('ins_s').focus();
			return false;
		}
    }
	
	if($('id_0').checked==false)
    {
		   alert('PILIH DATABASE YANG AKAN DIBACKUP !!!');
		   $('bp_s').disabled=false; 
		   return false;
    }
	else if($('id_0').disabled!=true)
	{
		               $('id_td_0').innerHTML="&nbsp;<IMG src='spinner.gif'>&nbsp;&nbsp;Sedang Proses ...";
					   var n=encodeURIComponent($('nm_db').value);
                       var ins_s=encodeURIComponent(hapusspasichars($('ins_s').value,' '));  
					   var urlb="ex_sql_d_s.php?ins_s="+ins_s+"&n="+n+"&r="+new Date().getTime();
                       new Ajax.Request(
                       urlb,
                       {
                               method:"get",
			                   onSuccess:function(r) {
														if(r.responseText==1)
	                                                    {
		                                           			document.getElementById("id_td_0").innerHTML="";
		                                                    alert("Folder 'backup_data_bkkbn\backup_database' Untuk Menyimpan File Tidak Ada !!!");
		                                                    document.getElementById("id_0").checked=false;
                                                        }
														else if(r.responseText==2)
	                                                    {
		                                                    document.getElementById("id_td_0").innerHTML='<font color="red">GAGAL</font> Membackup Tabel | <a href=javascript:void(0) onclick="rst_t(0)">reset</a>';
		                                                    document.getElementById("id_0").disabled=true;
	                                                    }
														else if(r.responseText!="")
	                                                    {
		                                                    document.getElementById("id_td_0").innerHTML='GAGAL, <font color="red">'+r.responseText+'</font> | <a href=javascript:void(0) onclick="rst_t(0)">reset</a>';
	                                                        document.getElementById("id_0").disabled=true;
                                                        }
														else
														{
														    document.getElementById("id_td_0").innerHTML='<font color="blue">OK</font>, Nama File : dbo_semua_tabel.sql | <a href=javascript:void(0) onclick="rst_t(0)">reset</a>';
														    document.getElementById("id_0").disabled=true;
														}
													    $('bp_s').disabled=false; 
							                        },
					           onFailure:function() { $('bp_s').disabled=false; alert('PROSES GAGAL'); } 

                       }
                     );  
	}
	else
	{
		   alert('DATABASE SUDAH DIBACKUP !!!');
		   $('bp_s').disabled=false; 
		   return false;
	}
	

}

function back_up_khs()
{
	x=/[a-z]/i; x1=/[0-9]/i;
  
	if($('ins_khs').value=='')
	{
		   alert('ISI NAMA FILE TERLEBIH DAHULU !!!');
		   $('bp_khs').disabled=false; 
		   $('ins_khs').focus();
		   return false;
	}
	for(var i=0; i<strp_slashes($('ins_khs').value).length; i++)
	{
	    if(strp_slashes($('ins_khs').value).charAt(i)==" ")
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI SPASI !!!");
			$('bp_khs').disabled=false; 
			$('ins_khs').focus();
			return false;
		}
	}
	
	for(var i=0; i<strp_slashes($('ins_khs').value).length; i++)
	{
		if(strp_slashes($('ins_khs').value).charAt(i).search(x)==-1 && strp_slashes($('ins_khs').value).charAt(i).search(x1)==-1 && strp_slashes($('ins_khs').value).charAt(i)!='_')
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI KARAKTER SELAIN ANGKA (0-9), ABJAD (a-z,A-Z) DAN _ ");
			$('bp_khs').disabled=false; 
			$('ins_khs').focus();
			return false;
		}
    }
    back_up_d_khs(1)
}

function back_up_d_khs(k)
{
				       var ada=false;

					   for(var i=k; i<parseInt($('id_hdn_c_khs').value); i++)
				       {
      							     
							  if($('id_khs_t'+i).value==0)
							  {
								 ada=true;
								 break;
							  }
					   }
					   if(!ada)
					   {
						       alert("SEMUA TABEL SUDAH DIBACKUP !!!");
							   $('bp_khs').disabled=false; 
							   return false;
					   }
				   
					   $('idx_c_khs').value=i;
					   $('id_td_khs'+i).innerHTML="&nbsp;<IMG src='spinner.gif'>&nbsp;&nbsp;Sedang Proses ...";
					   var id_d=$('id_d').value; 
					   var ket_d=$('ket_d').value;
					   var u=$('id_khs'+i).value;
					   var ins=encodeURIComponent(hapusspasichars($('ins_khs').value,' '));
					   
					   var n=encodeURIComponent($('nm_db').value);
					   var urlb="ex_sql_d_khs.php?ins="+ins+"&id_d="+id_d+"&ket_d="+ket_d+"&u="+u+"&n="+n+"&r="+new Date().getTime();
                       new Ajax.Request(
                       urlb,
                       {
                               method:"get",
			                   onSuccess:rslt_u_khs,
							   onFailure:rslt_u_khs 

                       }
                     );
}

function rslt_u_khs(r)
{

	  var k=parseInt($('idx_c_khs').value);
	  $('id_khs_t'+k).value=1;
	  if(r.responseText==1)
	  {
		  document.getElementById("id_td_khs"+k).innerHTML="";
		  alert("Folder 'backup_data_bkkbn\backup_data_khusus' Untuk Menyimpan File Tidak Ada !!!");
		  document.getElementById("id_khs"+k).checked=false;
		  $('bp_khs').disabled=false; 
		  return false;
	  }
	  if(r.responseText==2)
	  {
		  document.getElementById("id_td_khs"+k).innerHTML='<font color="red">GAGAL</font> Membackup Tabel: '+document.getElementById("id_khs"+k).value+' | <a href=javascript:void(0) onclick="rst_t_khs('+k+')">reset</a>';

	  }
	  else if(r.responseText!="")
	  {
		 document.getElementById("id_td_khs"+k).innerHTML='GAGAL, <font color="red">'+r.responseText+'</font> | <a href=javascript:void(0) onclick="rst_t_khs('+k+')">reset</a>';

      }
	  else
	  {
	  document.getElementById("id_td_khs"+k).innerHTML='<font color="blue">OK</font>, Nama File : '+document.getElementById("id_khs"+k).value+'.sql | <a href=javascript:void(0) onclick="rst_t_khs('+k+')">reset</a>';

	  }
      
	  k=parseInt($('idx_c_khs').value)+1;
      var ada=false;
	  if(k < parseInt($('id_hdn_c_khs').value))
	  {
	           for(var i=k; i<parseInt($('id_hdn_c_khs').value); i++)
			   {
				  if($('id_khs_t'+i).value==0)
				  {
 					   ada=true;
					   $('idx_c_khs').value=i;
					   break;
			      }
			   }
			   if(ada)
		          back_up_d_khs(i);
			   else	 
			     $('bp_khs').disabled=false; 
      }    
	  else	 
	     $('bp_khs').disabled=false; 
}



function back_up()
{
	x=/[a-z]/i; x1=/[0-9]/i;
  
	if($('ins').value=='')
	{
		   alert('ISI NAMA FILE TERLEBIH DAHULU !!!');
		   $('bp').disabled=false; 
		   $('ins').focus();
		   return false;
	}
	for(var i=0; i<strp_slashes($('ins').value).length; i++)
	{
	    if(strp_slashes($('ins').value).charAt(i)==" ")
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI SPASI !!!");
			$('bp').disabled=false; 
			$('ins').focus();
			return false;
		}
	}
	
	for(var i=0; i<strp_slashes($('ins').value).length; i++)
	{
		if(strp_slashes($('ins').value).charAt(i).search(x)==-1 && strp_slashes($('ins').value).charAt(i).search(x1)==-1 && strp_slashes($('ins').value).charAt(i)!='_')
		{
		    alert("NAMA FILE TIDAK BOLEH BERISI KARAKTER SELAIN ANGKA (0-9), ABJAD (a-z,A-Z) DAN _ ");
			$('bp').disabled=false; 
			$('ins').focus();
			return false;
		}
    }
    back_up_d(1)
}

function back_up_d(k)
{
		     
		
		if(k==1)
		{
			 var v=parseInt($('id_hdn_c').value); var ada=false;
			 
			 for(var k=1;k<v;k++)
			 {
			   if($('id_'+k).disabled==false)  
			   {	 
				     ada=true;
					 break;
			   } 
			 }
			 if(!ada)
			 {
				     alert('SEMUA TABEL SUDAH DIBACKUP !!!');
					 $('bp').disabled=false; 
					 return false;
			 }
			 
			 
			 ada=false;
			 for(var k=1;k<v;k++)
			 {
			   if($('id_'+k).checked && $('id_'+k).disabled!=true)  
			   {	 
					 $('idx_c').value=k;
					 ada=true;
					 break;
			   } 
			 }
			 
			 
			 if(!ada)
			 {
				  alert('SILAHKAN PILIH TABEL TERLEBIH DAHULU !!!'); 
			      $('bp').disabled=false; 
				  return false;
			 }
	     }
				        
					   $('id_td_'+k).innerHTML="&nbsp;<IMG src='spinner.gif'>&nbsp;&nbsp;Sedang Proses ...";
					   var id_d=$('id_d').value; 
					   var ket_d=$('ket_d').value;
					   var u=$('id_'+k).value;
					   var ins=encodeURIComponent(hapusspasichars($('ins').value,' '));
					   var n=encodeURIComponent($('nm_db').value);
					   var urlb="ex_sql_d.php?ins="+ins+"&id_d="+id_d+"&id_d="+id_d+"&ket_d="+ket_d+"&u="+u+"&n="+n+"&r="+new Date().getTime();
                       new Ajax.Request(
                       urlb,
                       {
                               method:"get",
			                   onSuccess:rslt_u,
							   onFailure:rslt_u 

                       }
                     );
}

function rslt_u(r)
{

	  var k=parseInt($('idx_c').value);
	  if(r.responseText==1)
	  {
		  document.getElementById("id_td_"+k).innerHTML="";
		  alert("Folder 'backup_data_bkkbn\backup_data_tabel' Untuk Menyimpan File Tidak Ada !!!");
		  document.getElementById("id_"+k).checked=false;
		  $('bp').disabled=false; 
		  return false;
	  }
	  if(r.responseText==2)
	  {
		  document.getElementById("id_td_"+k).innerHTML='<font color="red">GAGAL</font> Membackup Tabel: '+document.getElementById("id_"+k).value+' | <a href=javascript:void(0) onclick="rst_t('+k+')">reset</a>';
		  document.getElementById("id_"+k).disabled=true;
	  }
	  else if(r.responseText!="")
	  {
		 document.getElementById("id_td_"+k).innerHTML='GAGAL, <font color="red">'+r.responseText+'</font> | <a href=javascript:void(0) onclick="rst_t('+k+')">reset</a>';
	     document.getElementById("id_"+k).disabled=true;
      }
	  else
	  {
	  document.getElementById("id_td_"+k).innerHTML='<font color="blue">OK</font>, Nama File : '+document.getElementById("id_"+k).value+'.sql | <a href=javascript:void(0) onclick="rst_t('+k+')">reset</a>';
	  document.getElementById("id_"+k).disabled=true;
	  }
      k=parseInt($('idx_c').value)+1;
	  var v=parseInt($('id_hdn_c').value); var ada=false;
	  if(k < parseInt($('id_hdn_c').value))
	  {
	         for(var i=k;i<v;i++)
			 {
			   if($('id_'+i).checked && $('id_'+i).disabled!=true)  
			   {	 
				     var u=$('id_'+i).value;
					 $('idx_c').value=i;
					 ada=true;
					 break;
			   } 
			 }
			 if(ada)
		         back_up_d(i);
			 else	 
			    $('bp').disabled=false; 
	  }   
	  else	 
	     $('bp').disabled=false; 
	  
}