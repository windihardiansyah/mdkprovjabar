<?php
define('FPDF_FONTPATH','fpdf/font/');
require('mc_table.php');
include "../koneksi/koneksi.inc.php";
$pdf= new PDF_MC_Table('L','mm','A4');
$pdf->Open();
$pdf->SetMargins(5, 15, 5);
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(30,10,'F/I/MDK/2008',0,0,'L',1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(250.8,10,'FORMULIR MUTASI DATA KELUARGA',0,0,'C',1);
$pdf->Ln(7.5);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(25,5,'PROVINSI','LT',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(40,5,':Jawa Barat','T',0,'L',1);
$pdf->Cell(4.5,5,'3','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'2','LTRB',0,'C',1);
$pdf->Cell(15.8,5,'','LT',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(16.5,5,'Kab/Kota','T',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(20,5,':Bandung','T',0,'L',1);
$pdf->Cell(4.5,5,'7','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'3','LTRB',0,'C',1);
$pdf->Cell(14.7,5,'','LT',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(19.8,5,'Kecamatan','T',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(45,5,':','TR',0,'L',1);
$pdf->Cell(10,5,'','LTRB',0,'C',1);
$pdf->Cell(65,5,'Hal..........Dari........','LTR',0,'R',1);

$pdf->Ln(5);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(25,5,'Desa/Kelurahan','LB',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(40,5,':','B',0,'L',1);
$pdf->Cell(15,5,'','LTRB',0,'C',1);
$pdf->Cell(10,5,'','LB',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(16.3,5,'Dusun/RW','B',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(20,5,':','RB',0,'L',1);
$pdf->Cell(13.5,5,'','LTRB',0,'C',1);
$pdf->Cell(10,5,'','LB',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(20,5,'RT','B',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(45,5,':','RB',0,'L',1);
$pdf->Cell(13.5,5,'','LTRB',0,'C',1);
$pdf->Cell(39,5,'Kode Pos','LRB',0,'R',1);
$pdf->Cell(4.5,5,'','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'','LTRB',0,'C',1);
$pdf->Cell(4.5,5,'','LTRB',0,'C',1);

$pdf->Ln(7.5);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'Keluarga Pindah','R',0,'L',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(40,8,'No. Urut','L',0,'C',1);
$pdf->Cell(60,5,'',0,0,'C',1);
$pdf->Cell(20,8,'Bantuan',0,0,'C',1);
$pdf->Cell(20,8,'Ikut',0,0,'C',1);
$pdf->Cell(15,5,'',0,0,'C',1);
$pdf->Cell(70,7,'Peserta KB','B',0,'C',1);
$pdf->Cell(18,8,'Bukan',0,0,'C',1);

$pdf->Ln(5.75);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'Keluarga Baru','R',0,'L',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(40,5,'Rumah Tangga','L',0,'C',1);
$pdf->Cell(60,5,'Nomor Kode Keluarga Indonesia (KKI)',0,0,'C',1);
$pdf->Cell(20,5,'Modal',0,0,'C',1);
$pdf->Cell(20,5,'Posyandu',0,0,'C',1);
$pdf->Cell(15,5,'PUS',0,0,'C',1);
$pdf->Cell(20,5,'Pemerintah','T',0,'C',1);
$pdf->Cell(20,5,'Swasta','T',0,'C',1);
$pdf->Cell(30,5,'Implan Akan Dicabut','T',0,'C',1);
$pdf->Cell(18,5,'Peserta KB',0,0,'C',1);

$pdf->Ln(5);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'',0,0,'L',1);
$pdf->Cell(5.2,5,'','T',0,'L',1);

//rumah tangga
$pdf->Cell(7.5,5,'','R',0,'L',1);
$pdf->Cell(25,5,'','LTRB',0,'C',1);
$pdf->Cell(7.5,5,'','L',0,'L',1);

//kki
$pdf->Cell(60,5,'','LTRB',0,'C',1);

//modal
$pdf->Cell(7.5,5,'','LR',0,'C',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(7.5,5,'','L',0,'C',1);

//posyandu
$pdf->Cell(7.5,5,'','R',0,'C',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(7.5,5,'','L',0,'C',1);

//pus
$pdf->Cell(5,5,'','R',0,'C',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(5,5,'','L',0,'C',1);

//pemerintah
$pdf->Cell(5,5,'','R',0,'C',1);
$pdf->Cell(10,5,'','LTRB',0,'C',1);
$pdf->Cell(5,5,'','L',0,'C',1);

//swasta
$pdf->Cell(5,5,'','R',0,'C',1);
$pdf->Cell(10,5,'','LTRB',0,'C',1);
$pdf->Cell(5,5,'','L',0,'C',1);

//implan
$pdf->Cell(10,5,'','R',0,'C',1);
$pdf->Cell(10,5,'','LTRB',0,'C',1);
$pdf->Cell(10,5,'','L',0,'C',1);


//peserta KB
$pdf->Cell(18,5,'','LTRB',0,'C',1);

$pdf->Ln(7.5);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'Perubahan Data Keluarga','R',0,'L',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(40,8,'Indikator','L',0,'C',1);


$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk is NULL  AND id_ind_ks!='5'ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc=mysql_fetch_array($qr))
{
       $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	   while($rc1=mysql_fetch_array($qr1))
	   {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	      $jmd=mysql_num_rows($qr2);
		  if($jmd==0)
		     $jmd=4;
		  else	 
	         $jmd=4*$jmd;
	      $pdf->Cell($jmd,6.5,$rc1[no_ind_ks],'LTRB',0,'C',1);
	   }
       $pdf->Cell(5,5,'','L',0,'C',1);  
}


$pdf->Cell(25,9,'Indikator',0,0,'C',1);
$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks='5' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$i=1;
while($rc=mysql_fetch_array($qr))
{
       $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	   while($rc1=mysql_fetch_array($qr1))
	   {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	      $jmd=mysql_num_rows($qr2);
		  if($jmd==0)
		     $jmd=4;
		  else	 
	         $jmd=4*$jmd;
	      $pdf->Cell($jmd,6.5,$i,'LTRB',0,'C',1);
		     $i++; 
	   }
       $pdf->Cell(5,5,'','L',0,'C',1); 
	
}

$pdf->Ln(5.75);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(40,5,'Perubahan Data Angg. Kel.','R',0,'L',1);
$pdf->Cell(5,5,'','LTRB',0,'C',1);
$pdf->Cell(40,5,'Tahapan KS','L',0,'C',1);

//kueri ks family

$qr_ks=mysql_query("SELECT * FROM dbo_fam_ind_detail WHERE Id_fam='354941' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc_ks=mysql_fetch_array($qr_ks))
{
	      $arr_id_ks[$rc_ks[id_ind_ks]]=$rc_ks[kd_prospinstat]; 
}


$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk is NULL AND Id_ind_ks!='5' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$c_ks=1;
while($rc=mysql_fetch_array($qr))
{
      $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	  while($rc1=mysql_fetch_array($qr1))
	  {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql)); 
		  if(mysql_num_rows($qr2)==0)
		  {
		      if(isset($arr_id_ks[$rc1[id_ind_ks]]))
			  {     
				   $id_arr_id_ks=$arr_id_ks[$rc1[id_ind_ks]];
				   $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				   $rc3=mysql_fetch_array($qr3);
				   $td_pros=trim($rc3[R_ProsPinStat]);
				   $kd_pros=$rc3[Kd_prospinstat];
              }	   
			  else
			  {   
			       $kd_pros="";
				   $td_pros="";
			  }
	  	      $pdf->Cell(4,5,$td_pros,'LTRB',0,'C',1);
		  }
		  else if(mysql_num_rows($qr2)>0)
	      {
          	  while($rc2=mysql_fetch_array($qr2))
	          {
			        if(isset($arr_id_ks[$rc2[id_ind_ks]]))
			        {     
				        $id_arr_id_ks=$arr_id_ks[$rc2[id_ind_ks]];
				        $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				        $rc3=mysql_fetch_array($qr3);
				        $td_pros=trim($rc3[R_ProsPinStat]);
				        $kd_pros=$rc3[Kd_prospinstat];
                    }	   
			        else
			        {    
			            $kd_pros="";
				        $td_pros="";
			        }
					$pdf->Cell(4,5,$td_pros,'LTRB',0,'C',1);
                    $c_ks++;			  
			  }   
		      $c_ks--;
		  }
		  $c_ks++;      
}
 $pdf->Cell(5,5,'','L',0,'C',1); 
}
$pdf->Cell(25,5,'Tambahan',0,0,'C',1);

$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks='5' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$c_ks=1;
while($rc=mysql_fetch_array($qr))
{
      $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	  while($rc1=mysql_fetch_array($qr1))
	  {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql)); 
		  if(mysql_num_rows($qr2)==0)
		  {
		      if(isset($arr_id_ks[$rc1[id_ind_ks]]))
			  {     
				   $id_arr_id_ks=$arr_id_ks[$rc1[id_ind_ks]];
				   $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				   $rc3=mysql_fetch_array($qr3);
				   $td_pros=trim($rc3[R_ProsPinStat]);
				   $kd_pros=$rc3[Kd_prospinstat];
              }	   
			  else
			  {   
			       $kd_pros="";
				   $td_pros="";
			  }
	  	      $pdf->Cell(4,5,$td_pros,'LTRB',0,'C',1);
		  }
		  else if(mysql_num_rows($qr2)>0)
	      {
          	  while($rc2=mysql_fetch_array($qr2))
	          {
			        if(isset($arr_id_ks[$rc2[id_ind_ks]]))
			        {     
				        $id_arr_id_ks=$arr_id_ks[$rc2[id_ind_ks]];
				        $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				        $rc3=mysql_fetch_array($qr3);
				        $td_pros=trim($rc3[R_ProsPinStat]);
				        $kd_pros=$rc3[Kd_prospinstat];
                    }	   
			        else
			        {    
			            $kd_pros="";
				        $td_pros="";
			        }
					$pdf->Cell(4,5,$td_pros,'LTRB',0,'C',1);
                    $c_ks++;			  
			  }   
		      $c_ks--;
		  }
		  $c_ks++;      
}
 $pdf->Cell(5,5,'','L',0,'C',1); 
}


$pdf->Ln(10);




$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(33,5,'Nomor Kode Anggota Keluarga (KAK)','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 33, $pdf->GetY() - $yH);
$pdf->Cell(57.5,$yH ,'Nama','LTRB',0,'C',1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(22.5,5,'Hubungan dengan KK','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 22.5, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(17.5,5,'Jenis Kelamin','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 17.5, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(17,5,'Tanggal Lahir','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 17, $pdf->GetY() - $yH);
$pdf->Cell(21,$yH ,'Pekerjaan','LTRB',0,'C',1);
$pdf->Cell(35,$yH,'Pendidikan Terakhir','LTRB',0,'C',1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(17.5,5,'Status Perkawinan','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 17.5, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(20,5,'Ikut Program Posyandu','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 20, $pdf->GetY() - $yH);
$pdf->Cell(22.5,$yH ,'Mutasi','LTRB',0,'C',1);
$pdf->Cell(26.5,$yH ,'Keterangan','LTRB',0,'C',1);
$pdf->Ln();


$pdf->SetWidths(array(33,57.5,22.5,17.5,17,21,35,17.5,20,22.5,26.5));
//daftar anggota keluarga
$qr_i_a_k=mysql_query("SELECT * FROM dbo_individu WHERE Id_fam='354941' ORDER BY Kd_fammbrtyp", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc_i_a_k=mysql_fetch_array($qr_i_a_k))
{
  $arr1=$rc_i_a_k[Kd_indv];
  $arr2=$rc_i_a_k[Nama];
  
  $qr=mysql_query("SELECT * FROM dbo_fam_mbr_typ WHERE Kd_fammbrtyp='$rc_i_a_k[Kd_fammbrtyp]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr3=$rc[Nm_fammbrtyp_ind];
 
  $qr=mysql_query("SELECT * FROM dbo_gender WHERE Kd_gen='$rc_i_a_k[Kd_gen]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr4=$rc[Nm_gen_ind];
  $tgl_spl=explode("-",$rc_i_a_k[Tgl_lahir]);
  $arr5=$tgl_spl[2]."-".$tgl_spl[1]."-".$tgl_spl[0];

  $qr=mysql_query("SELECT * FROM dbo_empmnt_stat WHERE Kd_emp='$rc_i_a_k[Kd_emp]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr6=$rc[Nm_emp_ind];

  $qr=mysql_query("SELECT * FROM dbo_edu_lvl WHERE Kd_edu='$rc_i_a_k[Kd_edu]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr7=$rc[Nm_edu_ind];

  $qr=mysql_query("SELECT * FROM dbo_martl_stat WHERE Kd_martl='$rc_i_a_k[Kd_martl]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr8=$rc[Nm_martl_ind];
  
  $qr=mysql_query("SELECT * FROM dbo_mutasi WHERE Kd_mutasi='$rc_i_a_k[Kd_mutasi]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr9="Tidak Ikut Posyandu";
  $arr10=$rc[Nm_mutasi];
  
  $arr11=$rc_i_a_k[Keterangan];
 

	$pdf->Row(array($arr1,$arr2,$arr3,$arr4,$arr5,$arr6,$arr7,$arr8,$arr9,$arr10,$arr11));
}
$pdf->Output();







?>