<?php
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
include "../koneksi/koneksi.inc.php";
$pdf= new FPDF('L','cm','A4');
$pdf->Open();
$pdf->SetMargins(0.5, 1.5, 0.5);
$pdf->AddPage();
     
$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(3,1,'F/I/MDK/2008',0,0,'L',1);
$pdf->SetFont('Arial','B',13);
$pdf->Cell(25.8,1,'FORMULIR MUTASI DATA KELUARGA',0,0,'C',1);
$pdf->Ln(1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(2.5,0.5,'PROVINSI','LT',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(4,0.5,':Jawa Barat','T',0,'L',1);
$pdf->Cell(0.45,0.5,'3','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'2','LTRB',0,'C',1);
$pdf->Cell(1.58,0.5,'','LT',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(1.65,0.5,'Kab/Kota','T',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(2,0.5,':Bandung','T',0,'L',1);
$pdf->Cell(0.45,0.5,'7','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'3','LTRB',0,'C',1);
$pdf->Cell(1.47,0.5,'','LT',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(1.98,0.5,'Kecamatan','T',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(4.5,0.5,':','TR',0,'L',1);
$pdf->Cell(1,0.5,'','LTRB',0,'C',1);
$pdf->Cell(6.4,0.5,'Hal..........Dari........','LTR',0,'R',1);

$pdf->Ln(0.5);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(2.5,0.5,'Desa/Kelurahan','LB',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(4,0.5,':','B',0,'L',1);
$pdf->Cell(1.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(1,0.5,'','LB',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(1.63,0.5,'Dusun/RW','B',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(2,0.5,':','RB',0,'L',1);
$pdf->Cell(1.35,0.5,'','LTRB',0,'C',1);
$pdf->Cell(1,0.5,'','LB',0,'C',1);
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(2,0.5,'RT','B',0,'L',1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(4.5,0.5,':','RB',0,'L',1);
$pdf->Cell(1.35,0.5,'','LTRB',0,'C',1);
$pdf->Cell(3.8,0.5,'Kode Pos','LRB',0,'R',1);
$pdf->Cell(0.45,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.45,0.5,'','LTRB',0,'C',1);

$pdf->Ln(0.75);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(4,0.5,'Keluarga Pindah','R',0,'L',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(4,0.5,'No. Urut','L',0,'C',1);
$pdf->Cell(6,0.5,'',0,0,'C',1);
$pdf->Cell(2,0.5,'Bantuan',0,0,'C',1);
$pdf->Cell(2,0.5,'Ikut',0,0,'C',1);
$pdf->Cell(1.5,0.5,'',0,0,'C',1);
$pdf->Cell(7,0.5,'Peserta KB','B',0,'C',1);
$pdf->Cell(1.8,0.5,'Bukan',0,0,'C',1);

$pdf->Ln(0.575);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(4,0.5,'Keluarga Baru','R',0,'L',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(4,0.5,'Rumah Tangga','L',0,'C',1);
$pdf->Cell(6,0.5,'Nomor Kode Keluarga Indonesia (KKI)',0,0,'C',1);
$pdf->Cell(2,0.5,'Modal',0,0,'C',1);
$pdf->Cell(2,0.5,'Posyandu',0,0,'C',1);
$pdf->Cell(1.5,0.5,'PUS',0,0,'C',1);
$pdf->Cell(2,0.5,'Pemerintah','T',0,'C',1);
$pdf->Cell(2,0.5,'Swasta','T',0,'C',1);
$pdf->Cell(3,0.5,'Implan Akan Dicabut','T',0,'C',1);
$pdf->Cell(1.8,0.5,'Peserta KB',0,0,'C',1);

$pdf->Ln(0.5);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(4,0.5,'',0,0,'L',1);
$pdf->Cell(0.52,0.5,'','T',0,'L',1);

//rumah tangga
$pdf->Cell(0.75,0.5,'','R',0,'L',1);
$pdf->Cell(2.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.75,0.5,'','L',0,'L',1);

//kki
$pdf->Cell(6,0.5,'','LTRB',0,'C',1);

//modal
$pdf->Cell(0.75,0.5,'','LR',0,'C',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.75,0.5,'','L',0,'C',1);

//posyandu
$pdf->Cell(0.75,0.5,'','R',0,'C',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.75,0.5,'','L',0,'C',1);

//pus
$pdf->Cell(0.5,0.5,'','R',0,'C',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.5,0.5,'','L',0,'C',1);

//pemerintah
$pdf->Cell(0.5,0.5,'','R',0,'C',1);
$pdf->Cell(1,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.5,0.5,'','L',0,'C',1);

//swasta
$pdf->Cell(0.5,0.5,'','R',0,'C',1);
$pdf->Cell(1,0.5,'','LTRB',0,'C',1);
$pdf->Cell(0.5,0.5,'','L',0,'C',1);

//implan
$pdf->Cell(1,0.5,'','R',0,'C',1);
$pdf->Cell(1,0.5,'','LTRB',0,'C',1);
$pdf->Cell(1,0.5,'','L',0,'C',1);


//peserta KB
$pdf->Cell(1.8,0.5,'',1,0,'C',1);

//sesi indikator ks
$pdf->Ln(0.75);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(4,0.5,'Perubahan Data Keluarga','R',0,'L',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(4,0.5,'Indikator','L',0,'C',1);


$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk is NULL  AND id_ind_ks!='5'ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc=mysql_fetch_array($qr))
{
       $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	   while($rc1=mysql_fetch_array($qr1))
	   {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	      $jmd=mysql_num_rows($qr2);
		  if($jmd==0)
		     $jmd=0.4;
		  else	 
	         $jmd=0.4*$jmd;
	      $pdf->Cell($jmd,0.65,$rc1[no_ind_ks],'LTRB',0,'C',1);
	   }
       $pdf->Cell(0.5,0.5,'','L',0,'C',1);  
}


$pdf->Cell(2.5,0.5,'Indikator',0,0,'C',1);
$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks='5'ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$i=1;
while($rc=mysql_fetch_array($qr))
{
       $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	   while($rc1=mysql_fetch_array($qr1))
	   {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	      $jmd=mysql_num_rows($qr2);
		  if($jmd==0)
		     $jmd=0.4;
		  else	 
	         $jmd=0.4*$jmd;
	      $pdf->Cell($jmd,0.65,$i,'LTRB',0,'C',1);
		     $i++; 
	   }
       $pdf->Cell(0.5,0.5,'','L',0,'C',1); 
	
}

$pdf->Ln(0.65);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$pdf->Cell(4,0.5,'Perubahan Data Angg. Kel.','R',0,'L',1);
$pdf->Cell(0.5,0.5,'','LTRB',0,'C',1);
$pdf->Cell(4,0.5,'Tahapan KS','L',0,'C',1);

//kueri ks family

$qr_ks=mysql_query("SELECT * FROM dbo_fam_ind_detail WHERE Id_fam='1' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc_ks=mysql_fetch_array($qr_ks))
{
	      $arr_id_ks[$rc_ks[id_ind_ks]]=$rc_ks[kd_prospinstat]; 
}


$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk is NULL AND Id_ind_ks!='5' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$c_ks=1;
while($rc=mysql_fetch_array($qr))
{
      $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	  while($rc1=mysql_fetch_array($qr1))
	  {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql)); 
		  if(mysql_num_rows($qr2)==0)
		  {
		      if(isset($arr_id_ks[$rc1[id_ind_ks]]))
			  {     
				   $id_arr_id_ks=$arr_id_ks[$rc1[id_ind_ks]];
				   $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				   $rc3=mysql_fetch_array($qr3);
				   $td_pros=trim($rc3[R_ProsPinStat]);
				   $kd_pros=$rc3[Kd_prospinstat];
              }	   
			  else
			  {   
			       $kd_pros="";
				   $td_pros="";
			  }
	  	      $pdf->Cell(0.4,0.5,$td_pros,'LTRB',0,'C',1);
		  }
		  else if(mysql_num_rows($qr2)>0)
	      {
          	  while($rc2=mysql_fetch_array($qr2))
	          {
			        if(isset($arr_id_ks[$rc2[id_ind_ks]]))
			        {     
				        $id_arr_id_ks=$arr_id_ks[$rc2[id_ind_ks]];
				        $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				        $rc3=mysql_fetch_array($qr3);
				        $td_pros=trim($rc3[R_ProsPinStat]);
				        $kd_pros=$rc3[Kd_prospinstat];
                    }	   
			        else
			        {    
			            $kd_pros="";
				        $td_pros="";
			        }
					$pdf->Cell(0.4,0.5,$td_pros,'LTRB',0,'C',1);
                    $c_ks++;			  
			  }   
		      $c_ks--;
		  }
		  $c_ks++;      
}
 $pdf->Cell(0.5,0.5,'','L',0,'C',1); 
}
$pdf->Cell(2.5,0.5,'Tambahan',0,0,'C',1);

$qr=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks='5' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$c_ks=1;
while($rc=mysql_fetch_array($qr))
{
      $qr1=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	  while($rc1=mysql_fetch_array($qr1))
	  {
	      $qr2=mysql_query("SELECT * FROM dbo_indikator_ks WHERE id_ind_ks_idk='$rc1[id_ind_ks]' ORDER BY no_ind_ks", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql)); 
		  if(mysql_num_rows($qr2)==0)
		  {
		      if(isset($arr_id_ks[$rc1[id_ind_ks]]))
			  {     
				   $id_arr_id_ks=$arr_id_ks[$rc1[id_ind_ks]];
				   $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				   $rc3=mysql_fetch_array($qr3);
				   $td_pros=trim($rc3[R_ProsPinStat]);
				   $kd_pros=$rc3[Kd_prospinstat];
              }	   
			  else
			  {   
			       $kd_pros="";
				   $td_pros="";
			  }
	  	      $pdf->Cell(0.4,0.5,$td_pros,'LTRB',0,'C',1);
		  }
		  else if(mysql_num_rows($qr2)>0)
	      {
          	  while($rc2=mysql_fetch_array($qr2))
	          {
			        if(isset($arr_id_ks[$rc2[id_ind_ks]]))
			        {     
				        $id_arr_id_ks=$arr_id_ks[$rc2[id_ind_ks]];
				        $qr3=mysql_query("SELECT * FROM dbo_prosp_ind_stat WHERE Kd_prospinstat='$id_arr_id_ks' ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
				        $rc3=mysql_fetch_array($qr3);
				        $td_pros=trim($rc3[R_ProsPinStat]);
				        $kd_pros=$rc3[Kd_prospinstat];
                    }	   
			        else
			        {    
			            $kd_pros="";
				        $td_pros="";
			        }
					$pdf->Cell(0.4,0.5,$td_pros,'LTRB',0,'C',1);
                    $c_ks++;			  
			  }   
		      $c_ks--;
		  }
		  $c_ks++;      
}
 $pdf->Cell(0.5,0.5,'','L',0,'C',1); 
}

//anggota keluarga
$pdf->Ln(1);

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(3.3,0.5,'Nomor Kode Anggota Keluarga (KAK)','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 3.3, $pdf->GetY() - $yH);
$pdf->Cell(5.75,$yH ,'Nama','LTRB',0,'C',1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(2.25,0.5,'Hubungan dengan KK','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 2.25, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(1.75,0.5,'Jenis Kelamin','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.75, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(1.7,0.5,'Tanggal Lahir','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.7, $pdf->GetY() - $yH);
$pdf->Cell(2.1,$yH ,'Pekerjaan','LTRB',0,'C',1);
$pdf->Cell(3.5,$yH,'Pendidikan Terakhir','LTRB',0,'C',1);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(1.75,0.5,'Status Perkawinan','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.75, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(2,0.5,'Ikut Program Posyandu','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 2, $pdf->GetY() - $yH);
$pdf->Cell(2.25,$yH ,'Mutasi','LTRB',0,'C',1);
$pdf->Cell(2.75,$yH ,'Keterangan','LTRB',0,'C',1);
$pdf->Ln();

//daftar anggota keluarga
$qr_i_a_k=mysql_query("SELECT * FROM dbo_individu WHERE Id_fam='1' ORDER BY Kd_fammbrtyp", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
while($rc_i_a_k=mysql_fetch_array($qr_i_a_k))
{


$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(256,256,256);
$pdf->SetTextColor(0,0,0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(3.3,1,$rc_i_a_k[Kd_indv],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 3.3, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(5.75,1 ,$rc_i_a_k[Nama],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 5.75, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$qr=mysql_query("SELECT * FROM dbo_fam_mbr_typ WHERE Kd_fammbrtyp='$rc_i_a_k[Kd_fammbrtyp]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->MultiCell(2.25,0.5,$rc[Nm_fammbrtyp_ind],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 2.25, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$qr=mysql_query("SELECT * FROM dbo_gender WHERE Kd_gen='$rc_i_a_k[Kd_gen]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->MultiCell(1.75,1,$rc[Nm_gen_ind],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.75, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$tgl_spl=explode("-",$rc_i_a_k[Tgl_lahir]);
$pdf->MultiCell(1.7,1,$tgl_spl[2]."-".$tgl_spl[1]."-".$tgl_spl[0],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.7, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$qr=mysql_query("SELECT * FROM dbo_empmnt_stat WHERE Kd_emp='$rc_i_a_k[Kd_emp]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->MultiCell(2.1,1,$rc[Nm_emp_ind],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 2.1, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$qr=mysql_query("SELECT * FROM dbo_edu_lvl WHERE Kd_edu='$rc_i_a_k[Kd_edu]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->MultiCell(3.5,1,$rc[Nm_edu_ind],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 3.5, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$qr=mysql_query("SELECT * FROM dbo_martl_stat WHERE Kd_martl='$rc_i_a_k[Kd_martl]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->MultiCell(1.75,1,$rc[Nm_martl_ind],'LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 1.75, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(2,1,'Ikut Program Posyandu','LTRB','C');
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 2, $pdf->GetY() - $yH);
$qr=mysql_query("SELECT * FROM dbo_mutasi WHERE Kd_mutasi='$rc_i_a_k[Kd_mutasi]'", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$rc=mysql_fetch_array($qr);
$pdf->Cell(2.25,1 ,$rc[Nm_mutasi],'LTRB',0,'C',1);
$pdf->Cell(2.75,1 ,$rc_i_a_k[Keterangan],'LTRB',0,'C',1);
$pdf->Ln();
}
$pdf->Output();







?>