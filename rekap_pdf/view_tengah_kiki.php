<?php
$pdf->Cell(30,2.5,'2. Status Keluarga dan KB',0,0,'L',0);
$pdf->Ln(3.2);
$pdf->SetFont('Arial','',6.5);
$pdf->Cell(15,3,'Jumlah','LT',0,'C',0);
$pdf->Cell(15,3,'PUS','LT',0,'C',0);
$pdf->Cell(72,3,'Status PUS',1,0,'C',0);
$pdf->Cell(22,3,'Implan Cabut','LT',0,'C',0);
$pdf->Cell(20,3,'Keluarga','LT',0,'C',0);
$pdf->Cell(20,3,'Ikut','LT',0,'C',0);
$pdf->Cell(20,3,'Keluarga','LT',0,'C',0);
$pdf->Cell(20,3,'Keluarga','LTR',0,'C',0);
$pdf->Ln();
$pdf->Cell(15,3,'WUS','L',0,'C',0);
$pdf->Cell(15,3,'','L',0,'C',0);
$pdf->Cell(24,3,'KB','L',0,'C',0);
$pdf->Cell(24,3,'Jalur','L',0,'C',0);
$pdf->Cell(24,3,'Tidak KB','L',0,'C',0);
$pdf->Cell(22,3,'Tahun ini','L',0,'C',0);
$pdf->Cell(20,3,'Balita','L',0,'C',0);
$pdf->Cell(20,3,'Posyandu','L',0,'C',0);
$pdf->Cell(20,3,'Remaja','L',0,'C',0);
$pdf->Cell(20,3,'Lansia','LR',0,'C',0);
$pdf->Ln();
if($wus==0)
   $wus='0';
$pdf->Cell(15,2.7,$wus,1,0,'C',0);
if(($rc_isi[pus]!=NULL OR $rc_isi[pus]!='') AND $rc_isi[pus]==1) $pus='Ya'; else $pus='Tidak';
    $pdf->Cell(15,2.7,$pus,1,0,'C',0);
if((($rc_isi[pus]!=NULL OR $rc_isi[pus]!='') AND $rc_isi[pus]==1) AND ($rc_isi[Kd_contyp]!=NULL OR $rc_isi[Kd_contyp]!=''))
      $contyp=$rc_isi[contyp]; 
  else
      $contyp='Tidak Berlaku'; 	
$pdf->Cell(24,2.7,$contyp,1,0,'C',0);
if((($rc_isi[pus]!=NULL OR $rc_isi[pus]!='') AND $rc_isi[pus]==1) AND ($rc_isi[Kd_contyp]!=NULL OR $rc_isi[Kd_contyp]!=''))
      $consrc=$rc_isi[consrc]; 
  else
      $consrc='Tidak Berlaku'; 	
$pdf->Cell(24,2.7,$consrc,1,0,'C',0);
if((($rc_isi[pus]!=NULL OR $rc_isi[pus]!='') AND $rc_isi[pus]==1) AND ($rc_isi[Kd_nonacptr]!=NULL OR $rc_isi[Kd_nonacptr]!=''))
      $nonacptr=$rc_isi[nonacptr]; 
  else
      $nonacptr='Tidak Berlaku'; 	
$pdf->Cell(24,2.7,$nonacptr,1,0,'C',0);
 if((($rc_isi[pus]!=NULL OR $rc_isi[pus]!='') AND $rc_isi[pus]==1) AND ($rc_isi[Kd_contyp]=4))
  {
      if($rc_isi[Kd_implan]==0 AND $rc_isi[Kd_implan]!=NULL)
	     $impl='Y'; 
	  else	 
	     $impl='T'; 
  }	  
  else
      $impl='T'; 
$pdf->Cell(22,2.7,$impl,1,0,'C',0);
$pdf->Cell(20,2.7,$blt,1,0,'C',0);
$pdf->Cell(20,2.7,$ikut_pos,1,0,'C',0);
$pdf->Cell(20,2.7,$rmj,1,0,'C',0);
$pdf->Cell(20,2.7,$lns,1,0,'C',0);

?>