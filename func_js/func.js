function setPointer(theRow, thePointerColor, theCursor)
{
    if (thePointerColor == '' || typeof(theRow.style) == 'undefined') {
        return false;
    }
    if (typeof(document.getElementsByTagName) != 'undefined') {
        var theCells = theRow.getElementsByTagName('td');
    }
    else if (typeof(theRow.cells) != 'undefined') {
        var theCells = theRow.cells;
    }
    else {
        return false;
    }

    var rowCellsCnt  = theCells.length;
    for (var c = 0; c < rowCellsCnt; c++) {
        theCells[c].style.backgroundColor = thePointerColor;
		theCells[c].style.cursor		  = theCursor;
    }

    return true;
} // end of the 'setPointer()' function

function checkrequired(which){
	var pass=true
	if (document.images){
		for (i=0;i<which.length;i++){
		var tempobj=which.elements[i]
			if (tempobj.name.substring(0,3)=="req"){
				if (((tempobj.type=="text"||tempobj.type=="textarea")&&tempobj.value=='')||(tempobj.type.toString().charAt(0)=="s"&&tempobj.selectedIndex==-1)){
					pass=false
					break
				}
			}
		}
	}
	if (!pass){
		alert("Ada sebuah field atau lebih yang wajib diisi.\nLengkapi field yang diberi tanda * dan ulangi lagi!")
		return false
	}
	else
		return true
	}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  //exampe : MM_openBrWindow('Link','WindowName','toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,width=400,height=400')
  window.open(theURL,winName,features);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_setTextOfLayer(objName,x,newText) { //v4.01
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (document.layers) {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}

function cadelete(){
	for (var i=0;i<fmDelete.elements.length;i++) {
		var e = fmDelete.elements[i];
		if ((e.name != 'deletebox') && (e.type=='checkbox') && (e.name.substring(0,6) == 'Delete'))
		{
			e.checked = fmDelete.deletebox.checked;
		}
	}
}

function cadelete2(){
	for (var i=0;i<fmDelete2.elements.length;i++) {
		var e = fmDelete2.elements[i];
		if ((e.name != 'deletebox') && (e.type=='checkbox') && (e.name.substring(0,6) == 'Delete'))
		{
			e.checked = fmDelete2.deletebox.checked;
		}
	}
}

function caactive(){
	for (var i=0;i<fmDelete.elements.length;i++)
	{
		var e = fmDelete.elements[i];
		if ((e.name != 'activebox') && (e.type=='checkbox') && (e.name.substring(0,6) == 'Active'))
		{
			e.checked = fmDelete.activebox.checked;
		}
	}
}

function caselect(){
	for (var i=0;i<fmDelete.elements.length;i++) {
		var e = fmDelete.elements[i];
		if ((e.name != 'selectbox') && (e.type=='checkbox'))
		{
			e.checked = fmDelete.selectbox.checked;
		}
	}
}

function confirmgenerateall() {
	if (window.confirm("Anda yakin ingin mengenerate seluruh tabel terpilih ?"))
	{
  		return (true);
	}
	else
  	  	return (false);
}


function confirmdeleteall() {
	if (window.confirm("Anda yakin ingin menghapus seluruh data terpilih ?"))
	{
  		return (true);
	}
	else
  	  	return (false);
}

function confirmsave() {
	if (window.confirm("Simpan perubahan data ?"))
	{
  		return (true);
	}
	else
  	  	return (false);
}

function confirmactiveall()
{ 
  if (window.confirm("Dengan melakukan publish, menyebabkan data-data tersebut ditampilkan di halaman depan website.\nAnda yakin untuk mempublikasikan seluruh data terpilih ?"))
  {
		return (true);
  }
  else 
		return (false);
}

// JS Calendar
var calendar = null; // remember the calendar object so that we reuse
// it and avoid creating another

// This function gets called when an end-user clicks on some date
function selected(cal, date) {
	cal.sel.value = date; // just update the value of the input field
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks the "Close" (X) button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
	cal.hide();			// hide the calendar

	// don't check mousedown on document anymore (used to be able to hide the
	// calendar when someone clicks outside it, see the showCalendar function).
	Calendar.removeEvent(document, "mousedown", checkCalendar);
}

// This gets called when the user presses a mouse button anywhere in the
// document, if the calendar is shown.  If the click was outside the open
// calendar this function closes it.
function checkCalendar(ev) {
	var el = Calendar.is_ie ? Calendar.getElement(ev) : Calendar.getTargetElement(ev);
	for (; el != null; el = el.parentNode)
	// FIXME: allow end-user to click some link without closing the
	// calendar.  Good to see real-time stylesheet change :)
	if (el == calendar.element || el.tagName == "A") break;
	if (el == null) {
		// calls closeHandler which should hide the calendar.
		calendar.callCloseHandler(); Calendar.stopEvent(ev);
	}
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id) {
	var el = document.getElementById(id);
	if (calendar != null) {
		// we already have one created, so just update it.
		calendar.hide();		// hide the existing calendar
		calendar.parseDate(el.value); // set it to a new date
	} else {
		// first-time call, create the calendar
		var cal = new Calendar(true, null, selected, closeHandler);
		calendar = cal;		// remember the calendar in the global
		cal.setRange(1900, 2070);	// min/max year allowed
		calendar.create();		// create a popup calendar
	}
	calendar.sel = el;		// inform it about the input field in use
	calendar.showAtElement(el);	// show the calendar next to the input field

	// catch mousedown on the document
	Calendar.addEvent(document, "mousedown", checkCalendar);
	return false;
}

