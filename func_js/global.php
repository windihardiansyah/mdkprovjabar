<?php 
	$getmessage = array();
	$getmessage[1] = "Username%20atau%20password%20tidak%20boleh%20kosong";
	$getmessage[2] = "Username%20tidak%20diketemukan%20atau%20password%20anda%20salah";
	$getmessage[3] = "Password%20salah";
	$getmessage[4] = "Anda%20tidak%20memiliki%20otoritas%20atau%20sesi%20anda%20habis";
	$getmessage[5] = "Log%20out%20successfull";
	$getmessage[6] = "telah%20berhasil%20ditambahkan";
	$getmessage[7] = "yang dicari tidak ada";
	$getmessage[8] = "telah%20berhasil%20diubah";

function grab_var($vname,$vdefault="") {
	global $$vname, $HTTP_GET_VARS, $HTTP_POST_VARS;
	if (!isset($$vname) || !$$vname) {
		if (isset($HTTP_POST_VARS[$vname])) {
			$$vname=$HTTP_POST_VARS[$vname];
		}
		else if (isset($HTTP_GET_VARS[$vname])) {
			$$vname=$HTTP_GET_VARS[$vname];
		}
		else {
			$$vname=$vdefault;
		}
	}
}

function createpaging($fa, $startpage, $offset, $limit, $numrows){
				$limitpage = 15;
				if ($startpage>=2) { // bypass PREV link if offset is 0
					$newpage=$startpage-1;
					$newoffset=$newpage*$limit*$limitpage;
					$link = $GLOBALS["self"]."?fa=$fa&startpage=$newpage&offset=$newoffset";
					print "<a href=\"".$link."\">&laquo;</a> &nbsp; \n";
				}
				
				if ($offset>=2) { // bypass PREV link if offset is 0
					$prevoffset=$offset-$limit;
					if ($offset == (($startpage-1)*$limit*$limitpage)) {
						$newpage = $startpage-1;
					} else {
						$newpage = $startpage;
					}
					$link = $GLOBALS["self"]."?fa=$fa&startpage=$newpage&offset=$prevoffset";
					print "<a href=\"".$link."\">&lt;</a> &nbsp; \n";
				}

				// calculate number of pages needing links
				$pages=ceil($numrows/$limit);

				// $pages now contains int of pages needed unless there is a remainder from division
				if (($numrows%$limit) > $limit) {
    			// has remainder so add one page
    				$pages++;
				}

				if ($startpage > 1) {
					$startnewpage = ($startpage-1) * $limitpage+$startpage;
				} else {
					$startnewpage = $startpage;
				}

				if ($pages > $startnewpage) {
					$endnewpage = $startnewpage + $limitpage;
					if ($endnewpage > $pages) {
						$endnewpage = $pages;
					}
				} 
				
				for ($i=$startnewpage;$i<=$endnewpage;$i++) { // loop thru
				    $newoffset=$limit*($i-1);
					if ($offset != $newoffset) {
						$link = $GLOBALS["self"]."?fa=$fa&startpage=$startpage&offset=$newoffset";
						print "<a href=\"".$link."\">$i</a> &nbsp; \n";
					}
					else 
					{
						print "<b>$i</b> &nbsp; ";
					}
					$lastoffset = $newoffset + $limit;
				}

				// check to see if last page
				if (!(($offset/$limit)>=$pages) && $pages!=1) {
				    // not last page so give NEXT link
				    $newoffset=$offset+$limit;
					if ($offset == ($startpage*$limit*$limitpage-$limit)) {
						$newpage = $startpage+1;
					} else {
						$newpage = $startpage;
					}
					if ($newoffset < $numrows) {
						$link = $GLOBALS["self"]."?fa=$fa&startpage=$newpage&offset=$newoffset";
						print "<a href=\"".$link."\">&gt;</a>\n";
					}
				}
				
				// check to see if last page
			
				if (!(($lastoffset)>=$numrows) && $pages!=1) {
				    // not last page so give NEXT link
				    $newoffset=$offset+$limit;
					$newpage=$startpage+1;
					if ($newoffset < $numrows) {
						$link = $GLOBALS["self"]."?fa=$fa&startpage=$newpage&offset=$lastoffset";
						print "<a href=\"".$link."\">&raquo;</a>\n";
					}
				}

}

function updatelastvisit ($userid){
	$sql =  "UPDATE tuser SET userlastvisit = now() WHERE userid = '$userid'";
	$usrquery = @mysql_query($sql) or die("[update last visit] Unable to execute query.");
	return;
}

function updatesession ($id, $userid, $ip){
	$sql = "SELECT session_id FROM tsessions WHERE session_id = '$id'";
	$rscheck = mysql_query($sql) or die ( '[Session 1] Unable to execute query.' );
	$num = mysql_numrows( $rscheck);
	
	if ( $num != 0 ) {
		$sql =  "UPDATE tsessions SET session_id = '$id', session_userid = '$userid', session_start = CURDATE(), ".
				"session_time = CURTIME(), session_ip = '$ip' WHERE session_id = '$id'";		
	} else {
		$sql =  "INSERT INTO tsessions VALUES ('$id', '$userid', CURDATE(), CURTIME(), '$ip')";
	}
	echo $sql;
	$sessionquery = @mysql_query($sql) or die("[Session 2] Unable to execute query.");
	return;
}

function indomonth($mysqldate) {
	$dumbmonth = substr($mysqldate,5,2);
	switch($dumbmonth) {
	case "01": $bulan = "Januari"; break;
	case "02": $bulan = "Pebruari"; break;
	case "03": $bulan = "Maret"; break;
	case "04": $bulan = "April"; break;
	case "05": $bulan = "Mei"; break;
	case "06": $bulan = "Juni"; break;
	case "07": $bulan = "Juli"; break;
	case "08": $bulan = "Agustus"; break;
	case "09": $bulan = "September"; break;
	case "10": $bulan = "Oktober"; break;
	case "11": $bulan = "Nopember"; break;
	case "12": $bulan = "Desember"; break;
	}
	if ($mysqldate == "0000-00-00") {
		return "";
	} else {
		return substr($mysqldate,8,2)." ".$bulan." ".substr($mysqldate,0,4);
	}
}

// start of dari andi
// tanggal yang dimasukkan harus benar
function indodate($mysqldate) {
	return substr($mysqldate,8,2)."-".substr($mysqldate,5,2)."-".substr($mysqldate,0,4);
}

function mysqldate($indodate) {
	return substr($indodate,6,4)."-".substr($indodate,3,2)."-".substr($indodate,0,2);
}

function indo_day_of_date($day, $month, $year) {
	$day_num = date("w",mktime(0,0,0,$month,$day,$year));
	switch($day_num) {
		case 0 :
			return "Minggu";
			break;
		case 1 :
			return "Senin";
			break;
		case 2 :
			return "Selasa";
			break;
		case 3 :
			return "Rabu";
			break;
		case 4 :
			return "Kamis";
			break;
		case 5 :
			return "Jumat";
			break;
		case 6 :
			return "Sabtu";
			break;
		default :
			return "undefined day";
			break;
	}
}

function umur_a($tgl) {
	$tahun_lahir = (int)substr($tgl,6,4);
	$bulan_lahir = (int)substr($tgl,3,2);
	$tahun_skrng = (int)date("Y");
	$bulan_skrng = (int)date("n");
	
	if(($bulan_skrng - $bulan_lahir) < 0) {
		return $tahun_skrng - $tahun_lahir - 1;
	} else {
		return $tahun_skrng - $tahun_lahir - 1;
	}
}

// end of dari andi
// dari lap SIMPEG
	function GetTanggal($date) {
		return $date["mday"];
	}

	function GetBulan($date) {
		switch ($date["mon"]) {
			case 1 : return "Januari"; break;
			case 2 : return "Februari"; break;
			case 3 : return "Maret"; break;
			case 4 : return "April"; break;
			case 5 : return "Mei"; break;
			case 6 : return "Juni"; break;
			case 7 : return "Juli"; break;
			case 8 : return "Agustus"; break;
			case 9 : return "September"; break;
			case 10 : return "Oktober"; break;
			case 11 : return "November"; break;
			case 12 : return "Desember";
		}
	}

	function ShowMonth($no) {
		switch ($no) {
			case 1 : return "Januari"; break;
			case 2 : return "Februari"; break;
			case 3 : return "Maret"; break;
			case 4 : return "April"; break;
			case 5 : return "Mei"; break;
			case 6 : return "Juni"; break;
			case 7 : return "Juli"; break;
			case 8 : return "Agustus"; break;
			case 9 : return "September"; break;
			case 10 : return "Oktober"; break;
			case 11 : return "November"; break;
			case 12 : return "Desember";
		}
	}
	function GetTahun($date) {
		return $date["year"];
	}
// end dari lap SIMPEG
// dari data induk SIMPEG

// New Function for Bandung.go.id
function AlertFormat($message) {
	return 	"<div align=\"center\"><table width=\"100%\" border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\" class=\"message\">".
  			"<tr><td width=\"5%\" align=\"center\"><img src=\"../images/ico_alert.png\" width=\"32\" height=\"32\"></td>".
    		"<td width=\"5%\" valign=\"center\">&nbsp;</td>".
			"<td width=\"90%\" valign=\"center\">$message</td></tr>".
			"</table></div>";
}

function indoformat($mysqldate) {
	$dumbmonth = substr($mysqldate,5,2);
	$dumbday   = date("w",$mysqldate);
	switch($dumbmonth) {
		case "01": $bulan = "Januari"; break;
		case "02": $bulan = "Februari"; break;
		case "03": $bulan = "Maret"; break;
		case "04": $bulan = "April"; break;
		case "05": $bulan = "Mei"; break;
		case "06": $bulan = "Juni"; break;
		case "07": $bulan = "Juli"; break;
		case "08": $bulan = "Agustus"; break;
		case "09": $bulan = "September"; break;
		case "10": $bulan = "Oktober"; break;
		case "11": $bulan = "November"; break;
		case "12": $bulan = "Desember"; break;
	}
	
	switch($dumbday) {
		case "0": $day = "Minggu"; break;
		case "1": $day = "Senin"; break;
		case "2": $day = "Selasa"; break;
		case "3": $day = "Rabu"; break;
		case "4": $day = "Kamis"; break;
		case "5": $day = "Jum'at"; break;
		case "6": $day = "Sabtu"; break;
	}
	if ($mysqldate == "0000-00-00") {
		return "";
	} else {
		return $day . ", ". substr($mysqldate,8,2)." ".$bulan." ".substr($mysqldate,0,4);
	}
}

function updatehits ($dbhostname,$dbusername,$dbpassword, $dbname, $tablename, $fieldname, $idname, $id){
	$connhits = &ADONewConnection($dbtype);
	$connhits->PConnect($dbhostname,$dbusername,$dbpassword, $dbname);
	$sql =  "UPDATE $tablename SET $fieldname = $fieldname + 1 WHERE $idname = $id";
	$rshits = $connhits->Execute($sql);
	return;
}

/*------ security if somebody want hack the site */
$search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                 "'<[\/\!]*?[^<>]*?>'si",           // Strip out html tags
                 "'([\r\n])[\s]+'",                 // Strip out white space
                 "'&(quot|#34);'i",                 // Replace html entities
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(\d+);'e");                    // evaluate as php

$replace = array ("",
                  "",
                  "\\1",
                  "\"",
                  "&",
                  "<",
                  ">",
                  " ",
                  chr(161),
                  chr(162),
                  chr(163),
                  chr(169),
                  "chr(\\1)");

// Example :
// $yourfield = preg_replace ($search, $replace, $yourfield);
?>
