$(document).ready(function(){
  // $("#loader").hide();
  $("#ulangi").hide();
  var kota = document.getElementById("kota");
  var kecamatan = document.getElementById("kecamatan");
  var desa = document.getElementById("desa");
  var butt = document.getElementById("generate_data");
      butt.disabled = false;
  
  $("#loader").hide();
  
  $("#propinsi").change(function(){

        var propinsi = $("#propinsi").val();
        //alert(propinsi);
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = propinsi; 
        // alert(nama_wilayah);
        if(propinsi!=''){
          $("#nama_prop").html("<b>Propinsi Jawa Barat</b>");
        }else{
          $("#nama_prop").text("");
          $("#nama_kota").text("");
          $("#nama_kec").text("");
          $("#nama_kel").text("");
        }

        $.ajax({
            url: "proses_kota.php",
            data: "propinsi=" + propinsi,
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kota").html(data);  
                $("#kecamatan").html("<option value=''>--Pilih Kecamatan--</option>");
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");                
            }
        });
  });

  $("#kota").change(function(){

        var kota = $("#kota").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kota;

        $.ajax({
            url: "nama_wilayah.php",
            data: "wil_id=" + kota,
            success: function(data){
                $("#nama_kota").html(data);
            }
        });

        if(kota=='--Pilih Kota--'){
          $("#nama_kota").text("");
          $("#nama_kec").text("");
          $("#nama_kel").text("");
        }

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kota +"&kode=Kecamatan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#kecamatan").html(data);
                $("#desa").html("<option value=''>--Pilih Kelurahan--</option>");
            }
        });
  });

  $("#kecamatan").change(function(){
    
        var kecamatan = $("#kecamatan").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = kecamatan;

        $.ajax({
            url: "nama_wilayah.php",
            data: "wil_id=" + kecamatan,
            success: function(data){
                $("#nama_kec").html(data);
            }
        });

        if(kecamatan=='--Pilih Kecamatan--'){
          $("#nama_kec").text("");
          $("#nama_kel").text("");
        }

        $.ajax({
            url: "proses_kecamatan.php",
            data: "wil_id=" + kecamatan + "&kode=Kelurahan",
            success: function(data){
                // jika data sukses diambil dari server, tampilkan di <select id=kota>
                $("#desa").html(data);
            }
        });
  });

  $("#desa").change(function(){
        var desa = $("#desa").val();
        var kode_wilayah = document.getElementById("kode_kel");
        kode_wilayah.value = desa;

        $.ajax({
            url: "nama_wilayah.php",
            data: "wil_id=" + desa,
            success: function(data){
                $("#nama_kel").html(data);
            }
        });

        $.ajax({
            url: "proses_kecamatan.php",
            //data: "wil_id=" + desa + "&kode=RW",
            success: function(data){

            }
        });
  });

});