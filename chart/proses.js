(function($) {
	// fungsi dijalankan setelah seluruh dokumen ditampilkan
	$(document).ready(function(e) {
		
		$("#generate_data").bind("click", function(event) {
			$("#loader").fadeIn();
			var url = "filter_wilayah.php";
			var kode_kel = $("#kode_kel").val();

			//alert(kode_kel);
			//exit();
			//var length = kd_fam.length;
			// kode_kel nilai id dari tombol hapus
			if(kode_kel=="" || kode_kel==null){
				alert("Kode Wilayah tidak boleh kosong...!");
				var error = document.getElementById("kode_kel");
					error.focus();
				$("#loader").fadeOut(100);
			}else{
			
				// tampilkan dialog konfirmasi
				var answer = confirm("Apakah anda ingin melakukan generate Piramida Kependudukan untuk kode wilayah  "+kode_kel+" ?");
				
				// // ketika ditekan tombol ok
				if (answer) {
					var butt = document.getElementById("generate_data");
						butt.disabled = true;


					//$.post(url, {kode_kel: kode_kel} ,function() {
						//reload data
						// location.reload();
						$("#chart").load("proses_piramida.php?kode_kel="+kode_kel+"").fadeIn("fast");

						$("#ulangi").fadeIn();
						$("#loader").fadeOut(30000);
						//location.reload();
					//});
				}else{
					$("#loader").fadeOut(100);	
				}
				//alert("Data KAK dan KKI pada wilayah "+kode_kel+" Berhasil Digenerate");
			}
			
		});

		$("#ulangi").bind("click", function(event) {
			// var butt = document.getElementById("generate_data");
			// 	butt.disabled = false;
			location.reload();
		});
		
	});
}) (jQuery);
