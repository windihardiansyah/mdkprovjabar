<?php

	function enol($nilai){
		$char = strlen($nilai);

		switch ($char) {
			case '1':
				$no_urutan = '000000'.$nilai;
				break;
			case '2':
				$no_urutan = '00000'.$nilai;
				break;
			case '3':
				$no_urutan = '0000'.$nilai;
				break;
			case '4':
				$no_urutan = '000'.$nilai;
				break;
			case '5':
				$no_urutan = '00'.$nilai;
				break;
			case '6':
				$no_urutan = '0'.$nilai;
				break;
			default:
				$no_urutan = $nilai;
				break;
		}

		return $no_urutan;
	}

?>
