<?php 
session_start();
include "../../koneksi/koneksi.inc.php"; 
if(!isset($_SESSION['un']))
   exit();
 
if($_GET['rt'])
{
   $nm="rt";   
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT RT";
}   
elseif($_GET['rw'])
{
   $nm="rw";   
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT DUSUN/RW";
}   
elseif($_GET['kel'])
{
   $nm="kel";   
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT DESA/KELURAHAN";
}   
elseif($_GET['kec'])
{
   $nm="kec";   
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT KECAMATAN";
}   
elseif($_GET['kab_kota'])
{
   $nm="kab_kota";
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT KABUPATEN/KOTA";
}   
elseif($_GET['prop'])
{
   $nm="prop";   
   $ket="JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN";
   $wl="TINGKAT PROVINSI";
}   
   
   

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Print Out</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
body{
	text-align: center;
}
form{
	width: 550px;
	margin-left: auto;
	margin-right: auto;
	border: solid 1px #000000;
	padding: 10px;
	font-size: 12px;
	font-family: Arial;
	text-align: left;
}

</style>
</head>

<body>
<br><br><br>
<div align="center">
		<font face="Arial, Helvetica, sans-serif" size="3" color="#000099"><B><?php echo $ket ?></B></font>
</div>
<div align="center">
		<font face="Arial, Helvetica, sans-serif" size="3" color="#990033"><B><?php echo $wl ?></B></font>
</div>
<br><br>
<div align="center">
		<font face="Arial, Helvetica, sans-serif" size="2"><B>Setting Halaman</B></font>
</div>
<br><br>
<form method="post" action="bkb_rekap_kb_umum_print_word.php">
	
		Orientation:
		<select name="ort" id="ort">
			<option value="portrait">Portrait</option>
			<option value="landscape">Landscape</option>
		</select>
		Type:
		
		<select name="type" id="type">
			<?php 
			  $qr_kts=mysql_query("SELECT * FROM dbo_kertas ORDER BY nm_kertas", $idmysql);
              while($rc_kts=mysql_fetch_array($qr_kts))
			  {
			?>
		         <option value="<?php echo $rc_kts[format_kertas]; ?>"><?php echo $rc_kts[nm_kertas]; ?> (<?php echo $rc_kts[ukuran_kertas]; ?>)</option>
			<?php
			  }
			?>
		</select><br>
		Top Margin:
		<input type="text" name="tm" id="tm" value="3.0" size="3">
		Right Margin:
		<input type="text" name="rm" id="rm" value="2.5" size="3">
		Bottom Margin:
		<input type="text" name="bm" id="bm" value="3.0" size="3">
		Left Margin:
		<input type="text" name="lm" id="lm" value="2.5" size="3">
		<br><br><br>
		<div align="center">
		<input type="submit" name="submit" value="ok">
		</div>
		<input type="hidden" name="<?php echo $nm; ?>" value="<?php echo htmlspecialchars($_GET[$nm]); ?>">

</form>
</body>
</html>