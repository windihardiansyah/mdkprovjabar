<?php
$d="../backup_data_bkkbn";
if(!is_dir($d))
   mkdir($d,0777);

if(is_dir($d))
{
   $d="../backup_data_bkkbn/backup_data_tabel";
   if(!is_dir($d))
      mkdir($d,0777);
}

$qr="CREATE TABLE IF NOT EXISTS dbo_tabel (".
  "id_tabel TINYINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,".
  "nm_tabel VARCHAR( 50 ) NOT NULL, ".
  "ket_tabel text NOT NULL".
  ") ENGINE = MYISAM ;";

  mysql_query($qr,$idmysql) OR die("EKSEKUSI ERROR"); 
    
  $qr="TRUNCATE dbo_tabel";
  mysql_query($qr,$idmysql) OR die("EKSEKUSI ERROR"); 
  
  
 $str="CREATE TABLE IF NOT EXISTS dbo_individu (".
  "Kd_indv varchar(30) NOT NULL,".
  "Kd_fam varchar(30) NOT NULL,".
  "Kd_neigh varchar(25) NOT NULL,".
  "Kd_fammbrtyp tinyint(4) NOT NULL,".
  "Nama varchar(100) NOT NULL,".
  "Kd_gen tinyint(4) NOT NULL,".
  "Tgl_lahir date NOT NULL,".
  "Tpt_lahir varchar(150) NOT NULL,".
  "Kd_edu tinyint(4) NOT NULL,".
  "Kd_martl tinyint(4) NOT NULL,".
  "Kd_emp tinyint(4) NOT NULL,".
  "Kd_mutasi tinyint(4) default NULL,".
  "Keterangan text,".
  "Posyandu enum(\'0\',\'1\') default NULL,".
  "Kd_unik_indv varchar(15) NOT NULL,".
  "PRIMARY KEY  (Kd_indv),".
  "KEY Kd_neigh (Kd_neigh),".
  "KEY Kd_edu (Kd_edu),".
  "KEY Kd_unik_indv (Kd_unik_indv),".
  "KEY Kd_emp (Kd_emp),".
  "KEY Kd_gen (Kd_gen),".
  "KEY Tgl_lahir (Tgl_lahir),".
  "KEY Kd_fammbrtyp (Kd_fammbrtyp),".
  "KEY Posyandu (Posyandu),".
  "KEY Kd_fam (Kd_fam),".
  "KEY Kd_fam_2 (Kd_fam,Kd_neigh,Kd_fammbrtyp,Kd_gen,Tgl_lahir,Kd_edu,Kd_martl,Kd_emp,Kd_mutasi,Posyandu,Kd_unik_indv),".
  "FULLTEXT KEY Nama (Nama)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";


   $kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_individu','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR");
		 
  $str="CREATE TABLE IF NOT EXISTS dbo_family (".
  "Kd_fam varchar(30) NOT NULL,".
  "Kd_prop varchar(25) NOT NULL,".
  "Kd_dist varchar(25) NOT NULL,".
  "Kd_subdist varchar(25) NOT NULL,".
  "Kd_vill varchar(25) NOT NULL,".
  "Kd_subvill varchar(25) NOT NULL,".
  "Kd_neigh varchar(25) NOT NULL,".
  "alamat varchar(256) NOT NULL,".
  "komplek varchar(256) default NULL,".
  "Kd_nonacptr tinyint(4) default NULL,".
  "Kd_prosplvl tinyint(4) default NULL,".
  "Kd_povrty tinyint(4) default NULL,".
  "Kd_consrc tinyint(4) default NULL,".
  "Kd_contyp tinyint(4) default NULL,".
  "Kd_conclass tinyint(4) default NULL,".
  "Kd_implan tinyint(4) default NULL,".
  "pus enum(\'0\',\'1\') NOT NULL default \'0\',".
  "tgl_kb varchar(10) default NULL,".
  "Bantuan_modal enum(\'0\',\'1\') NOT NULL default \'0\',".
  "Kd_unik_fam varchar(15) default NULL,".
  "PRIMARY KEY  (Kd_fam),".
  "KEY Kd_prop (Kd_prop,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_dist (Kd_dist,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_subdist (Kd_subdist,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_vill (Kd_vill,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_subvill (Kd_subvill,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_neigh (Kd_neigh,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,Bantuan_modal,Kd_unik_fam),".
  "KEY Kd_nonacptr (Kd_nonacptr),".
  "KEY Kd_prosplvl (Kd_prosplvl),".
  "KEY Kd_povrty (Kd_povrty),".
  "KEY Kd_consrc (Kd_consrc),".
  "KEY Kd_contyp (Kd_contyp),".
  "KEY Kd_implan (Kd_implan),".
  "KEY pus (pus),".
  "KEY Bantuan_modal (Bantuan_modal),".
  "KEY Kd_unik_fam (Kd_unik_fam),".
  "KEY Kd_prop_2 (Kd_prop),".
  "KEY Kd_dist_2 (Kd_dist),".
  "KEY Kd_subdist_2 (Kd_subdist),".
  "KEY Kd_vill_2 (Kd_vill),".
  "KEY Kd_subvill_2 (Kd_subvill),".
  "KEY Kd_neigh_2 (Kd_neigh),".
  "KEY tgl_kb (tgl_kb),".
  "KEY Kd_fam (Kd_fam,Kd_prop,Kd_dist,Kd_subdist,Kd_vill,Kd_subvill,Kd_neigh,Kd_nonacptr,Kd_prosplvl,Kd_povrty,Kd_consrc,Kd_contyp,Kd_implan,pus,tgl_kb,Bantuan_modal)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";
	 
    
	$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_family','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 
		 
  $str="CREATE TABLE IF NOT EXISTS dbo_fam_ind_detail ( ".
 " Kd_fam varchar(30) NOT NULL,".
 " id_ind_ks tinyint(4) NOT NULL,".
 " kd_prospinstat tinyint(4) NOT NULL,".
 " PRIMARY KEY  (Kd_fam,id_ind_ks),".
 " KEY Kd_fam (Kd_fam,id_ind_ks,kd_prospinstat)".
 ") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

  	$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_fam_ind_detail','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

  $str="CREATE TABLE IF NOT EXISTS dbo_contr_class (".
  "Kd_conclass varchar(1) NOT NULL,".
  "Nm_conclass_eng varchar(15) default NULL,".
  "Nm_conclass_ind varchar(15) default NULL,".
  "PRIMARY KEY  (Kd_conclass)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

  $kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_contr_class','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_kertas (".
  "id_kertas tinyint(4) NOT NULL auto_increment,".
  "nm_kertas varchar(20) NOT NULL,".
  "ukuran_kertas varchar(30) NOT NULL,".
  "format_kertas varchar(50) NOT NULL,".
  "PRIMARY KEY  (id_kertas)".
") ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;";
  
   $kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_kertas','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR");    
  
  
  $str="CREATE TABLE IF NOT EXISTS dbo_fam_typ (".
  "Kd_famtyp varchar(1) NOT NULL,".
  "Nm_famtyp_eng varchar(20) default NULL,".
  "Nm_famtyp_ind varchar(20) default NULL,".
  "PRIMARY KEY  (Kd_famtyp)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_fam_typ','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 
  
  
  $str="CREATE TABLE IF NOT EXISTS dbo_elco (".
  "Kd_elco varchar(30) NOT NULL,".
  "Nm_elco varchar(30) default NULL,".
  "PRIMARY KEY  (Kd_elco)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

  $kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_elco','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 
  
  $str="CREATE TABLE IF NOT EXISTS dbo_contr_src ( ".
  "Kd_consrc tinyint(3) unsigned NOT NULL,".
  "Nm_consrc_eng varchar(10) default NULL,".
  "Nm_consrc_ind varchar(10) default NULL,".
  "PRIMARY KEY  (Kd_consrc)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";		 	 

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_contr_src','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 
		 
$str="CREATE TABLE IF NOT EXISTS dbo_contr_typ (".
  "Kd_contyp tinyint(3) unsigned NOT NULL,".
  "kat tinyint(3) unsigned NOT NULL,".
  "Nm_contyp_eng varchar(30) default NULL,".
  "Nm_contyp_ind varchar(15) default NULL,".
  "Col004 varchar(1) default NULL,".
  "no tinyint(3) unsigned NOT NULL,".
  "singkatan varchar(10) NOT NULL,".
  "PRIMARY KEY  (Kd_contyp),".
  "KEY kat (kat),".
  "KEY no (no)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";		 

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_contr_typ','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR");
		 
$str="CREATE TABLE IF NOT EXISTS dbo_edu_lvl (".
  "Kd_edu tinyint(4) NOT NULL auto_increment,".
  "Nm_edu_eng varchar(20) default NULL,".
  "Nm_edu_ind varchar(50) default NULL,".
  "no_urut tinyint(4) default NULL,".
  "PRIMARY KEY  (Kd_edu)".
") ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;";		 

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_edu_lvl','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_empmnt_stat (".
  "Kd_emp tinyint(4) NOT NULL auto_increment,".
  "Nm_emp_eng varchar(20) default NULL,".
  "Nm_emp_ind varchar(20) default NULL,".
  "no_urut_empmnt tinyint(4) default NULL,".
  "PRIMARY KEY  (Kd_emp)".
") ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_empmnt_stat','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_fam_mbr_typ (".
  "Kd_fammbrtyp tinyint(4) NOT NULL auto_increment,".
  "Nm_fammbrtyp_eng varchar(15) default NULL,".
  "Nm_fammbrtyp_ind varchar(15) default NULL,".
  "PRIMARY KEY  (Kd_fammbrtyp)".
") ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_fam_mbr_typ','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_gender (".
  "Kd_gen tinyint(4) NOT NULL auto_increment,".
  "Nm_gen_eng varchar(10) default NULL,".
  "Nm_gen_ind varchar(10) default NULL,".
  "PRIMARY KEY  (Kd_gen)".
") ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_gender','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_implan (".
  "Kd_implan varchar(1) NOT NULL,".
  "Nm_implan_eng varchar(5) default NULL,".
  "Nm_implan_ind varchar(5) default NULL,".
  "PRIMARY KEY  (Kd_implan)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_implan','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_indikator_ks (".
  "id_ind_ks tinyint(4) NOT NULL auto_increment,".
  "nm_ind_ks text NOT NULL,".
  "no_ind_ks tinyint(4) default NULL,".
  "id_ind_ks_idk tinyint(4) default NULL,".
  "id_ind_ks_idk_1 tinyint(4) default NULL,".
  "PRIMARY KEY  (id_ind_ks),".
  "KEY id_ind_ks_idk (id_ind_ks_idk),".
  "KEY id_ind_ks_idk_1 (id_ind_ks_idk_1)".
") ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_indikator_ks','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_kat_validasi (".
  "Id_kat_val tinyint(3) unsigned NOT NULL auto_increment,".
  "Kd_prospinstat tinyint(3) unsigned NOT NULL,".
  "Id_ind_ks tinyint(3) unsigned NOT NULL,".
  "PRIMARY KEY  (Id_kat_val),".
  "KEY Kd_prospinstat (Kd_prospinstat),".
  "KEY Id_ind_ks (Id_ind_ks)".
") ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_kat_validasi','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_kategori (".
  "id_kat tinyint(4) NOT NULL auto_increment,".
  "id_ind_ks tinyint(4) NOT NULL,".
  "Kd_prospinstat tinyint(4) NOT NULL,".
  "Kd_povrty tinyint(4) NOT NULL,".
  "Kd_prosplvl tinyint(4) NOT NULL,".
  "PRIMARY KEY  (id_kat),".
  "KEY id_ind_ks (id_ind_ks)".
") ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_kategori','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_martl_stat (".
  "Kd_martl tinyint(4) NOT NULL auto_increment,".
  "Nm_martl_eng varchar(15) default NULL,".
  "Nm_martl_ind varchar(15) default NULL,".
  "PRIMARY KEY  (Kd_martl)".
") ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_martl_stat','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_menu (".
  "id_menu smallint(6) NOT NULL auto_increment,".
  "id_induk_menu smallint(6) default NULL,".
  "id_aplikasi tinyint(4) NOT NULL,".
  "link_menu varchar(250) default NULL,".
  "nm_menu varchar(60) NOT NULL,".
  "ket_menu text NOT NULL,".
  "no_urut_menu smallint(6) default NULL,".
  "tingkat tinyint(4) default NULL,".
  "PRIMARY KEY  (id_menu),".
  "KEY tingkat (tingkat)".
") ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_menu','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_mutasi (".
  "Kd_mutasi tinyint(4) NOT NULL auto_increment,".
  "Nm_mutasi varchar(255) NOT NULL,".
  "PRIMARY KEY  (Kd_mutasi)".
") ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_mutasi','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_non_acptr_reas (".
  "Kd_nonacptr tinyint(4) NOT NULL auto_increment,".
  "Nm_nonacptr_eng varchar(30) default NULL,".
  "Nm_nonacptr_ind varchar(30) default NULL,".
  "PRIMARY KEY  (Kd_nonacptr)".
") ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_non_acptr_reas','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_povrty_lvl (".
  "Kd_povrty varchar(1) NOT NULL,".
  "Nm_povrty_eng varchar(15) default NULL,".
  "Nm_povrty_ind varchar(15) default NULL,".
  "PRIMARY KEY  (Kd_povrty)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_povrty_lvl','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_prosp_ind (".
  "Kd_prospin varchar(2) NOT NULL,".
  "Nm_prospin_eng varchar(30) default NULL,".
  "Nm_prospin_ind varchar(80) default NULL,".
  "PRIMARY KEY  (Kd_prospin)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_prosp_ind','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_prosp_ind_stat (".
  "Kd_prospinstat tinyint(4) NOT NULL auto_increment,".
  "Nm_prospinstat_eng varchar(30) default NULL,".
  "Nm_prospinstat_ind varchar(30) default NULL,".
  "R_ProsPinStat varchar(2) default NULL,".
  "PRIMARY KEY  (Kd_prospinstat)".
") ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_prosp_ind_stat','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_prosp_lvl (".
  "Kd_prosplvl varchar(1) NOT NULL,".
  "Nm_prosplvl_eng varchar(50) default NULL,".
  "Nm_prosplvl_ind varchar(50) default NULL,".
  "no_urut tinyint(3) unsigned NOT NULL,".
  "PRIMARY KEY  (Kd_prosplvl),".
  "KEY no_urut (no_urut)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_prosp_lvl','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_typeuser (".
  "id_typeuser varchar(40) NOT NULL,".
  "nm_typeuser varchar(100) NOT NULL,".
  "ket_typeuser text,".
  "id_typeuser_idk varchar(40) default NULL,".
  "id_unik_typeuser varchar(25) NOT NULL,".
  "PRIMARY KEY  (id_typeuser),".
  "KEY id_typeuser_idk (id_typeuser_idk)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_typeuser','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_typeuser_menu (".
  "id_menu smallint(6) NOT NULL,".
  "id_typeuser varchar(40) NOT NULL,".
  "PRIMARY KEY  (id_menu,id_typeuser)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_typeuser_menu','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_typeuser_menu_akses (".
  "id_menu smallint(6) NOT NULL,".
  "id_typeuser varchar(40) NOT NULL,".
  "PRIMARY KEY  (id_menu,id_typeuser)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_typeuser_menu_akses','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_typeuser_unit_detail (".
  "id_typeuser varchar(40) NOT NULL,".
  "id_unit_detail varchar(25) NOT NULL,".
  "PRIMARY KEY  (id_typeuser,id_unit_detail)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_typeuser_unit_detail','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_unit (".
  "id_unit mediumint(9) NOT NULL auto_increment,".
  "nm_unit varchar(100) NOT NULL,".
  "id_unit_idk mediumint(9) default NULL,".
  "PRIMARY KEY  (id_unit)".
") ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_unit','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_unit_detail (".
  "id_unit_detail varchar(25) NOT NULL,".
  "nm_unit_detail varchar(100) NOT NULL,".
  "no_unit_detail varchar(50) NOT NULL,".
  "id_unit smallint(6) default NULL,".
  "id_unit_1 smallint(6) NOT NULL,".
  "id_unit_detail_idk varchar(25) default NULL,".
  "id_unik varchar(15) NOT NULL,".
  "PRIMARY KEY  (id_unit_detail),".
  "KEY id_unik (id_unik),".
  "KEY id_unit_detail_idk (id_unit_detail_idk),".
  "KEY id_unit (id_unit),".
  "KEY id_unit_1 (id_unit_1)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_unit_detail','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 

$str="CREATE TABLE IF NOT EXISTS dbo_user (".
  "username varchar(50) NOT NULL,".
  "id_typeuser varchar(25) NOT NULL,".
  "nama varchar(75) NOT NULL,".
  "password varchar(100) NOT NULL,".
  "email varchar(35) NOT NULL,".
  "telepon varchar(20) NOT NULL,".
  "tgl_end varchar(40) NOT NULL,".
  "status enum(\'1\',\'0\') NOT NULL default \'0\',".
  "aktif enum(\'1\',\'0\') NOT NULL default \'0\',".
  "PRIMARY KEY  (username)".
") ENGINE=MyISAM DEFAULT CHARSET=latin1;";

$kueri="INSERT INTO dbo_tabel " . 
	           "(nm_tabel,ket_tabel)" . 
		       "VALUES ('dbo_user','$str') ";
   	     mysql_query($kueri,$idmysql) OR die("EKSEKUSI ERROR"); 		
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</script><script src="prototype.js" type="text/javascript"></script>
</script><script src="ex_sql_d_1.js" type="text/javascript"></script>
	
</head>
<body>
<form>
<table width="80%" align="center" border="0">
<tr>
<td colspan="2" align="center">
<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000066"><b>T A B E L</b></font>  
</td>
<td width="60%" align="center">
<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000066"><b>S T A T U S</b></font>  
</td>
<td width="60%">
<a href="javascript:void(0)" onClick="cek_semua()">cek</a>&nbsp;&&nbsp;<a href="javascript:void(0)" onClick="uncek_semua()">uncek semua tabel</a>&nbsp;|&nbsp;<a href="javascript:void(0)" onClick="rst_s_t()">reset tabel backup</a>
</td>
<td>
<input type="button" value="BACKUP DATA" onClick="this.disabled=true; back_up()" id="bp">
</td>
</tr>
<tr>
<td colspan="5">&nbsp;

</td>
</tr>
<tr style="visibility:hidden ">
<td>
<input type="checkbox" id="id_0" value="0">  
</td>
<td>
<font face="Verdana, Arial, Helvetica, sans-serif" size="2">Semua Tabel</font>  
</td>
<td id="id_td_0">
            
</td>

</tr>
<tr>
<td colspan="5">
==========================
</td>
</tr>

<?php
     /*
     
     $qr=mysql_query("SELECT * FROM dbo_tabel ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	 while($rc=mysql_fetch_array($qr))
	 {
	 */
	 $i=1;
	 $qr=mysql_query("SHOW TABLES FROM $nama_db ", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
	 while($rc=mysql_fetch_row($qr))
	 {
?>
        <tr>
        <td>
            <input type="checkbox" id="id_<?php echo $i; ?>" value="<?php echo htmlspecialchars(trim($rc[0])); ?>">  
        </td>
		<td>
            <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><?php echo $rc[0]; ?></font>  
        </td>
		<td id="id_td_<?php echo $i; ?>">
            
        </td>
        </tr>
<?php
     $i++;
    }
?>
</table>
<input type="hidden" id="id_hdn_c" value="<?php echo $i; ?>">
<input type="hidden" id="idx_c" value="<?php echo $i; ?>">
<input type="hidden" id="nm_db" value="<?php echo htmlspecialchars($nama_db); ?>">
</form>
</body>
</html>



