<?php
function copyfile($source, $dest, $folderPermission=0755,$filePermission=0644)
{
    $result=false;
   
    if (is_file($source)) 
	{ 
        if(is_dir($dest)) 
		{ 
            if ($dest[strlen($dest)-1]!='/') 
                $__dest=$dest."/";
            $__dest .= basename($source);
        }
        else 
		{ 
            $__dest=$dest;
        }
        $result=copy($source, $__dest);
        chmod($__dest,$filePermission);
    }
    elseif(is_dir($source)) 
	{ 
        if(!is_dir($dest)) 
		{ 
            @mkdir($dest,$folderPermission);
            chmod($dest,$folderPermission);
        }
        if ($source[strlen($source)-1]!='/') 
            $source=$source."/";
        if ($dest[strlen($dest)-1]!='/') 
            $dest=$dest."/";

        
        $result = true;
        $dirHandle=opendir($source);
        while($file=readdir($dirHandle)) 
		{ 
            if($file!="." && $file!="..") 
			{
                $result=copyfile($source.$file, $dest.$file, $folderPermission, $filePermission);
            }
        }
        closedir($dirHandle);
    }
    else 
	{
        $result=false;
    }
    return $result;
}
	
	if(!copyfile("../../../mysql/data/$_GET[n]","../backup_file"))
	   echo 1;
	else
	{
	   chdir('..');
	   echo getcwd()."\backup_file";   
	}   
	
	

?>
