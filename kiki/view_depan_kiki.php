<?php
$pdf->Ln(80); 
$pdf->SetFont('Arial','',9);
$pdf->Cell(105,3,'',0,09,'L',0);
$pdf->Cell(32,3,'TANGGAL DICETAK ',0,0,'L',0); $pdf->Cell(15,3,': '.date("d-m-Y"),0,0,'L',0);
$pdf->Ln();
$pdf->Cell(105,3,'',0,0,'L',0);
$pdf->Cell(32,3,'WAKTU DICETAK ',0,0,'L',0); $pdf->Cell(15,3,': '.date("H:i:s"),0,0,'L',0);
$pdf->Ln(3.5);

$pdf->SetFont('Arial','',12);


$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(112,5,'DAFTAR KELUARGA DAN ANGGOTA KELUARGA','LTR',0,'C',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();


$pdf->Cell(46,5,'',0,0,'L',0);
$thn=date('Y');
$pdf->Cell(112,5,'TAHUN : '.$thn,'LBR',0,'C',0);
$pdf->Cell(46,5,'',0,0,'L',0);


$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'RT',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[rt],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);

$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'RW',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[rw],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();

$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'Desa/Kelurahan',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[kel],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'Kecamatan',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[kec],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'Kabupaten/Kota',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[kota],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','L',0,'L',0); $pdf->Cell(32,5,'Propinsi',0,0,'L',0);$pdf->Cell(75,5,': '.$rc_isi[prop],'R',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Ln();
$pdf->Cell(46,5,'',0,0,'L',0);
$pdf->Cell(5,5,'','LB',0,'L',0); $pdf->Cell(107,5,'Kode Pos','RB',0,'L',0);
$pdf->Cell(46,5,'',0,0,'L',0);


?>