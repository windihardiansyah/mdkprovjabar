<?php
$pdf->SetFont('Arial','',7.5);
$pdf->Cell(35,2.5,'Nama Kepala Keluarga',0,0,'L',1);
$pdf->Cell(100,2.5,': '.trim(strtoupper($rc_isi[Nama])),0,'C',1);
$pdf->Ln();
$pdf->Cell(35,2.5,'NO. KKI',0,0,'L',1);
$pdf->Cell(100,2.5,': '.$rc_isi[Kd_fam],0,'C',1);
$pdf->Ln();
$pdf->Cell(35,2.5,'Alamat Kepala Keluarga',0,0,'L',1);
$pdf->Cell(174,2.5,': RT:'.$rc_isi[rt].' RW:'.$rc_isi[rw].' Kel:'.$rc_isi[kel].' Kec:'.$rc_isi[kec],0,'L',1);
$pdf->Ln();
$pdf->Cell(35,2.5,'',0,0,'L',1);
$pdf->Cell(174,2.5,'  Kab/Kota:'.$rc_isi[kota].' prop:'.$rc_isi[prop],0,'L',1);


$pdf->Ln(10);
$pdf->SetFont('Arial','B',7.5);
$pdf->Cell(30,2.5,'1. Identitas Keluarga',0,0,'L',1);
$pdf->Ln(3.2);
$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(192,192,192);
$pdf->SetTextColor(0,0,0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(30,8,'Nomor KAK','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 30, $pdf->GetY() - $yH);
$pdf->Cell(30,$yH ,'Nama','LTRB',0,'C',0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->Cell(25,$yH ,'NIK','LTRB',0,'C',0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(20,4,'Hubungan dengan KK','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 20, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(16,4,'Usia Kawin Pertama','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 16, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(16,4,'Jenis Kelamin','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 16, $pdf->GetY() - $yH);
$pdf->Cell(16.5,8,'Tempat Lahir','LTRB',0,'C',0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(15,4,'Tanggal Lahir','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 15, $pdf->GetY() - $yH);
$pdf->Cell(15,$yH ,'Akta Lahir','LTRB',0,'C',0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->Cell(25,$yH ,'Pekerjaan','LTRB',0,'C',0);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(20,4,'Pendidikan Terakhir','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 20, $pdf->GetY() - $yH);
$x = $pdf->GetX();
$y = $pdf->GetY();
$y1 =$pdf->GetY();
$pdf->MultiCell(16.5,4,'Status Kawin','LTRB','C',0);
$y2 = $pdf->GetY();
$yH = $y2 - $y1;
$pdf->SetXY($x + 16.5, $pdf->GetY() - $yH);
$pdf->Cell(15,$yH ,'Mutasi','LTRB',0,'C',0);
$pdf->Cell(20,$yH ,'Tgl. Mutasi','LTRB',0,'C',0);
$pdf->Ln();
$pdf->SetFont('Arial','',6.75);
$pdf->SetWidths(array(30,30,25,20,16,16,16.5,15,15,25,20,16.5,15,20));


//daftar anggota keluarga
$qr_i_a_k=mysql_query("SELECT * FROM dbo_individu WHERE Kd_fam='$rc_isi[Kd_fam]' ORDER BY Kd_fammbrtyp, Kd_indv", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
$jdindv=mysql_num_rows($qr_i_a_k);
$ikut_pos='T'; $blt='T'; $rmj='T'; $lns='T'; $wus=0;
while($rc_i_a_k=mysql_fetch_array($qr_i_a_k))
{
  $arr1=$rc_i_a_k[Kd_indv];
  $arr2=strtoupper($rc_i_a_k[Nama]);
  $arrnik=$rc_i_a_k[nik];
  $arr12=$rc_i_a_k[Tpt_lahir];
  
              $d=date('j'); $m=date('n'); $y=date('Y'); 
              if($d<10)
                  $d="0".$d;
              if($m<10)
                 $m="0".$m;  

              $u_skrg=$y.$m.$d;
			  $uq_skrg=$y."-".$m."-".$d;	  
			  
			  $y_16=$y-16; $y_24=$y-25;
              $u_16=$y_16.$m.$d;	  
              $u_24=$y_24.$m.$d;	  
			  
			  $y_15=$y-15; $y_49=$y-50;
              $u_15=$y_15.$m.$d;	  
              $u_49=$y_49.$m.$d;	  
			  
			  $y_60=$y-60;
			  $u_60=$y_60.$m.$d;	
			  
			         $t=explode("-",$rc_i_a_k[Tgl_lahir]);
                     $th=$t[0].$t[1].$t[2]; 
					 
					 if((strcmp($th,$u_49)>0 AND strcmp($th,$u_15)<=0) AND $rc_i_a_k[Kd_gen]==2) 
				     {
					    $wus++;
					 }
					 if(strcmp($th,$u_24)>0 AND strcmp($th,$u_16)<=0) 
				     {
					    $rmj='Y';
					 }
					 elseif(strcmp($th,$u_60)<=0) 
				     {
					    $lns='Y';
 				     }
  
  $qr=mysql_query("SELECT * FROM dbo_fam_mbr_typ WHERE Kd_fammbrtyp=$rc_i_a_k[Kd_fammbrtyp]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));

  $rc=mysql_fetch_array($qr);
  $arr3=$rc[Nm_fammbrtyp_ind];

  $arrukp=$rc_i_a_k[usiakawin];
 
  $qr=mysql_query("SELECT * FROM dbo_gender WHERE Kd_gen=$rc_i_a_k[Kd_gen]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr4=$rc[Nm_gen_ind];
  $tgl_spl=explode("-",$rc_i_a_k[Tgl_lahir]);
  $arr5=$tgl_spl[2]."-".$tgl_spl[1]."-".$tgl_spl[0];

  $tgl_mutasi=explode("-",$rc_i_a_k[tgl_mutasi]);
  $arrTglMutasi=$tgl_mutasi[2]."-".$tgl_mutasi[1]."-".$tgl_mutasi[0];

  $qr=mysql_query("SELECT * FROM dbo_empmnt_stat WHERE Kd_emp=$rc_i_a_k[Kd_emp]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr6=$rc[Nm_emp_ind];

  $qr=mysql_query("SELECT * FROM dbo_yesno WHERE Kd_yesno=$rc_i_a_k[aktalahir]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arrakta=$rc[Nm_yesno_ind];

  $qr=mysql_query("SELECT * FROM dbo_edu_lvl WHERE Kd_edu=$rc_i_a_k[Kd_edu]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  if($rc[Kd_edu]==0)
     $arr7='Tdk/Blm Sekolah';
  else	    
     $arr7=$rc[Nm_edu_ind];

  $qr=mysql_query("SELECT * FROM dbo_martl_stat WHERE Kd_martl=$rc_i_a_k[Kd_martl]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr8=$rc[Nm_martl_ind];
  if($rc_i_a_k[Kd_mutasi]!=NULL)
  {
  $qr=mysql_query("SELECT * FROM dbo_mutasi WHERE Kd_mutasi=$rc_i_a_k[Kd_mutasi]", $idmysql) or die("Gagal Mengeksekusi Query, ". mysql_error($idmysql));
  $rc=mysql_fetch_array($qr);
  $arr10=$rc[Nm_mutasi];
  }
  else
  $arr10="";
  
  $tgl_spl=explode("-",$rc_i_a_k[Tgl_lahir]);

  
  $t=date('j');
  $b=date('n');
  $y=date('Y');
  
  if($tgl_spl[0]>($y-5))
  {
     if($rc_i_a_k[Posyandu]=='1') 
	 {     
	    
		$ikut_pos='Y';
	 }	
	 
	    
  $blt='Y';		
  }
  else if($tgl_spl[0]==($y-5))
  {
     if(intval($tgl_spl[1])>$b)
	 {
	    if($rc_i_a_k[Posyandu]=='1')      
	    {	
	
		   $ikut_pos='Y';
		}   
	
		$blt='Y';   
	 }
	 else if(intval($tgl_spl[1])==$b)
	 {
	     if(intval($tgl_spl[2])>$t)
	     {
	        if($rc_i_a_k[Posyandu]=='1')      
			{
	
			 $ikut_pos='Y';
			} 
	       else if($rc_i_a_k[Posyandu]=='0')      
	
		   $blt='Y';	 
		 }
		
	 }
	
	    
  }
  
	 
  
  
  

 

	$pdf->Row(array(trim($arr1),trim($arr2),$arrnik,$arr3,$arrukp,$arr4,trim($arr12),$arr5,$arrakta,$arr6,$arr7,$arr8,$arr10,$arrTglMutasi));
	$pdf->Row(array('','','','','','','','','','','','','',''));
}
	$pdf->Row(array('','','','','','','','','','','','','',''));
	$pdf->Row(array('','','','','','','','','','','','','',''));
	$pdf->Row(array('','','','','','','','','','','','','',''));

?>