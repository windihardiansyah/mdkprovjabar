function print_doc_spec(rt)
{
      var urlb="bkb_rekap_kb_umum_form_spec_print.php?rt="+rt+"&r="+new Date().getTime();
	  var x=600; var y=350;
	
	messageObj.setSource(urlb);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(x,y);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
	return false;
}

function print_doc(rt)
{
      var urlb="bkb_rekap_kb_umum_form_print.php?rt="+rt+"&r="+new Date().getTime();
	  var x=600; var y=350;
	
	messageObj.setSource(urlb);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(x,y);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
	return false;
}


function view_data_edit(kki,idx_tr,wl,hs,t)
{
		var wl = encodeURIComponent(wl);

		if(wl!='')
		    ajax_showTooltip('menu_ajax_edit.php?wl='+wl+'&kki='+kki+'&idx_tr='+idx_tr+'&hs='+hs+'&r='+new Date().getTime(),t,1200);
		else
		    ajax_showTooltip('menu_ajax_edit.php?kki='+kki+'&idx_tr='+idx_tr+'&hs='+hs+'&r='+new Date().getTime(),t,1200)
		
}

function view_data_keluarga_d(id)
{
	var url='view_data_keluarga_d.php?id='+id+'&r='+new Date().getTime();
    messageObj.setSource(url);
	messageObj.setCssClassMessageBox(false);
	messageObj.setSize(1000,800);
	messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
	messageObj.display();
	return false;
}

function cek_semua()
{
	ajax_hideTooltip();
	for(var i=1; i<$('hd_c_h_kki').value; i++)
	{
	    if($('tr_c_kki_'+i).style.display=='')
		{
		    if($('c_h_kki_'+i) && $('c_h_kki_'+i).checked==false)
	           $('c_h_kki_'+i).checked=true;
		}
		else if($('c_h_kki_'+i))
	        $('c_h_kki_'+i).checked=false;
		      
    }
	$('hps_c').style.display='';
}

function uncek_semua()
{
	ajax_hideTooltip();
	for(var i=1; i<$('hd_c_h_kki').value; i++)
	{
		
		if($('tr_c_kki_'+i).style.display=='')
		{
		    if($('c_h_kki_'+i) && $('c_h_kki_'+i).checked==true)
	            $('c_h_kki_'+i).checked=false;
		}
		else if($('c_h_kki_'+i))
	        $('c_h_kki_'+i).checked=false;
    }
	$('hps_c').style.display='none';
}

function cek_kk(t)
{
  ajax_hideTooltip();
  var ada=false;
  if(t.checked==false)
  {
    for(var i=1; i<$('hd_c_h_kki').value; i++)
	{
		if($('c_h_kki_'+i) && $('c_h_kki_'+i).checked==true)
		{
			ada=true;
			break;
		}
    }
	if(!ada)
      $('hps_c').style.display='none';
  }
  else
  {
	     if($('hps_c').style.display=='none')
		    $('hps_c').style.display='';
  }
}

function hapus_sub_c_kki(kki,hs,s)
{
	   ajax_hideTooltip();
	   if(confirm('Yakin Ingin Menghapus Data Nomor KKI '+kki+' ?'))
	       hapus_sub_c_kki_p(kki,hs,s)
}

function hapus_sub_c_kki_p(kki,hs,s)
{
   			 cemangtampilmodal("<br><IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses Menghapus Nomor KKI "+kki+" ...<B>");
			 var urlb="hapus_data_keluarga.php?kki="+kki+"&r="+new Date().getTime();
             new Ajax.Request(
             urlb,
             {
               method:"get",
			   onSuccess: function (r) {
           								   messageObj.close();
										   if($('hd_c_h_kki').value==2)
                                                hs=hs-1;
										   link_g(hs,s);
			                           },
               onFailure: function (r) { alert("Gagal Menghapus Nomor KKI "+kki); }
             }
            );    

}

function hapus_c_kki(x,hs,s)
{
	   ajax_hideTooltip();
	   var j=1;
	   for(var i=1; i<$('hd_c_h_kki').value; i++)
	   {
		  if($('c_h_kki_'+i) && $('c_h_kki_'+i).checked==true)
		  {
			 if(j==1)
			    var sc=$('c_h_kki_'+i).value;
		     else		
			    sc+='\n'+$('c_h_kki_'+i).value;
			 j++;	
		  }
       }
	   
	   if(confirm('Yakin Ingin Menghapus Data KKI Yang Dicek ?\n'+sc))
	       hapus_c_kki_p(x,hs,s,0); 
}

function hapus_c_kki_p(x,hs,s,jdt)
{
    var ada=false; 
	for(var i=x; i<$('hd_c_h_kki').value; i++)
	{
	   if($('c_h_kki_'+i) && $('c_h_kki_'+i).checked==true)
	   {
			ada=true; jdt=jdt+1;
			var k=i;
			break;
		}
    }
	if(!ada)
	{
	   messageObj.close();
	   if($('hd_c_h_kki').value==jdt+1)
           hs=hs-1;
	   link_g(hs,s);
	   return false;
	}
	   
			 var kki=$('c_h_kki_'+k).value;
			 
			 cemangtampilmodal("<br><IMG src='../images/spinner.gif'>&nbsp;&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><B>Silahkan Tunggu, Sedang Proses Menghapus Nomor KKI "+kki+" ...<B>");
			 var urlb="hapus_data_keluarga.php?kki="+kki+"&r="+new Date().getTime();
             new Ajax.Request(
             urlb,
             {
               method:"get",
			   onSuccess: function (r) {
				                           $('cem_g').removeChild($('tr_c_kki_'+k));
										   messageObj.close();
										   hapus_c_kki_p(k+1,hs,s,jdt);
										   
			                           },
               onFailure: function (r) { alert("Gagal Menghapus Nomor KKI "+kki); }
             }
            );    

}

