USE bkkbn_sukabumi;
DROP TABLE dbo_typeuser;

CREATE TABLE `dbo_typeuser` (
  `id_typeuser` varchar(40) NOT NULL,
  `nm_typeuser` varchar(100) NOT NULL,
  `ket_typeuser` text,
  `id_typeuser_idk` varchar(40) DEFAULT NULL,
  `id_unik_typeuser` varchar(25) NOT NULL,
  PRIMARY KEY (`id_typeuser`),
  KEY `id_typeuser_idk` (`id_typeuser_idk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO dbo_typeuser VALUES("0000","Admin BKKBD","Admin BKKBN Pusat","","000");