<?php
	include "../brt_main_cms_1.php";
?>

<form enctype="multipart/form-data" action="recovery_data.php" method="post">
	<p align="center"><em><strong>RESTORE / IMPORT DATABASE </strong></em></p>
	<table align="center">
	<tr><td>File Backup Database (*.sql) <input type="file" name="datafile" size="30" id="gambar" /></td></tr>
	<tr><td><input type="submit" onclick="return confirm('Apakah Anda yakin akan restore database?')" name="restore" value="Restore Database" /></td>
	</tr>
	</table>
</form>


<?php

if(isset($_POST['restore'])){
	//$koneksi=mysql_connect("localhost","root","");

	include "../koneksi/koneksi.inc.php"; 
	
	$nama_file=$_FILES['datafile']['name'];
	$ukuran=$_FILES['datafile']['size'];
	
	//periksa jika data yang dimasukan belum lengkap
	if ($nama_file=="")
	{
		echo "Fatal Error";
	}else{
		//definisikan variabel file dan alamat file
		$uploaddir='./restore/';
		$alamatfile=$uploaddir.$nama_file;

		//periksa jika proses upload berjalan sukses
		if (move_uploaded_file($_FILES['datafile']['tmp_name'],$alamatfile))
		{

			$filename = './restore/'.$nama_file.'';
			
			// Temporary variable, used to store current query
			$templine = '';
			// Read in entire file
			$lines = file($filename);
			// Loop through each line
			foreach ($lines as $line)
			{
				// Skip it if it's a comment
				if (substr($line, 0, 2) == '--' || $line == '')
					continue;
			 
				// Add this line to the current segment
				$templine .= $line;
				// If it has a semicolon at the end, it's the end of the query
				if (substr(trim($line), -1, 1) == ';')
				{
					// Perform the query
					mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
					// Reset temp variable to empty
					$templine = '';
				}
			}
			echo "<center>Berhasil Restore Database, silahkan di cek.</center>";

			//query dari tabel migrasi
			$migrasi_family = mysql_query("SELECT Kd_fam FROM dbo_family_migrasi ORDER BY Kd_fam");
			while($data = mysql_fetch_array($migrasi_family)){
				$kd_fam_migrasi = $data['Kd_fam'];

				$cek = mysql_fetch_array(mysql_query("SELECT a.Kd_fam, COUNT(b.Kd_fam) as jml_angkel FROM dbo_family a LEFT JOIN dbo_individu b ON a.Kd_fam=b.Kd_fam WHERE a.Kd_fam='$kd_fam_migrasi'"));
				$hasil_cek = $cek['jml_angkel'];
				if($hasil_cek==0){
					mysql_query("INSERT INTO dbo_family SELECT * FROM dbo_family_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
					mysql_query("INSERT INTO dbo_individu SELECT * FROM dbo_individu_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
					mysql_query("INSERT INTO dbo_fam_ind_detail SELECT * FROM dbo_fam_ind_detail_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
				}else{
					$jml_angkel = $cek['jml_angkel'];
					$cek_migrasi = mysql_fetch_array(mysql_query("SELECT a.Kd_fam, COUNT(b.Kd_fam) as jml_angkel FROM dbo_family_migrasi a LEFT JOIN dbo_individu_migrasi b ON a.Kd_fam=b.Kd_fam WHERE a.Kd_fam='$kd_fam_migrasi'"));	
					$jml_angkel_migrasi = $cek_migrasi['jml_angkel'];

					if($jml_angkel_migrasi>$jml_angkel){

						mysql_query("DELETE FROM dbo_family WHERE Kd_fam='$kd_fam_migrasi'");
						mysql_query("DELETE FROM dbo_individu WHERE Kd_fam='$kd_fam_migrasi'");
						mysql_query("DELETE FROM dbo_fam_ind_detail WHERE Kd_fam='$kd_fam_migrasi'");

						mysql_query("INSERT INTO dbo_family SELECT * FROM dbo_family_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
						mysql_query("INSERT INTO dbo_individu SELECT * FROM dbo_individu_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
						mysql_query("INSERT INTO dbo_fam_ind_detail SELECT * FROM dbo_fam_ind_detail_migrasi WHERE Kd_fam='$kd_fam_migrasi'");
					}
				}

			}

			//================== cek apabila ada duplikat data ==============================
			$cekdouble = mysql_query("SELECT a.nama, b.alamat, a.Tgl_lahir, a.Kd_fammbrtyp, a.Tpt_lahir, a.Kd_edu, a.Kd_emp, a.Kd_fam FROM dbo_individu a LEFT JOIN dbo_family b ON a.Kd_fam=b.Kd_fam WHERE a.Kd_fammbrtyp=1 GROUP BY a.nama, b.alamat, a.Tgl_lahir, a.Kd_fammbrtyp, a.Tpt_lahir, a.Kd_edu, a.Kd_emp HAVING   COUNT(*) > 1");

			while($data = mysql_fetch_array($cekdouble)){
				$Kd_fam = $data['Kd_fam'];
				mysql_query("DELETE FROM dbo_fam_ind_detail where Kd_fam='$Kd_fam' ");
				mysql_query("DELETE FROM dbo_individu where Kd_fam='$Kd_fam' ");
				mysql_query("DELETE FROM dbo_family where Kd_fam='$Kd_fam' ");
			}

			mysql_query("update dbo_individu SET bkb=null where bkb=0");
			mysql_query("update dbo_individu SET bkr=NULL where bkr=0");
			mysql_query("update dbo_individu SET bkl=NULL where bkl=0");
			mysql_query("update dbo_individu SET pik=NULL where  pik=0");
			mysql_query("update dbo_individu SET kd_mutasi=NULL where kd_mutasi=0");

			mysql_query("update dbo_family SET kd_povrty=null where kd_povrty=0");
			mysql_query("update dbo_family SET kd_consrc=NULL where Kd_consrc=0");
			mysql_query("update dbo_family SET Kd_contyp=null where Kd_contyp=0");
			mysql_query("update dbo_family SET Kd_conclass=null where Kd_conclass=0");
			mysql_query("update dbo_family SET Kd_implan=null where Kd_implan=0");
			mysql_query("update dbo_family SET apbn=null where apbn=0");
			mysql_query("update dbo_family SET apbd=null where apbd=0");
			mysql_query("update dbo_family SET krista=null where krista=0");
			mysql_query("update dbo_family SET kur=null where kur=0");
			mysql_query("update dbo_family SET lainnya=null where lainnya=0");
			mysql_query("update dbo_family SET bkb=null where bkb=0");
			mysql_query("update dbo_family SET bkr=NULL where bkr=0");
			mysql_query("update dbo_family SET bkl=NULL where bkl=0");
			mysql_query("update dbo_family SET pik=NULL where  pik=0");
			mysql_query("update dbo_family SET Kd_nonacptr=NULL where Kd_nonacptr=0");		
		}else{
			//jika gagal
			echo "Proses upload gagal, kode error = " . $_FILES['location']['error'];
		}	
	}

}else{
	unset($_POST['restore']);
}
?>

</body>
</head>

	