USE bkkbn_sukabumi;
DROP TABLE dbo_menu;

CREATE TABLE `dbo_menu` (
  `id_menu` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_induk_menu` smallint(6) DEFAULT NULL,
  `id_aplikasi` tinyint(4) NOT NULL,
  `link_menu` varchar(250) DEFAULT NULL,
  `nm_menu` varchar(300) NOT NULL,
  `ket_menu` text NOT NULL,
  `no_urut_menu` smallint(6) DEFAULT NULL,
  `tingkat` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `tingkat` (`tingkat`)
) ENGINE=MyISAM AUTO_INCREMENT=294 DEFAULT CHARSET=latin1;

INSERT INTO dbo_menu VALUES("76","65","3","menu/menu_pilih.php","Menu","Menu","6","");
INSERT INTO dbo_menu VALUES("112","106","0","rekap_1/rekap_rt.php","Report General","Report General","5","");
INSERT INTO dbo_menu VALUES("50","","3","logout.php","Logout","Logout","7","");
INSERT INTO dbo_menu VALUES("65","","3","","Setup","Setup","2","");
INSERT INTO dbo_menu VALUES("91","65","0","indikator_ks/indikator.php","Indikator KS","Indikator KS","2","");
INSERT INTO dbo_menu VALUES("106","","0","","Report ","Report","4","");
INSERT INTO dbo_menu VALUES("95","","0","","Data","Data","2","");
INSERT INTO dbo_menu VALUES("102","95","0","form_keluarga/hapus_data_keluarga.php","Penghapusan Data Keluarga","Penghapusan Data Keluarga","3","");
INSERT INTO dbo_menu VALUES("101","95","0","form_keluarga/form_edit.php","Perubahan Data Keluarga","Perubahan Data Keluarga","2","");
INSERT INTO dbo_menu VALUES("100","95","0","form_keluarga/menu_ajax.php","Penambahan Data  Keluarga Baru","Tambah Keluarga Baru","1","");
INSERT INTO dbo_menu VALUES("162","106","0","rekap_data_keluarga/rekap_rt.php","Report Keluaran - Output MDK","Report Data Keluarga Pusat","11","");
INSERT INTO dbo_menu VALUES("161","65","0","area_induk_test_tree_ada_link_area_refresh_all_tampil_all/area.php","Area-wilayah","","1","");
INSERT INTO dbo_menu VALUES("163","","0","","Rekapitulasi ","Rekapitulasi","5","");
INSERT INTO dbo_menu VALUES("164","163","0","","KS","KS","1","");
INSERT INTO dbo_menu VALUES("165","164","0","report_ks/rekap_rt.php","PRA S Dan KS I","PRA S & KS I","1","");
INSERT INTO dbo_menu VALUES("166","164","0","report_ks_kb/rekap_rt.php","PRA KS Dan KS I KB","PRA KS & KS I KB","2","");
INSERT INTO dbo_menu VALUES("167","164","0","report_kb/rekap_rt.php","KB Detail","KB Detail","3","");
INSERT INTO dbo_menu VALUES("168","163","0","","KB","KB","2","");
INSERT INTO dbo_menu VALUES("169","168","0","report_kb_kontrasepsi/rekap_rt.php","Mix Kontrasepsi","Mix Kontrasepsi","1","");
INSERT INTO dbo_menu VALUES("170","106","0","rekap_register_data_keluarga/rekap_rt.php","Register Data Keluarga R1 KS - R I PUS - R II PUS","Register Data Keluarga","5","");
INSERT INTO dbo_menu VALUES("171","95","0","cari_individu/rekap_rt.php","Cari individu","Cari individu","4","");
INSERT INTO dbo_menu VALUES("172","163","0","","Demografi","Demografi","3","");
INSERT INTO dbo_menu VALUES("173","172","0","report_demografi/rekap_rt.php","Umur","Umur","1","");
INSERT INTO dbo_menu VALUES("174","172","0","report_demografi_krj/rekap_rt.php","Status Kerja Dan Umur","","2","");
INSERT INTO dbo_menu VALUES("175","172","0","report_demografi_skl/rekap_rt.php","Status Usia Sekolah","","3","");
INSERT INTO dbo_menu VALUES("176","163","0","report_non_kb_alsn/rekap_rt.php","Non KB per Alasan","","4","");
INSERT INTO dbo_menu VALUES("177","163","0","report_tahapan/rekap_rt.php","Tahapan KS","","5","");
INSERT INTO dbo_menu VALUES("178","65","0","area_typeuser_gabung_test_tree_ada_link_area_refresh_all_tampil_all/area.php","Typeuser-Menuakses-User","","8","");
INSERT INTO dbo_menu VALUES("179","172","0","report_jumlah_kk_jiwa/rekap_rt.php","Jumlah KK dan Jiwa","","0","");
INSERT INTO dbo_menu VALUES("221","220","0","pelaporan/tabel_1/rekap_rt.php","1. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN ","JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR DAN JENIS PEKERJAAN","1","");
INSERT INTO dbo_menu VALUES("222","220","0","pelaporan/tabel_2/rekap_rt.php","2. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR JENIS KELAMIN DAN BERSTATUS BEKERJA ","","2","");
INSERT INTO dbo_menu VALUES("223","220","0","pelaporan/tabel_3/rekap_rt.php","3. JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA ","JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA","3","");
INSERT INTO dbo_menu VALUES("224","220","0","pelaporan/tabel_4/rekap_rt.php","4. JUMLAH PESERTA KB MENURUT KELOMPOK UMUR METODE KONTRSEPSI DAN JUMLAH ANAK ","JUMLAH PESERTA KB MENURUT KELOMPOK UMUR METODE KONTRSEPSI DAN JUMLAH ANAK","4","");
INSERT INTO dbo_menu VALUES("225","220","0","pelaporan/tabel_5/rekap_rt.php","5. JUMLAH PUS DAN PUS PESERTA KB, PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR DAN ALASAN TIDAK BER-KB ","JUMLAH PUS DAN PUS PESERTA KB, PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR DAN ALASAN TIDAK BER-KB","5","");
INSERT INTO dbo_menu VALUES("226","220","0","pelaporan/tabel_6/rekap_rt.php","6. JUMLAH JIWA MENURUT KELOMPOK UMUR DAN JENIS KELAMIN ","JUMLAH JIWA MENURUT KELOMPOK UMUR DAN JENIS KELAMIN","6","");
INSERT INTO dbo_menu VALUES("227","220","0","pelaporan/tabel_7/rekap_rt.php","7. JUMLAH PUS DAN PUS PESERTA KB USIA 45-49 ","JUMLAH PUS DAN PUS PESERTA KB USIA 45-49","7","");
INSERT INTO dbo_menu VALUES("228","220","0","pelaporan/tabel_8/rekap_rt.php","8. JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, DAN STATUS BERSEKOLAH ","JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH","8","");
INSERT INTO dbo_menu VALUES("229","220","0","pelaporan/tabel_9/rekap_rt.php","9. JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH","JUMLAH JIWA USIA WAJIB BELAJAR MENURUT MENURUT KELOMPOK UMUR, JENIS KELAMIN DAN STATUS BERSEKOLAH","9","");
INSERT INTO dbo_menu VALUES("230","220","0","pelaporan/tabel_10/rekap_rt.php","10. JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PENDIDIKAN","JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PENDIDIKAN","10","");
INSERT INTO dbo_menu VALUES("231","220","0","pelaporan/tabel_11/rekap_rt.php","11. JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PEKERJAAN","JUMLAH JIWA MENURUT MENURUT KELOMPOK UMUR DAN PEKERJAAN","11","");
INSERT INTO dbo_menu VALUES("232","220","0","pelaporan/tabel_12/rekap_rt.php","12. JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR, DAN STATUS TAHAPAN KELUARGA SEJAHTERA","JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR, DAN STATUS TAHAPAN KELUARGA SEJAHTERA","12","");
INSERT INTO dbo_menu VALUES("233","220","0","pelaporan/tabel_13/rekap_rt.php","13. JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR,  KELAMIN, STATUS BEKERJA DAN TAHAPAN KELUARGA SEJAHTERA","JUMLAH JIWA USIA KERJA MENURUT KELOMPOK UMUR,  KELAMIN, STATUS BEKERJA DAN TAHAPAN KELUARGA SEJAHTERA","13","");
INSERT INTO dbo_menu VALUES("234","220","0","pelaporan/tabel_14/rekap_rt.php","14. JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR,   STATUS TAHAPAN KELUARGA SEJAHTERA DAN PENDIDIKAN","JUMLAH KEPALA KELUARGA MENURUT KELOMPOK UMUR,   STATUS TAHAPAN KELUARGA SEJAHTERA DAN PENDIDIKAN","14","");
INSERT INTO dbo_menu VALUES("235","220","0","pelaporan/tabel_15/rekap_rt.php","15. JUMLAH KELUARGA DENGAN ANAK BALITA MENURUT MENURUT KELOMPOK UMUR,  JUMLAH BALITA, KEGIATAN POSYANDU DAN STATUS THP KS","JUMLAH KELUARGA DENGAN ANAK BALITA MENURUT MENURUT KELOMPOK UMUR,  JUMLAH BALITA, KEGIATAN POSYANDU DAN STATUS THP KS","15","");
INSERT INTO dbo_menu VALUES("236","220","0","pelaporan/tabel_16/rekap_rt.php","16. JUMLAH PUS  MENURUT KELOMPOK UMUR, KESERTAAN BER-KB, TEMPAT PELAYANAN DAN TAHAPAN KELUARGA SEJAHTERA","JUMLAH PUS  MENURUT KELOMPOK UMUR, KESERTAAN BER-KB, TEMPAT PELAYANAN DAN TAHAPAN KELUARGA SEJAHTERA","16","");
INSERT INTO dbo_menu VALUES("254","219","0","","Tabel Status Kerja Pertahapan KS","Tabel Status Kerja Kepala Keluarga","3","");
INSERT INTO dbo_menu VALUES("250","240","0","pelaporan/report_temanggung_ks_tabel_3/rekap_rt.php","3. JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - KECIL 5 TH DAN PROXY CWR","JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - < 5 TH DAN PROXY CWR","3","");
INSERT INTO dbo_menu VALUES("248","240","0","pelaporan/report_temanggung_ks_tabel_1/rekap_rt.php","1. JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN","JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN","1","");
INSERT INTO dbo_menu VALUES("247","239","0","pelaporan/report_temanggung_tabel_6_1/rekap_rt.php","7. JUMLAH KEPALA KELUARGA PRASEJAHTERA MENURUT INDIKATOR YANG JATUH","JUMLAH KEPALA KELUARGA PRASEJAHTERA MENURUT INDIKATOR YANG JATUH","7","");
INSERT INTO dbo_menu VALUES("246","239","0","pelaporan/report_temanggung_tabel_6/rekap_rt.php","6. JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI","JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI","6","");
INSERT INTO dbo_menu VALUES("245","239","0","pelaporan/report_temanggung_tabel_5/rekap_rt.php","5. JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN","JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN","5","");
INSERT INTO dbo_menu VALUES("243","239","0","pelaporan/report_temanggung_tabel_3/rekap_rt.php","3. JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - KECIL 5 TH DAN PROXY CWR","JUMLAH WANITA USIA SUBUR, BALITA USIA 0 - < 5 TH DAN PROXY CWR","3","");
INSERT INTO dbo_menu VALUES("257","254","0","pelaporan/report_ks_tabel_1/rekap_rt.php","Berdasarkan Kepala Keluarga","","1","");
INSERT INTO dbo_menu VALUES("255","219","0","pelaporan/report_ks_tabel_2/rekap_rt.php","Tabel Non KB Peralasan Per Thp KS","sdas","4","");
INSERT INTO dbo_menu VALUES("258","254","0","pelaporan/report_ks_tabel_3/rekap_rt.php","Berdasarkan Individu, Umur dan Pendidikan","","2","");
INSERT INTO dbo_menu VALUES("259","","0","","Backup-Restore Data","Backup & Restore Data","6","");
INSERT INTO dbo_menu VALUES("260","259","0","backup-restore/index.php","Eksport","Eksport","1","");
INSERT INTO dbo_menu VALUES("249","240","0","pelaporan/report_temanggung_ks_tabel_2/rekap_rt.php","2. JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 - KECIL 1 TH DAN PROXY CBR","JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 -< 1 TH DAN PROXY CBR","2","");
INSERT INTO dbo_menu VALUES("244","239","0","pelaporan/report_temanggung_tabel_4/rekap_rt.php","4. JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR","JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR","4","");
INSERT INTO dbo_menu VALUES("242","239","0","pelaporan/report_temanggung_tabel_2/rekap_rt.php","2. JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 - KECIL 1 TH DAN PROXY CBR","JUMLAH JIWA DALAM KELUARGA, BAYI USIA 0 -< 1 TH DAN PROXY CBR","2","");
INSERT INTO dbo_menu VALUES("238","219","0","","Tabel Umum","Tabel Umum","2","");
INSERT INTO dbo_menu VALUES("239","238","0","","Berdasarkan Wilayah","","1","");
INSERT INTO dbo_menu VALUES("240","238","0","","Berdasarkan Tahapan","","2","");
INSERT INTO dbo_menu VALUES("241","239","0","pelaporan/report_temanggung_tabel_1/rekap_rt.php","1. JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN","JUMLAH KEPALA KELUARGA DAN JUMLAH JIWA DALAM KELUARGA MENURUT JENIS KELAMIN","1","");
INSERT INTO dbo_menu VALUES("219","","0","","Tabel Kependudukan  ","TABEL KEPENDUDUKAN","3","");
INSERT INTO dbo_menu VALUES("220","219","0","","Kelompok Umur","","1","");
INSERT INTO dbo_menu VALUES("237","220","0","pelaporan/tabel_17/rekap_rt.php","17. JUMLAH PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR,  ALASAN TIDAK BER-KB DAN TAHAPAN KELUARGA SEJAHTERA","JUMLAH PUS BUKAN PESERTA KB MENURUT KELOMPOK UMUR,  ALASAN TIDAK BER-KB DAN TAHAPAN KELUARGA SEJAHTERA","17","");
INSERT INTO dbo_menu VALUES("251","240","0","pelaporan/report_temanggung_ks_tabel_4/rekap_rt.php","4. JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR","JUMLAH JIWA ANGGOTA KELUARGA MENURUT KELOMPOK UMUR","4","");
INSERT INTO dbo_menu VALUES("252","240","0","pelaporan/report_temanggung_ks_tabel_5/rekap_rt.php","5. JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN","JUMLAH USIA ANAK SEKOLAH, YANG SEKOLAH DAN TIDAK SEKOLAH MENURUT JENIS KELAMIN","5","");
INSERT INTO dbo_menu VALUES("253","240","0","pelaporan/report_temanggung_ks_tabel_6/rekap_rt.php","6. JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI","JUMLAH PASANGAN USIA SUBUR (PUS) DAN PESERTA KB MENURUT METODE KONTRASEPSI","6","");
INSERT INTO dbo_menu VALUES("261","259","0","backup-restore/recovery_data.php","Import-Restore","Import","2","");
INSERT INTO dbo_menu VALUES("262","259","0","","Backup","","3","");
INSERT INTO dbo_menu VALUES("263","262","0","backup_data_desa/index.php","Backup Data Keluarga","","1","");
INSERT INTO dbo_menu VALUES("264","262","0","backup_tabel/backup.php","Backup Tabel","","2","");
INSERT INTO dbo_menu VALUES("265","262","0","backup_data_desa/backup/","Download File Data Keluarga","","3","");
INSERT INTO dbo_menu VALUES("266","259","0","backup_repair/backup.php","Repair Tabel","","4","");
INSERT INTO dbo_menu VALUES("267","259","0","backup-restore/backup/","Download File Backup","","5","");
INSERT INTO dbo_menu VALUES("268","95","0","kiki/rekap_rt.php","Update Data Keluarga","","3","");
INSERT INTO dbo_menu VALUES("269","65","0","conver_status/rekap_rt.php","Konversi","conver_status","5","");
INSERT INTO dbo_menu VALUES("270","220","0","pelaporan/tabel_3_+/rekap_rt.php","3+. JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA","JUMLAH JIWA PEMILIH MENURUT KELOMPOK UMUR JENIS KELAMIN DAN STATUS DALAM KELUARGA","3","");
INSERT INTO dbo_menu VALUES("271","219","0","pelaporan/report_kat_usia/rekap_rt.php","Tabel Kategori Usia (Balita, Remaja, Lansia)","Tabel Kategori Usia (Balita, Remaja, Lansia)","5","");
INSERT INTO dbo_menu VALUES("272","219","0","pelaporan/report_temanggung_pus/rekap_rt.php","Jumlah PUS, Peserta KB dan bukan peserta KB ","Jumlah PUS, Peserta KB dan bukan peserta KB ","6","");
INSERT INTO dbo_menu VALUES("273","164","0","report_ks_alek/rekap_rt.php","Pra S - KS I Alek Dan Non Alek","Pra S - KS I Alek Dan Non Alek ","1","");
INSERT INTO dbo_menu VALUES("274","288","0","report_jumlah_sasaran_11/rekap_rt.php","5. JUMLAH KELUARGA MEMILIKI ANAK 0-11 TAHUN","","4","");
INSERT INTO dbo_menu VALUES("276","288","0","report_sasaran_kel_balita/rekap_rt.php","13.  JUMLAH SASARAN KELUARGA MEMILIKI BALITA","","12","");
INSERT INTO dbo_menu VALUES("277","288","0","report_ratio_beban_per_daerah/rekap_rt.php","11. PROXY RATIO BEBAN KETERGANTUNGAN PER DAERAH","Report Proxi Ratio Beban Ketergantungan Daerah","10","");
INSERT INTO dbo_menu VALUES("278","288","0","report_angka_kematian_kasar/rekap_rt.php","10. PROXY ANGKA KEMATIAN KASAR PER DAERAH","Report Proxi Angka Kematian Kasar","9","");
INSERT INTO dbo_menu VALUES("279","288","0","report_peserta_kb/rekap_rt.php","2. PESERTA KB MENURUT METODE KONTRASEPSI, KELOMPOK UMUR DAN JUMLAH ANAK","Peserta KB Menurut Metoda, Kelompok Umur dan Jumlah Anak","1","");
INSERT INTO dbo_menu VALUES("280","288","0","report_KP_KSI_tanpa_jamban/rekap_rt.php","9.  JUMLAH KPS DAN KS I YANG TIDAK MEMILIKI JAMBAN / SUMBER AIR MINUM BERSIH / SUMBER PENERANGAN LISTRIK / BAHAN BAKAR GAS/LISTRIK UNTUK MEMASAK","Report KPS dan KS 1 Tidak Punya Punya Jamban","8","");
INSERT INTO dbo_menu VALUES("281","288","0","report_kematian_bayi/rekap_rt.php","1. PROXI ANGKA KEMATIAN BAYI (IMR) PER DAERAH","Report Kematian Bayi","0","");
INSERT INTO dbo_menu VALUES("282","288","0","report_jum_kel_menurut_anak/rekap_rt.php","4. JUMLAH KELUARGA MENURUT JUMLAH ANAK YANG DIMILIKI DAN STATUS TAHAPAN KELUARGA SEJAHTERA ","Report Jml Keluarga Menurut Jml Anak & Status Tahapan Keluarga","3","");
INSERT INTO dbo_menu VALUES("283","288","0","report_kel_per_daerah/rekap_rt.php","3. JUMLAH KELUARGA PER DAERAH DAN STATUS TAHAPAN KELUARGA SEJAHTERA","Report Jml Keluarga per Daerah dan Status Tahapan Keluarga Sejahtera","2","");
INSERT INTO dbo_menu VALUES("284","288","0","report_kel_uppks/rekap_rt.php","8. JUMLAH KELUARGA IKUT UPPKS PER DAERAH","Report Keluarga Ikut UPPKS","7","");
INSERT INTO dbo_menu VALUES("285","288","0","report_punya_lansia/rekap_rt.php","7. JUMLAH SASARAN KELUARGA MEMILIKI LANSIA","Report Punya Lansia","6","");
INSERT INTO dbo_menu VALUES("286","288","0","report_punya_remaja/rekap_rt.php","6. JUMLAH SASARAN KELUARGA MEMILIKI REMAJA","Report Keluarga Punya Remaja","5","");
INSERT INTO dbo_menu VALUES("287","288","0","report_kelahiran_menurut_asfr/rekap_rt.php","12. PROXY ANGKA KELAHIRAN MENURUT KELOMPOK UMUR (ASFR)","Report Kelahiran Menurut ASFR Per Daerah","11","");
INSERT INTO dbo_menu VALUES("288","219","0","","Output Tambahan","","6","");
INSERT INTO dbo_menu VALUES("290","219","0","report_data_ind_ks/rekap_rt.php","Data Individu Rekapitulasi Pra S & KS I Alek, Non Alek","","7","");
INSERT INTO dbo_menu VALUES("291","288","0","rekap_usia_kawin_pertama_wanita/rekap_rt.php","14. Rekap Usia Kawin Pertama Wanita","14. Rekap Usia Kawin Pertama Wanita","14","");
INSERT INTO dbo_menu VALUES("292","259","0","generate_kki/index.php","Generate KKI & KAK","Generate KKI & KAK","6","");
INSERT INTO dbo_menu VALUES("293","219","0","chart/index.php","Piramida Kependudukan","Piramida Kependudukan","9","");



DROP TABLE  dbo_typeuser_menu_akses;

CREATE TABLE `dbo_typeuser_menu_akses` (
  `id_menu` smallint(6) NOT NULL,
  `id_typeuser` varchar(40) NOT NULL,
  PRIMARY KEY (`id_menu`,`id_typeuser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO  dbo_typeuser_menu_akses VALUES("0","10000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("0","100032");
INSERT INTO  dbo_typeuser_menu_akses VALUES("50","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("65","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("76","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("91","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("95","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("100","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("101","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("102","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("106","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("112","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("161","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("162","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("163","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("164","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("165","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("166","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("167","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("168","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("169","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("170","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("171","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("172","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("173","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("174","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("175","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("176","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("177","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("178","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("179","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("219","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("220","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("221","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("222","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("223","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("224","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("225","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("226","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("227","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("228","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("229","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("230","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("231","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("232","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("233","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("234","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("235","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("236","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("237","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("238","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("239","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("240","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("241","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("242","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("243","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("244","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("245","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("246","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("247","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("248","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("249","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("250","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("251","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("252","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("253","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("254","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("255","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("257","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("258","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("259","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("260","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("261","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("262","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("263","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("264","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("265","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("266","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("267","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("268","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("269","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("270","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("271","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("272","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("273","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("274","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("276","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("277","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("278","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("279","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("280","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("281","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("282","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("283","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("284","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("285","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("286","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("287","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("288","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("290","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("291","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("292","0000");
INSERT INTO  dbo_typeuser_menu_akses VALUES("293","0000");



